<!-- Fun Facts Start-->
<?php $health_data= health_care_get_options(); ?>
<span class="anchor" id="facts-section"></span>
<div class="facts">
<div class="container-fluid space facts-cover ">
	<div class="container ">
			<?php if($health_data['facts_heading']!=''){ ?>
		<h1 class="white"><?php echo $health_data['facts_heading']; ?></h1>
			<?php if($health_data['facts_icon']!=''){ ?>
			<div class="ln2 white"></div>
			<span class="<?php echo $health_data['facts_icon']; ?> white heart"></span>
			<div class="ln3 white"></div>
		<?php } ?>
	<?php } ?>
	<?php if($health_data['facts_desc']!=''){ ?>
			<p class="desc white"><?php echo $health_data['facts_desc']; ?></p>
	<?php } ?>
				<div class="row ">
					<div class="col-md-12 white">
						<div class="skills">
						<?php for($i=1; $i<=4; $i++){ 
						if($health_data['fact_icon_'.$i]!=''){?>
							<div class="col-sm-6 col-md-3 text-center">
							<span class="<?php echo $health_data['fact_icon_'.$i]; ?> icon" ></span>
							<span data-percent="<?php echo $health_data['fact_value_'.$i]; ?>" class="chart" style="width: 140px; height: 140px; line-height: 60px;">
							<span data-speed="2000" data-to="<?php echo $health_data['fact_value_'.$i]; ?>" class="percent countTo"><?php echo $health_data['fact_value_'.$i]; ?></span></span>
							<h2 class="text-name"><?php echo $health_data['fact_name_'.$i]; ?></h2>
							</div>
						<?php } } ?>
						</div>				
					</div>
				</div>	
	</div>
</div>
</div>
<!-- Fun Facts End-->