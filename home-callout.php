<!-- Call-out-->
<?php $health_data= health_care_get_options(); ?>
	
	<?php if($health_data['callout_text']!=''){ ?>
	<div class="container-fluid text">
		<div class="container">
			<div class="col-md-9 col-sm-6 hc_callout-text">
			<?php echo $health_data['callout_text']; ?>
			</div>
			<?php if($health_data['callout_button']!=''){ ?>
			<div class="col-md-3 col-sm-6 hc_callout-btn">
			<a href="<?php echo $health_data['callout_link']; ?>" class="btn link" <?php if($health_data['call_out_button_target']=='on') echo 'target="_blank"'; ?> ><?php echo $health_data['callout_button']; ?><span class="fa fa-plus"></span></a>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
	<!-- End Call-out -->