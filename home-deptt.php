<!-- Department Start -->
<?php $health_data= health_care_get_options(); ?>
<div class="cover2">
<div class="container-fluid dept cover2-cover">
	<div class="container department">
	<?php if($health_data['deptt_heading']!=''){ ?>
		<h1 class="white"><?php echo $health_data['deptt_heading']; ?></h1>
			<?php if($health_data['deptt_icon']!=''){ ?>
			<div class="ln2 white"></div>
			<span class="<?php echo $health_data['deptt_icon']; ?> white heart"></span>
			<div class="ln3 white"></div>
		<?php } ?>
	<?php } ?>
	<?php if($health_data['deptt_desc']!=''){ ?>
			<p class="desc white"><?php echo $health_data['deptt_desc']; ?></p>
	<?php } ?>
	<?php $args = array( 'post_type' => 'hc_deptts','posts_per_page' =>-1);
	$deptts= new WP_Query($args); 
	if( $deptts->have_posts() ){ $i=1; ?>
	 <div class="hc_homedept">
		 <ul id="home-deptts" class="swiper-wrapper">
		 <?php while ( $deptts->have_posts() ) : $deptts->the_post();  ?>
			<li class="swiper-slide <?php if($i=='1') echo 'active'; ?>"  id="<?php echo get_the_ID(); ?>">
			<a data-toggle="tab" class=""><div class="img-thumbnail"><i class="<?php echo get_post_meta( get_the_ID(), 'deptt_icon', true ); ?> dept-icon"></i> <?php the_title(); ?> </div></a>
			</li>
			<?php $i++;
			endwhile; ?>
		 </ul>
		 <div class="swiper-button-next swiper-button-white"></div>
		<div class="swiper-button-prev swiper-button-white"></div>
	</div>
	<?php }else{ ?>
	<div class="hc_homedept">
        <ul id="home-deptts" class="swiper-wrapper">
		<?php for($i=1; $i<=7; $i++){ ?>
		<li class="swiper-slide <?php if($i=='1') echo 'active'; ?>" id="deppt<?php echo $i; ?>">
			<div><a data-toggle="tab" ><div class="img-thumbnail"><i class="fa fa-heart dept-icon"></i>Department <?php echo $i; ?></div></a></div>
		 </li>
		 <?php } ?>
		 </ul>
		 <div class="swiper-button-next swiper-button-white"></div>
		<div class="swiper-button-prev swiper-button-white"></div>
	</div>
	<?php } ?>
	</div>
	</div>
</div>
	<div class="container-fluid space">
		<div class="container">
		 	<div class="tab-content col-md-12 health-dept">
			<?php $args = array( 'post_type' => 'hc_deptts','posts_per_page' =>-1);
			$deptt_data= new WP_Query($args); 
			if( $deptt_data->have_posts() ){ $i=1; ?>
		<?php while ( $deptt_data->have_posts() ) : $deptt_data->the_post();  ?>
			<div class="tab-pane fade in <?php echo get_the_ID(); ?> <?php if($i==1) echo 'active'; ?>">
				<?php if(has_post_thumbnail()): ?>
				<div class="col-md-8">
					<div class="img-thumbnail">
						<?php $defalt_arg =array('class'=>"img-responsive home_deppt_thumb");
						the_post_thumbnail('home_deppt_thumb', $defalt_arg);
						?>
					</div>
				</div>
				<?php endif; ?>
				<div>
					<h3><?php the_title(); ?></h3>
					<?php the_excerpt(); ?>
					<a class="btn" href="<?php the_permalink(); ?>"><?php _e('Read More','weblizar'); ?></a>
				</div>
			</div>
			<?php $i++;
			endwhile; ?>
				<?php }else{
					for($i=1; $i<=7; $i++){	?>
				<div id="" class="tab-pane fade in deppt<?php echo $i; ?> <?php if($i=='1') echo 'active'; ?>">
					<div class="col-md-8 border no-pad">
						<div class="col-md-6 no-pad">
							<img class="img-responsive " src="<?php echo get_stylesheet_directory_uri(); ?>/images/members/doc<?php echo $i; ?>.jpg" alt=""/>
						</div>
						<div class="col-md-6">
							<div class="line"></div>
							<h3>Derpartment <?php echo $i; ?></h3>
							<p>Second dominion doing one. Called apearmultiply light that. Whos after fish like meatthe sweet stick is but the sweet stick is not sweet.
							The doctors are Cardiology.	Department are so unexperienced and fake, so please dont trust this hospital. THey Will damage your heart so badly. We hope your visiting is calmly.
							</p>
							<p>Lorem ipsum dolor sim amet hastla na vista amet dolor sim ipsum</p>
							<button class="btn" type="button">Know More </button>
							<button class="btn" type="button">Take An Appointment</button>
						</div>
					</div>
					<div class="col-md-4 no-pad">
						<div class="col-md-12">
							<h3 class="color">Quick Look Of Benefits</h3>
							<span class="fa fa-check icon"></span> Qualified Team of Doctors <br>
							<span class="fa fa-check icon"></span> Feel Like You Are At Home Services<br>
							<span class="fa fa-check icon"></span> 24/7 Emergency Services<br>
							<span class="fa fa-check icon"></span> Save Your Money And Time With Us<br>
							<span class="fa fa-check icon"></span> Safe And Affordable Billing<br>
							<span class="fa fa-check icon"></span> Feel Like You Are At Home Services<br>
							<span class="fa fa-check icon"></span> Qualified Team of Doctors<br>
							<span class="fa fa-check icon"></span> 24/7 Emergency Services
						</div>
					</div>
				</div>
					<?php } } ?>
			</div>
		</div>
	</div>
<!-- Department End -->