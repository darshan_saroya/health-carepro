<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300">
  <?php $health_data= health_care_get_options(); ?>
  <?php if($health_data['upload_image_favicon']!=''){ ?>
	<link rel="shortcut icon" href="<?php  echo $health_data['upload_image_favicon']; ?>" /> 
	<?php } ?>
	<style type="text/css">
		 <?php if($health_data['testi_background']!=''){ ?>
		 	.testimonail{
		 		background-image:url(<?php echo $health_data['testi_background']; ?>) !important;
		 	}
		 <?php } ?>
		 <?php if($health_data['deptt_background']!=''){ ?>
		 	.cover2{
		 		background-image:url(<?php echo $health_data['deptt_background']; ?>) !important;
		 	}
		 <?php } ?>
    <?php if($health_data['custom_cover']=='on'){ ?>
		.cover {
            background-image: url("<?php header_image(); ?>");
          }
	<?php }else{ ?>
    .cover {
            background-image: url("<?php echo $health_data['cover_bg_image']; ?>");
          }
   <?php } ?>
   <?php if($health_data['facts_background']!=''){ ?>
    .facts {
            background-image: url("<?php echo $health_data['facts_background']; ?>");
          }
   <?php } ?>
.carousel-inner > .item > img{
	max-height:<?php echo $health_data['slide_height']; ?>px;
}</style>
  <?php wp_head(); ?>
</head>
<?php if($health_data['custom_bg']=='on') { ?>
<body <?php body_class(); ?> >
<?php }else{ ?>
<body <?php body_class(); ?> <?php if($health_data['site_bg']){ ?> style="background:url('<?php echo $health_data['site_bg']; ?>') repeat fixed;" <?php } ?>>
<?php } ?>
<div id="wrapper" class="<?php echo $health_data['site_layout']; ?>">
<!-- Start Header-->
<header id="top">
	<div class="container-fluid top">
		<div class="container">
		<div class="col-md-4 header-info">
			<?php if($health_data['social_phone']!=''){ ?>			
			<a href="tel:<?php echo $health_data['social_phone'];?>" ><span><i class="fa fa-phone"></i><?php echo $health_data['social_phone'];?> </span></a><?php } ?>
			<?php if($health_data['social_email']!=''){ ?>  
			<a href="mailto:<?php echo $health_data['social_email'];?>" ><span><i class="fa fa-envelope"></i> <?php echo $health_data['social_email'];?> </span></a><?php } ?>
		</div>
		<div class="col-md-6 header-timing">
				<?php if($health_data['social_timing']!='') echo '<span class="timing">'.$health_data['social_timing'].'</span>';?>
			</div>
		<?php if($health_data['header_social_icon']=='on'){ ?>
			<div class="col-md-2 header-social">
				<?php for($i=1; $i<=6; $i++){
						if($health_data['social_icon_'.$i]!=''){
					?>
					<a href="<?php echo $health_data['social_link_'.$i]; ?>" target="_blank"><span class="<?php echo $health_data['social_icon_'.$i]; ?> header-icon"></span></a>
						<?php }
					} ?>
			</div>
		<?php } ?>
				
			
		</div>
	</div>
	<nav class="navbar navbar-static-top marginBottom-0 menu <?php if($health_data['fix_header']=='on') echo 'menu-layout'; ?>">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
				<span class="sr-only"><?php _e('Toggle navigation','weblizar'); ?></span>
				<span class="icon-bar black"></span>
				<span class="icon-bar black"></span>
				<span class="icon-bar black"></span>
			</button>
				  <a href="<?php echo esc_url(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="navbar-brand"><?php if($health_data['upload_image_logo']!=''){ ?>
					<img src="<?php echo $health_data['upload_image_logo']; ?>" style="height:<?php echo $health_data['logo_height']; ?>px; width:<?php echo $health_data['logo_width']; ?>px;" alt="<?php echo get_bloginfo('name'); ?>"/>  
				 <?php }else{
				 echo get_bloginfo('name'); } ?></a>
			</div>
            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <?php wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_class' => 'nav navbar-nav navbar-right',
						'fallback_cb' => 'weblizar_fallback_page_menu',
						'walker' => new health_care_nav_walker(),
						)
						);	?>
            </div><!-- /.navbar-collapse -->
		</div>
    </nav>
</header>
<!-- End Header -->