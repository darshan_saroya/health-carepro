<?php get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space portfolio-details">
	<div class="container">
	<?php the_post(); 
	$portfolio_categories_list = get_the_terms( get_the_ID(), 'hc_portfolio_categories' );
								if($portfolio_categories_list){
								$totalcat= count($portfolio_categories_list);
								$i=1;
								$cats= array();
									foreach($portfolio_categories_list as $list)
									{	
									$cats[]= $list->slug;
									} 									
								} ?> 
		<div class="row navi">
			<ul class="pager">
				<?php $next_post = get_next_post();
					if (!empty( $next_post )): ?>
						<li class="previous"><a href="<?php echo get_permalink( $next_post->ID ); ?>" title="Previous" rel="prev"><span class="fa fa-arrow-left"></span></a></li>
				<?php endif; ?>	
				<?php $prev_post = get_previous_post();
				if (!empty( $prev_post )): ?>
					<li class="next"><a href="<?php echo get_permalink( $prev_post->ID ); ?>" title="Next" rel="next"><span class="fa fa-arrow-right"></span></a></li>
				<?php endif; ?>	
			</ul>	
		</div>
		<div class="row portfolio-pic">
			<div class="col-md-8">
				<div class="img-thumbnail">
					<?php if(has_post_thumbnail()){
					$class=array('class'=>'img-responsive portfolio_detail_thumb');  
					the_post_thumbnail('portfolio_detail_thumb', $class); 
				} ?>
				</div>
			</div>
			<div class="col-md-4">
				<h2><?php the_title(); ?></h2>
				<span class="fa fa-clock-o icon port-date"> <?php echo get_the_date(); ?>	</span>
				<div class="detail">
					<?php if(get_post_meta( get_the_ID(), 'portfolio_skill', true )!=''){ ?>
					<span><?php _e('Skills: ','weblizar'); 
					echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_skill', true )); ?></span>
					<?php } if(get_post_meta( get_the_ID(), 'portfolio_skill', true )!=''){ ?>
					<span><?php _e('Client: ','weblizar'); 
					echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_client', true )); ?> </span>
					<span class="port-author"><?php _e('Author: ','weblizar'); the_author(); ?></span>
					<?php } ?>
				</div>
				<?php if(get_post_meta( get_the_ID(), 'portfolio_url', true )!=''){ ?>
				<a href="<?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_url', true )); ?>" class="btn port-link" <?php if(get_post_meta( get_the_ID(), 'portfolio_link_target', true )) echo 'target="_blank"'; ?>><span class="fa fa-search-plus"></span><?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_url_text', true )); ?></a>
				<?php } ?>
			</div>
		</div>
		<?php if(sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_desc_title', true ))){ ?>
		<div class="row single-text">
				<h1 class="color"><?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_desc_title', true )); ?></h1>
				<div class="ln2 color"></div>
				<span class="<?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_icon', true )); ?> color heart"></span>
				<div class="ln3 color"></div>
		</div>
		<?php } ?>
		<div class="row type1">
			<p><?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_desc', true )); ?></p>
		</div>
		<?php $attachments = get_children(
					array('post_parent' => get_the_ID(),'post_status' => 'inherit','post_type' => 'attachment','post_mime_type' => 'image')
					);
					if($attachments){ ?>
						<div class="row  gallery">
							<div id="grid-masonry">
								<?php foreach($attachments as $att_id => $attachment) {
								$full_img_url = wp_get_attachment_url($attachment->ID);
								echo '<div class="col-md-3 col-sm-6 port-item"><a class="port-single" href="'.$full_img_url.'"><img src="'.$full_img_url.'" class="img-responsive" /></a></div>';
								} ?>
							</div>
						</div>
					<?php	} ?>
	</div>
	<div class="container">
		<?php $args = array( 'post_type' => 'hc_portfolios', 'post_status'=>'publish', 'posts_per_page' => -1, 'post__not_in'=> array(get_the_ID()),'tax_query' => array(array('taxonomy' => 'hc_portfolio_categories','field'    => 'slug','terms'    => $cats,),),);
				$gallery = new WP_Query( $args );
				if($gallery->have_posts()){ ?>
				<div class="line"></div>
				<h3><?php _e('Related Portfolio','weblizar'); ?></h3>
				<div class="hc_relatedports">
				<div id="single-gallery" class="swiper-wrapper gallerys">
					<?php  while($gallery->have_posts()):
					$gallery->the_post(); ?>
					<?php if(has_post_thumbnail()): ?>
						<div class="swiper-slide wow zoomIn">
						<?php $defalt_arg =array('class'=>"img-responsive home_porfolio_thumb");
								the_post_thumbnail('home_porfolio_thumb',$defalt_arg); ?>
							<div class="overlay">
							<h3><a class="port-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<div class="ln"></div>
							<div class="<?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_icon', true )); ?> hrt"></div>
								<h5><?php 	$portfolio_categories_list = get_the_terms( get_the_ID(), 'hc_portfolio_categories' );
											if($portfolio_categories_list){
											$totalcat= count($portfolio_categories_list);
											$i=1;
												foreach($portfolio_categories_list as $list)
												{	echo $list->name;
													if($i <$totalcat){ echo ", ";}
													$i++;
												}  
											} ?></h5>
								<a class="home-port" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"><span class="fa fa-search"></span></a>
								<a href="<?php the_permalink(); ?>"><span class="fa fa-chain"></span></a>
							</div>
						</div>
					<?php endif; 
					endwhile; ?>
				</div>
					<div class="swiper-button-next swiper-button-white"></div>
					<div class="swiper-button-prev swiper-button-white"></div>
				</div>
					<?php } ?>
	</div>
</div>
<?php get_footer(); ?>