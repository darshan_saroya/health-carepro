<?php //Template name: Service Template 2
get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid services space">
	<div class="container">
	<div class="col-md-12 service-content"><?php the_post();
	the_content(); ?>
	</div>
		<div class="row">
				<?php $args = array( 'post_type' => 'hc_services','posts_per_page' =>$health_data['service_count']);
			$service = new WP_Query( $args );
			if( $service->have_posts() ){
				while ( $service->have_posts() ) : $service->the_post(); $col=12; ?>
					<a href="<?php if(get_post_meta( get_the_ID(), 'service_button_link', true )){ echo get_post_meta( get_the_ID(), 'service_button_link', true ); }else{ the_permalink(); } ?>" <?php if(get_post_meta( get_the_ID(),'service_button_target', true )) { echo 'target="_blank"'; }?>>
					<div class="col-md-4 col-sm-6 service" >
					<?php if(get_post_meta( get_the_ID(), 'service_font_awesome_icons', true )){ 
					$col=9; ?>
						<div class="col-md-3 hc_serv_icon" ><span class="<?php echo get_post_meta( get_the_ID(), 'service_font_awesome_icons', true ); ?> icon"></span></div>
					<?php }else if(has_post_thumbnail()){ $thumbs = "img-responsive service_home_thumb"; ?>
							<div class="img-thumbnail">
							<?php the_post_thumbnail('service_home_thumb', $thumbs); ?>
							</div>
							<?php } ?> 
						<div class="col-md-<?php echo $col; ?> hc_serv_text">
							<h3><?php the_title(); ?></h3>
							<p><?php the_excerpt(); ?></p>
						</div>
					</div>
					</a>
					<?php endwhile; 
			}else{ 
			for($i=1; $i<=6; $i++){ ?>
					<a href="">
					<div class="col-md-4 col-sm-6 service" >
						<div class="col-md-3"><span class="fa fa-ambulance icon"></span></div>
						<div class="col-md-9">
							<h3>Psychological Counceling</h3>
							<p>We Will put together a detailed and specific style guide that covers all areas of your brand to ansure that anything.</p>
						</div>
					</div>
					</a>
			<?php } } ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>