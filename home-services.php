<!-- Services Start -->
<?php $health_data= health_care_get_options(); ?>
<span class="anchor" id="service-section"></span>
<div class="container-fluid space services">
	<div class="container">
	<?php if($health_data['service_heading']!=""){ ?>
		<h1 class="color"><?php echo $health_data['service_heading']; ?></h1>
		<?php if($health_data['service_icon']!=""){ ?>
		<div class="ln2 color"></div>
	<span class="<?php echo $health_data['service_icon']; ?> color heart"></span>
	<div class="ln3 color"></div>
		<?php } ?>
	<?php } ?>
	<?php if($health_data['service_desc']!=""){ ?>
	<p class="desc"><?php echo $health_data['service_desc']; ?></p>
	<?php } ?>
		<div class="row hc-service">
		<?php $args = array( 'post_type' => 'hc_services','posts_per_page' =>$health_data['service_count']);
			$layout=$health_data['service_layout'];
			if($health_data['service_layout']=='2'){ $i=6;}
			if($health_data['service_layout']=='3'){ $i=4;}
			if($health_data['service_layout']=='4'){ $i=3;}
	$service = new WP_Query( $args );
	if( $service->have_posts() ){
		while ( $service->have_posts() ) : $service->the_post();  ?>
			<a href="<?php if(get_post_meta( get_the_ID(), 'service_button_link', true )){ echo get_post_meta( get_the_ID(), 'service_button_link', true ); }else{ the_permalink(); } ?>" <?php if(get_post_meta( get_the_ID(),'service_button_target', true )) { echo 'target="_blank"'; }?>>
			<div class="col-md-<?php echo $i; ?> col-sm-6 service">
			<?php if(get_post_meta( get_the_ID(), 'service_font_awesome_icons', true )){ ?>
				<span class="<?php echo get_post_meta( get_the_ID(), 'service_font_awesome_icons', true ); ?> icon"></span>
			<?php }else if(has_post_thumbnail()){ $thumbs = "img-responsive service_home_thumb" ; ?>
					<div class="img-thumbnail">
					<?php the_post_thumbnail('service_home_thumb', $thumbs); ?>
					</div>
					<?php } ?> 
				<div class="col-md-12">
					<h3><?php the_title(); ?></h3>
					<p><?php the_excerpt(); ?></p>
				</div>
			</div>
			</a>
			<?php endwhile; 
	}else{ ?>
			<a href="">
			<div class="col-md-4 col-sm-6 service" >
				<span class="fa fa-ambulance icon"></span>
				<div class="col-md-12">
					<h3>Psychological Counceling</h3>
					<p>We Will put together a detailed and specific style guide that covers all areas of your brand to ansure that anything.</p>
				</div>
			</div>
			</a>
			<a href="">
			<div class="col-md-4 col-sm-6 service" >
				<span class="fa fa-ambulance icon"></span>
				<div class="col-md-12">
					<h3>Psychological Counceling</h3>
					<p>We Will put together a detailed and specific style guide that covers all areas of your brand to ansure that anything.</p>
				</div>
			</div>
			</a>
			<a href="">
			<div class="col-md-4 col-sm-6 service" >
				<span class="fa fa-heartbeat icon"></span>
				<div class="col-md-12">
					<h3>Rehabilation Therapy</h3>
					<p>We Will put together a detailed and specific style guide that covers all areas of your brand to ansure that anything.</p>
				</div>
			</div>
			</a>
			<a href="">
			<div class="col-md-4 col-sm-6 service" >
				<span class="fa fa-stethoscope icon"></span>
				<div class="col-md-12">
					<h3>Cardio Monitoring</h3>
					<p>We Will put together a detailed and specific style guide that covers all areas of your brand to ansure that anything.</p>
				</div>
			</div>
			</a>
			<a href="">
			<div class="col-md-4 col-sm-6 service" >
				<span class="fa fa-wheelchair icon"></span>
				<div class="col-md-12">
					<h3>Emergency Help</h3>
				<p>We Will put together a detailed and specific style guide that covers all areas of your brand to ansure that anything.</p>
				</div>
			</div>
			</a>
			<a href=""> 
			<div class="col-md-4 col-sm-6 service" >
				<span class="fa fa-user-md icon"></span>
				<div class="col-md-12">
					<h3>Background Check</h3>
					<p>We Will put together a detailed and specific style guide that covers all areas of your brand to ansure that anything.</p>
				</div>
			</div>
			</a>
	<?php } ?>
		</div>
	</div>
</div>
<!-- Services End -->