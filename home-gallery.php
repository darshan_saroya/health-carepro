<!-- Gallery Start -->
<?php $health_data= health_care_get_options(); ?>
<span class="anchor" id="gallery-section"></span>
<div class="space gallerys">
<?php if($health_data['gallery_heading']!=''){ ?>
				<h1 class="color"><?php echo $health_data['gallery_heading']; ?></h1>
		<?php if($health_data['gallery_icon']!=''){ ?>
			<div class="ln2 color"></div>
			<span class="<?php echo $health_data['gallery_icon']; ?> color heart"></span>
			<div class="ln3 color"></div>
		<?php } ?>
<?php } ?>
<?php if($health_data['gallery_desc']!=''){ ?>
		<p class="desc"><?php echo $health_data['gallery_desc']; ?></p>
<?php } ?>
 <div class="hc_homegallery">
        <div id="home-gallery" class="swiper-wrapper gallery">
<?php
	$args = array( 'post_type' => 'hc_portfolios', 'post_status'=>'publish', 'posts_per_page' => $health_data['gallery_count']);
		$gallery = new WP_Query( $args );
		if($gallery->have_posts()){ 
		while($gallery->have_posts()):
		$gallery->the_post(); ?>
		<?php if(has_post_thumbnail()): ?>
			<div class="swiper-slide wow zoomIn">
			<?php $defalt_arg =array('class'=>"img-responsive home_porfolio_thumb");
					the_post_thumbnail('home_porfolio_thumb',$defalt_arg); ?>
				<div class="overlay">
				<h3><a class="port-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<div class="ln"></div>
				<div class="<?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_icon', true )); ?> hrt"></div>
					<h5><?php 	$portfolio_categories_list = get_the_terms( get_the_ID(), 'hc_portfolio_categories' );
								if($portfolio_categories_list){
								$totalcat= count($portfolio_categories_list);
								$i=1;
									foreach($portfolio_categories_list as $list)
									{	echo $list->name;
										if($i <$totalcat){ echo ", ";}
										$i++;
									}  
								} ?></h5>
					<a class="home-port" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"><span class="fa fa-search"></span></a>
					<a href="<?php the_permalink(); ?>"><span class="fa fa-chain"></span></a>
				</div>
			</div>
			<?php endif; 
			endwhile; 
			}else{ for($i=1; $i<=8; $i++){ ?>
			<div class="swiper-slide wow zoomIn">
				<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/portfolio/<?php echo $i; ?>.jpg" alt="Owl Image" >
				<div class="overlay">
				<h3>Lorem Ipsum Dolor</h3>
					<div class="ln"></div>
					<div class="fa fa-heart hrt"></div>
					<h5>Lorem Ipsum Dolor</h5>
					<a class="home-port" href="<?php echo get_template_directory_uri(); ?>/images/portfolio/<?php echo $i; ?>.jpg"><span class="fa fa-search"></span></a>
					<a href=""><span class="fa fa-chain"></span></a>
				</div>
			</div>
			<?php } } ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {
     var swiper = new Swiper('.hc_homegallery', {
		slidesPerView: <?php echo $health_data['gallery_column']; ?>,
        paginationClickable: true,
        spaceBetween: 0,
		breakpoints: {
            1024: {
                slidesPerView: <?php echo $health_data['gallery_column']; ?>,
                spaceBetween: 0
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 0
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 0
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            }
        }
    });
  });
</script>
<!-- Gallery End -->