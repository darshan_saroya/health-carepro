<?php //Template name: About Us 2
get_header(); 
get_template_part('blog','cover'); 
$health_data= health_care_get_options();
if ( have_posts()){ 
	while ( have_posts() ): the_post(); ?>
<!-- About Start -->
<div class="container-fluid space">
	<div class="container about-us">
	<?php if(has_post_thumbnail()): ?>
		<div class="col-md-12 no-pad">
			<?php $class=array('class'=>'img-responsive');  
				the_post_thumbnail('portfolio_detail_thumb', $class); ?>
		</div>
	<?php endif; ?>
		<div class="col-md-12">
		<?php the_content(); ?>
		</div>
	</div>
</div>
<?php endwhile; } ?>
<!-- Doctors Start -->
<?php $health_data= health_care_get_options(); ?>
	<div class="container-fluid space">
		<div class="container about-doctors">
			<?php if($health_data['member_heading']!=''){ ?>
		<h1 class="color"><?php echo $health_data['member_heading']; ?></h1>
			<?php if($health_data['member_icon']!=''){ ?>
			<div class="ln2 color"></div>
			<span class="<?php echo $health_data['member_icon']; ?> color heart"></span>
			<div class="ln3 color"></div>
		<?php } ?>
	<?php } ?>
		<div class="button-group filter-button-group">
		<?php $args = array( 'post_type' => 'hc_deptts','posts_per_page' =>-1);
				$member_cats= new WP_Query($args); 
				$i=0;
				if( $member_cats->have_posts() ){ ?>
		  <?php while ( $member_cats->have_posts() ) : $member_cats->the_post(); ?> 
		  <button class="button <?php if($i==0) echo 'is-checked'; ?>" data-filter=".<?php echo str_replace(' ', '_', get_the_title()); ?>"><?php echo get_the_title(); ?><span class="<?php echo get_post_meta( get_the_ID(), 'deptt_icon', true ); ?>  icon"></span></button>
		  <?php $i++;
		  endwhile; 
				} ?>
		</div>
		<?php $args = array( 'post_type' => 'hc_member','posts_per_page' =>-1);
				$members= new WP_Query($args); 
				if( $members->have_posts() ){ ?>
		<div class="grid">	
			 <?php while ( $members->have_posts() ) : $members->the_post();
			  $member_cat= get_post_meta( get_the_ID(), 'member_cat', true ); 
			  $cats=explode(" ,",$member_cat); ?>
			   <?php if(has_post_thumbnail()): ?>
			<div class="element-item col-md-4  col-sm-6 wl-gallery doctors-detail <?php  foreach($cats as $cat){ echo str_replace(' ', '_', $cat).' '; } ?>" data-category="transition" >
				<div class="col-md-12 about-doc">
				<div class="col-md-12 about-doc-details">
				<div class="img-thumbnail">
					<?php $defalt_arg =array('class'=>"img-responsive img-circle");
					the_post_thumbnail('home_member_thumb', $defalt_arg);
					?>
				</div>
				<div class="row about-doctors-detail">
					<div class="ln white"></div>
					<span class="fa fa-heart white heart"></span>
					<h3><?php the_title(); ?></h3>
					<div class="ln3 white"></div>
					
					<p><?php the_excerpt(); ?></p>
					<div class="social">
					<?php $member_twitter = sanitize_text_field( get_post_meta( get_the_ID(), 'member_twitter', true ));
					if($member_twitter!=''){ ?>
					<a href="<?php echo $member_twitter; ?>"><span class="fa fa-twitter icon"></span></a>
					<?php } 
					$member_fb = sanitize_text_field( get_post_meta( get_the_ID(), 'member_fb', true ));
					if($member_fb!=''){ ?>
					<a href="<?php echo $member_fb; ?>"><span class="fa fa-facebook icon"></span></a>
					<?php } 
					$member_google = sanitize_text_field( get_post_meta( get_the_ID(), 'member_google', true ));
					if($member_google!=''){ ?>
					<a href="<?php echo $member_google; ?>"><span class="fa fa-google-plus icon"></span></a>
					<?php } ?>
					</div>
					<?php $member_exp = sanitize_text_field( get_post_meta( get_the_ID(), 'member_exp', true ));
							if($member_exp!=''){?>
          <div class="col-md-12 about-doc-link">
						<a href="" class="btn"><?php echo $member_exp; ?></a>
           </div>
							<?php } ?>
					<div class="ln2"></div>
					<a href="<?php the_permalink(); ?>" class="view"><?php _e('View Profile','weblizar'); ?></a>
				</div>
				</div>
				</div>
			</div>
			<?php endif;
			endwhile; ?>
		</div>
				<?php } ?>
		</div>
	</div>
<!-- Doctors End -->
<!-- Testimonial -->
<div class="container-fluid space">
		<div class="container about-client-say">
		<?php if($health_data['testi_heading']!=""){ ?>
		<h1 class="color"><?php echo $health_data['testi_heading']; ?></h1>
		<?php if($health_data['testi_icon']!=''){ ?>
			<div class="ln2 color"></div>
			<span class="<?php echo $health_data['testi_icon']; ?> color heart"></span>
			<div class="ln3 color"></div>
		<?php } ?>
	<?php } ?>
		</div>
			<div class="row client">
				<div class="container">
				<?php $args = array( 'post_type' => 'hc_testimonials','posts_per_page' =>-1);        
	$hc_testimonials = new WP_Query( $args );
	if( $hc_testimonials->have_posts() ){ ?>
	<div class="about_testi">
        <div id="about-testi" class="swiper-wrapper gallery">
					<?php while ( $hc_testimonials->have_posts() ) : $hc_testimonials->the_post(); ?>
						<div class="swiper-slide">
							<div class="row">
								<div class="col-xs-12 client-say">
									<div class="col-xs-3 about-pics">
									<?php  if(has_post_thumbnail()){ ?>
										<div class="img-thumbnail">
											<?php $defalt_arg =array('class'=>"img-responsive img-circle");
										the_post_thumbnail('home_testi_thumb', $defalt_arg); ?>
										</div>
									<?php } ?>
									</div>
									<div class="col-xs-9">
										<h2><?php the_title(); ?> 
										<?php if(get_post_meta( get_the_ID(), 'testimonial_designation', true )){ ?>
											{<?php echo get_post_meta( get_the_ID(), 'testimonial_designation', true ); ?>}
									<?php } ?></h2>
									<?php if(get_post_meta( get_the_ID(), 'testimonial_description', true )){ ?>
										<p class="desc">
											"<?php echo get_post_meta( get_the_ID(), 'testimonial_description', true); ?>"
										</p>
									<?php } ?>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					</div>
				</div>
	<?php } ?>
				</div>
			</div>
</div>
<!-- Client Say -->
<?php get_template_part('home', 'clients');
get_template_part('home', 'appointment');
get_footer(); ?>