<?php get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space doctors-details">
	<div class="container">
			<div <?php post_class();?>>	
				<?php if ( have_posts()): 
					while ( have_posts() ): the_post(); ?>
					<?php if(has_post_thumbnail()): ?>
						<div class="row doctors-pic">
							<div class="col-md-8">
								<div class="img-thumbnail">
									<?php $defalt_arg =array('class'=>"img-responsive single_member_thumb");
								the_post_thumbnail('single_member_thumb', $defalt_arg);
								?>
								</div>
							</div>
							<div class="col-md-4">
								<h2><?php the_title(); ?></h2>
								<?php if(get_post_meta( get_the_ID(), 'member_post', true )!=''){ ?>
								<span>(<?php echo get_post_meta( get_the_ID(), 'member_post', true ); ?> )</span>
								<?php } ?>
								<div class="social">
								<?php $member_twitter = sanitize_text_field( get_post_meta( get_the_ID(), 'member_twitter', true ));
								if($member_twitter!=''){ ?>
						<a href="<?php echo $member_twitter; ?>"><span class="fa fa-twitter icon"></span></a>
								<?php } 
						$member_fb = sanitize_text_field( get_post_meta( get_the_ID(), 'member_fb', true ));
						if($member_fb!=''){ ?>
						<a href="<?php echo $member_fb; ?>"><span class="fa fa-facebook icon"></span></a>
						<?php } 
						$member_google = sanitize_text_field( get_post_meta( get_the_ID(), 'member_google', true ));
						if($member_google!=''){ ?>
						<a href="<?php echo $member_google; ?>"><span class="fa fa-google-plus icon"></span></a>
						<?php } ?>
								</div>
								<div class="detail">
								<?php if(get_post_meta( get_the_ID(), 'member_skill', true )!=''){ ?>
									<span><?php _e('Skills : ','weblizar'); echo get_post_meta( get_the_ID(), 'member_skill', true ); ?></span>
								<?php }
								if(get_post_meta( get_the_ID(), 'member_exp', true )!=''){ ?>
									<span><?php _e('Experiance : ','weblizar'); echo get_post_meta( get_the_ID(), 'member_exp', true ); ?></span>
								<?php } if(get_post_meta( get_the_ID(), 'member_mail', true )!=''){?>
									<span><?php _e('Email : ','weblizar');  echo get_post_meta( get_the_ID(), 'member_mail', true ); ?></span>
								<?php }
								if(get_post_meta( get_the_ID(), 'member_contact', true )!=''){?>
									<span><?php _e('Contact : ','weblizar'); echo get_post_meta( get_the_ID(), 'member_contact', true ); ?></span>
								<?php } ?>
								</div>
								<?php if(get_post_meta( get_the_ID(), 'member_link', true )!=''){ ?>
								<a href="<?php echo get_post_meta( get_the_ID(), 'member_link', true ); ?>" class="btn" target="_blank"><span class="fa fa-search-plus"></span><?php if(get_post_meta( get_the_ID(), 'member_btn_text', true )!=''){ echo get_post_meta( get_the_ID(), 'member_btn_text', true ); }else{ _e('Live Preview','weblizar'); } ?></a>
								<?php } ?>
							</div>
						</div>
						<?php the_content(); ?>
						<?php endif; ?>
					<?php endwhile; 
				endif; ?>
			</div>
	</div>
</div>
<?php get_footer(); ?>