<?php //Template name: Blog Masonry Right Sidebar
get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space">
	<div class="container masanary">
<!-- Right Start -->
			<div id="wrapper">
				<div class="container2">
					<div class="row">
					<div class="col-md-9 gallery">
					<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$args = array( 'post_type' => 'post', 'post_status'=>'publish', 'paged' => $paged ); 
					$blog_post = new WP_Query( $args );
					if($blog_post->have_posts()): ?>					
						
						<div class="gallery1 no-pad">
						<?php while($blog_post->have_posts()):
								$blog_post->the_post();	?>
							<?php get_template_part('loop'); ?>
						<?php endwhile; ?>
						</div>
						<?php endif; ?>
						<div class="col-md-12 left-pagi">
						<?php if (function_exists("template_pagination")) {
							template_pagination($blog_post->max_num_pages); } ?>
							</div>
						</div>
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
	</div>
</div>
<?php get_footer(); ?>