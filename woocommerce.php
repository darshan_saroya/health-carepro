<?php get_header(); ?>
<div class="cover">
  <div class="container-fluid space hc-cover">
    <div class="container">
      <h1 class="white"><?php global $wp;
				echo $current_url = (add_query_arg(array(),$wp->request)); ?></h1>
      <h4 class="white"><?php if (function_exists('health_care_breadcrumbs')) health_care_breadcrumbs(); ?></h4>
    </div>
  </div>
</div>
<div class="container-fluid space">
	<div class="container blogs">
		<!-- Right Start -->
		<div class="col-md-9 rightside gallery">
			<div id="<?php the_ID(); ?>" <?php post_class();?>>
				<?php woocommerce_content(); ?>
			</div>	
		</div>
		<!-- Right end -->
		<?php get_sidebar('woocommerce'); ?>
	</div>
</div>
<?php get_footer(); ?>