<!-- Sidebar start -->
		<div class="col-md-3 sidebar">
		<?php if ( is_active_sidebar( 'sidebar-primary' ) )
	{ dynamic_sidebar( 'sidebar-primary' );	}
	else  { 
	$args = array(
	'before_widget' => '<div class="row sidebar-widget">',
	'after_widget'  => '</div>',
	'before_title'  => '<div class="line"></div><h4>',
	'after_title'   => '</h4>' );
	the_widget('WP_Widget_Search', null, $args);
	the_widget('WP_Widget_Tag_Cloud', null, $args);
	the_widget('WP_Widget_Archives', null, $args); ?>
	<div class="row sidebar-widget">
			<div class="line"></div>
			<h4>Flicker Gallary</h4>
				<div class="row gallery">
					<div class="col-md-12">
					<?php for($i=1; $i<=9; $i++){ ?>
					<div class="col-xs-4">
						<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/flicker/flicker<?php echo $i; ?>.jpg" alt="" />
					</div>
				<?php } ?>
					</div>
				</div>
		</div>
	<?php } ?>
	</div>
<!-- side bar end -->