<!-- Doctors Start -->
<?php $health_data= health_care_get_options(); ?>
<span class="anchor" id="deppartment-section"></span>
	<div class="container-fluid space">
		<div class="container doctors">
			<?php if($health_data['member_heading']!=''){ ?>
		<h1 class="color"><?php echo $health_data['member_heading']; ?></h1>
			<?php if($health_data['member_icon']!=''){ ?>
			<div class="ln2 color"></div>
			<span class="<?php echo $health_data['member_icon']; ?> color heart"></span>
			<div class="ln3 color"></div>
		<?php } } ?>
		<div class="button-group ">
		<?php $args = array( 'post_type' => 'hc_deptts','posts_per_page' =>-1);
				$member_cats= new WP_Query($args); 
				if( $member_cats->have_posts() ){ ?>
		  <button class="button is-checked" id="hc_all" data-filter="*"><?php _e('All Department','weblizar'); ?></button>
		  <?php while ( $member_cats->have_posts() ) : $member_cats->the_post(); ?> 
		  <button class="button" id="<?php echo str_replace(' ', '_', get_the_title()); ?>"><?php echo get_the_title(); ?></button>
		  <?php endwhile; 
				}else{?>
				<button class="button is-checked" id="hc_all" data-filter="*"><?php _e('All Department','weblizar'); ?></button>
				<?php for($i=1; $i<=6; $i++){ ?>
				<button class="button" id="deppt_<?php echo $i; ?>">Department <?php echo $i; ?></button>
				<?php } } ?>
		</div>
		<?php $args = array( 'post_type' => 'hc_member','posts_per_page' =>-1);
				$mambers= new WP_Query($args); 
				if( $mambers->have_posts() ){ ?>
		<div class="home-docs">	
			 <?php while ( $mambers->have_posts() ) : $mambers->the_post();
			  $member_cat= get_post_meta( get_the_ID(), 'member_cat', true ); 
			  $cats=explode(" ,",$member_cat); ?>
			   <?php if(has_post_thumbnail()): ?>
			<div class="element-item col-md-4 hc_display  col-sm-6 wl-gallery <?php foreach($cats as $cat){ echo str_replace(' ', '_', $cat).' '; } ?>" data-category="transition" >
				<h3><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h3>
				<div class="img-thumbnail">
					<?php $defalt_arg =array('class'=>"img-responsive home_member_thumb");
					the_post_thumbnail('home_member_thumb', $defalt_arg);
					?>
					<div class="overlay">
						<h4><a class="hc-member-title" href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h4>
						<h5><?php  foreach($cats as $cat){ echo $cat.' '; } ?></h5>
						<?php $member_exp = sanitize_text_field( get_post_meta( get_the_ID(), 'member_exp', true ));
							if($member_exp!=''){?>
						<button class="" type="button"><?php echo $member_exp ; ?></button>
							<?php } ?>
						<a class="btn" <?php if(get_post_meta( get_the_ID(), 'member_link', true )!=''){ ?> href="<?php echo get_post_meta( get_the_ID(), 'member_link', true ); ?>" target="_blank" <?php }else{ ?> href="<?php the_permalink(); ?>"<?php } ?> ><?php _e('View Profile','weblizar'); ?></a>
							<p class="member_social">
							<?php $member_twitter = sanitize_text_field( get_post_meta( get_the_ID(), 'member_twitter', true ));
								if($member_twitter!=''){ ?>
						<a href="<?php echo $member_twitter; ?>"><span class="fa fa-twitter icon"></span></a>
								<?php } 
						$member_fb = sanitize_text_field( get_post_meta( get_the_ID(), 'member_fb', true ));
						if($member_fb!=''){ ?>
						<a href="<?php echo $member_fb; ?>"><span class="fa fa-facebook icon"></span></a>
						<?php } 
						$member_google = sanitize_text_field( get_post_meta( get_the_ID(), 'member_google', true ));
						if($member_google!=''){ ?>
						<a href="<?php echo $member_google; ?>"><span class="fa fa-google-plus icon"></span></a>
						<?php } ?>						
						</p>
					</div>
				</div>
			</div>
			<?php endif;
			endwhile; ?>
		</div>
				<?php }else{ ?>
			<div class="home-docs">
			<?php for($i=1; $i<=6; $i++){ ?>			
				<div class="element-item col-md-4 hc_display  col-sm-6 wl-gallery deppt_<?php echo $i; ?>" data-category="transition" >
					<h3>GERARD BUTLER</h3>
					<div class="img-thumbnail">
						<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/members/doc<?php echo $i; ?>.jpg" alt=""/>
						<div class="overlay">
							<h4>Cardiac Doctor</h4>
							<h5>The Speciality of the doctor is that she never say no to patient.</h5>
							<button class="" type="button">20 Year Experience</button>
							<button class="btn" type="button">View Profile</button>
							<a href=""><span class="fa fa-twitter icon"></span></a>
							<a href=""><span class="fa fa-facebook icon"></span></a>
							<a href=""><span class="fa fa-google-plus icon"></span></a>	
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
				<?php } ?>
		</div>
	</div>
<!-- Doctors End -->
<script>
jQuery(window).load(function(){
	jQuery('.button-group .button').click(function() {
		var data= jQuery(this).attr('id');
		jQuery('.button-group .button').removeClass('is-checked');
		jQuery(this).addClass('is-checked');
		jQuery('.element-item').removeClass('hc_display').fadeOut();
		jQuery('.element-item.'+data).addClass('hc_display').fadeIn();
});
	jQuery('#hc_all').click(function() {
		jQuery('.element-item').addClass('hc_display');
});
});
</script>