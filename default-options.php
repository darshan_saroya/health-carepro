<?php
//Default Options
function health_care_default_settings()
{
	$logo_img= get_template_directory_uri().'/images/Healthcare-logo.png';
	$cover_bg= get_template_directory_uri().'/images/back4.jpg';
	$department_img= get_template_directory_uri().'/images/department.jpg';
	$facts_background= get_template_directory_uri().'/images/banner.jpg';
	$testi_background= get_template_directory_uri().'/images/back1.jpg';
	$portfolio_img1= get_template_directory_uri().'/images/portfolio/1.jpg';
	$portfolio_img2= get_template_directory_uri().'/images/portfolio/2.jpg';
	$portfolio_img3= get_template_directory_uri().'/images/portfolio/3.jpg';
	$portfolio_img4= get_template_directory_uri().'/images/portfolio/4.jpg';
	$client_img1= get_template_directory_uri().'/images/clients/c1.png';
	$client_img2= get_template_directory_uri().'/images/clients/c2.png';
	$client_img3= get_template_directory_uri().'/images/clients/c3.png';
	$client_img4= get_template_directory_uri().'/images/clients/c4.png';
	$client_img5= get_template_directory_uri().'/images/clients/c3.png';
	$client_img6= get_template_directory_uri().'/images/clients/c2.png';
	$default_bg= get_template_directory_uri().'/images/bg/16.jpg';
	$wl_theme_options=array(
//Ganeral Settings Options
		'front_page' =>'on',
		'custom_cover' =>'off',
		'upload_image_logo' =>$logo_img,
		'cover_bg_image' =>$cover_bg,
		'logo_height' =>'80',
		'logo_width' =>'250',
		'upload_image_favicon' =>'',
		'google_analytics' =>'',
		'custom_css' =>'',
		'fix_header' =>'on',
//slider options
		'slide_pause' => 'on',
		'slide_cycle' => 'on',
		'slide_interval' => 5000,
		'slide_autoplay' => 'on',
		'slide_height' => 650,

// Service Section Options
		'service_heading' =>'Our Services',
		'service_desc' =>'We Provide all services in our hospital. The Services quality is so well known in midleeat countries.',
		'service_layout' =>3,
		'service_count' =>6,
		'service_icon' =>'fa fa-heart',
		
//call out options	
		'callout_text' => __('Welcome to Health Care','weblizar'),
		'callout_link' => '#',
		'call_out_button_target' => 'on',
		'callout_button' => __('Enter','weblizar'),
		
//Portfolio Default Options
		'portfolio_heading' =>'Our Gallery',
		'portfolio_desc' =>'Dolor Amet Sit Ipsum Lorem.',
		'portfolio_icon' =>'fa fa-heart',
		
//Blog Settings
		'blog_heading' =>'Latest News',
		'blog_desc' =>'We Provide all services in our hospital. The Services quality is so well known in midleeat countries.',
		'blog_icon' =>'fa fa-heart',
		'blog_slide_item' =>5,
//Testimonial Settings
		'testi_heading' =>'What Our Clients Say',
		'testi_icon' =>'fa fa-heart',
		'testi_num' =>5,
		'testi_background' => $testi_background,
//Client Settings
		'client_heading' =>'Our Clients',
		'client_desc' =>'I am realy satisfied with services of Healthsure Medical Service All the staffs are very friendly and helpful.',
		'client_icon' =>'fa fa-heart',
		'client_count' =>8,
//team Settings
		'team_heading' =>'Our Teams',
		'team_desc' =>'I am realy satisfied with services of Healthsure Medical Service All the staffs are very friendly and helpful.',
//gallery settings
		'gallery_heading' => 'Our Gallary',
		'gallery_desc' => 'Dolor Amet Sit Ipsum Lorem.',
		'gallery_icon' => 'fa fa-heart',
		'gallery_column' => 4,
		'gallery_count' => 8,
//deptt settings
		'deptt_heading' => 'Departments',
		'deptt_desc' => 'We Are Trusted And Riliable.',
		'deptt_icon' => 'fa fa-heart',
		'deptt_background' => $department_img,
		
//member settings
		'member_heading' =>'Meet Our Doctors',
		'member_icon'=>'fa fa-heart',

//fun-facts settings
		'facts_heading' =>'Our Interesting Facts',
		'facts_icon' =>'fa fa-heart',
		'facts_desc' =>'I am realy satisfied with services of Healthsure Medical Service All the staffs are very friendly and helpful.',
		'facts_background' =>$facts_background,
		'fact_icon_1' =>'fa fa-cogs',
		'fact_value_1'=>'720',
		'fact_name_1' =>'Our Machine',
		'fact_icon_2' =>'fa fa-home',
		'fact_value_2'=>'127',
		'fact_name_2' =>'Our Room',
		'fact_icon_3' =>'fa fa-thumbs-up',
		'fact_value_3'=>'2660',
		'fact_name_3' =>'Facebook Likes',
		'fact_icon_4' =>'fa fa-users',
		'fact_value_4'=>'240',
		'fact_name_4' =>'Our Staff',
		
//feedback Settings
		'feedback_mail' =>'',
		'feedback_heading' =>'Book Appointament',
		'feedback_icon' =>'fa fa-calendar',
		'feedback_btn' =>'Booking Appointament',
		
//Social Settings
		'header_social_icon' =>'on',
		'footer_social_icon' =>'on',
		'social_address' =>__('Address','weblizar'),
		'social_phone' =>__('+1234567890','weblizar'),
		'social_email' =>__('admin@site.com','weblizar'),
		'social_timing' =>__(' Opening Hours: Monday To Saturday - 8am To 9pm','weblizar'),
		'footer_copyright_text' =>__('Copyright 2016 Designed By','weblizar'),
		'footer_link' =>'https://weblizar.com',
		'footer_link_text' =>'Weblizar',
		'social_icon_1' =>'fa fa-facebook',
		'social_icon_2' =>'fa fa-twitter',
		'social_icon_3' =>'fa fa-envelope',
		'social_icon_4' =>'fa fa-instagram',
		'social_icon_5' =>'fa fa-google',
		'social_icon_6' =>'fa fa-rss',
		'social_link_1' =>'#',
		'social_link_2' =>'#',
		'social_link_3' =>'#',
		'social_link_4' =>'#',
		'social_link_5' =>'#',
		'social_link_6' =>'#',
// contact options
		'contact_detail_heading' => __('Contact Us','weblizar'),
		'contact_icon_1' => 'fa fa-map-marker',
		'contact_icon_2' => 'fa fa-phone',
		'contact_icon_3' => 'fa fa-map-marker',
		'contact_icon_4' => 'fa fa-envelope',
		'contact_heading_1' => __('Address','weblizar'),
		'contact_heading_2' => __('Phone','weblizar'),
		'contact_heading_3' => __('Headquarter','weblizar'),
		'contact_heading_4' => __('Email','weblizar'),
		'contact_detail_1' => __('Honey Buiseness Twenty Fourth St, Los Angeles, USA','weblizar'),
		'contact_detail_2' => __('+91 9876543210, 1234567890','weblizar'),
		'contact_detail_3' => __('Joney Buiseness Twenty Fourth Angeles, 1 USA, Earth, Milkyway Galaxy.','weblizar'),
		'contact_detail_4' => __('weblizar@gmail.com','weblizar'),
		'contact_form_heading' => __('Send Your Message','weblizar'),
		'contact_form_button' => __('Send Message','weblizar'),
		'contact_footer_icon' => 'fa fa-phone',
		'contact_footer_text' => __('EMERGENCY CALL','weblizar'),
		'contact_footer_number' => __('+0 578 878 8543','weblizar'),
		'google_map_url' => 'https://www.google.com/maps/embed?pb=!1m24!1m8!1m3!1d19867.645669670597!2d-0.08854!3d51.504855!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0x487604a931761995%3A0x1ffc6f23e06b9be7!2sTate+Modern%2C+Bankside%2C+London+SE1+9TG%2C+United+Kingdom!3m2!1d51.5075953!2d-0.0993564!4m5!1s0x48760349331f38dd%3A0xa8bf49dde1d56467!2sTower+of+London%2C+London+EC3N+4AB%2C+United+Kingdom!3m2!1d51.508112399999995!2d-0.0759493!5e0!3m2!1sen!2sus!4v1443011812035',
//Extra options/option
		'home_page_layout'=>array('callout','services','deptt','doctor','testimonial','blog','gallery','facts','clients','appointment'),
		
//Skin Layout options
		'theme_color' =>'#0098ff',
		'site_layout' =>'wide',
		'site_bg' => $default_bg,
		'custom_bg' =>'off',

//custom URLs
		'cpt_deptt' => 'hc_deptts',
		'cpt_service' => 'hc_services',
		'cpt_portfolio' => 'hc_portfolios',
		'cpt_testimonial' => 'hc_testimonials',
		'cpt_member' => 'hc_member',

//Maintainance Mode
		'maintenance_switch' => 'off',
		'maintenance_text' =>__('Site Under Construction','weblizar'),
		'maintenance_social' => 'on',
		'maintenance_date' => '',
		'maintenance_image' => $facts_background,
		'maintenance_video' => 'https://www.youtube.com/embed/NQKC24th90U',
		);
	return apply_filters( 'health_pro_options', $wl_theme_options );
}

function health_care_get_options() {
    // Options API
    return wp_parse_args( get_option( 'health_pro_options', array() ), health_care_default_settings() );    
}

function hc_general_setting()
{ 
	$logo_img= get_template_directory_uri().'/images/Healthcare-logo.png';
  $cover_bg= get_template_directory_uri().'/images/back4.jpg';
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['upload_image_logo']= $logo_img;
	$wl_theme_options['cover_bg_image']= $cover_bg;
	$wl_theme_options['front_page']= 'on';
	$wl_theme_options['custom_cover']= 'off';
	$wl_theme_options['logo_height']= '80';
	$wl_theme_options['logo_width']= '250';
	$wl_theme_options['upload_image_favicon']= '';
	$wl_theme_options['google_analytics']= '';
	$wl_theme_options['custom_css']= '';
	$wl_theme_options['fix_header']= 'on';
	update_option('health_pro_options', $wl_theme_options);
}

function hc_slider_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['slide_pause']= 'on';
	$wl_theme_options['slide_cycle']= 'on';
	$wl_theme_options['slide_interval']= '5000';
	$wl_theme_options['slide_autoplay']= 'on';
	$wl_theme_options['slide_height']= '650';
	update_option('health_pro_options', $wl_theme_options);
}

function hc_service_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['service_heading']= 'Our Services';
	$wl_theme_options['service_desc']= 'We Provide all services in our hospital. The Services quality is so well known in midleeat countries.';
	$wl_theme_options['service_layout']= '3';
	$wl_theme_options['service_count']= '6';
	$wl_theme_options['service_icon']= 'fa fa-heart';
	update_option('health_pro_options', $wl_theme_options);
}

function hc_portfolio_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['gallery_heading']= 'Our Gallary';
	$wl_theme_options['gallery_desc']= 'Dolor Amet Sit Ipsum Lorem.';
	$wl_theme_options['gallery_icon']= 'fa fa-heart';
	$wl_theme_options['gallery_column']= '4';
	$wl_theme_options['gallery_count']= '8';
	update_option('health_pro_options', $wl_theme_options);
}

function hc_blog_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['blog_heading']= 'Latest News';
	$wl_theme_options['blog_desc']= 'We Provide all services in our hospital. The Services quality is so well known in midleeat countries.';
	$wl_theme_options['blog_icon']= 'fa fa-heart';
	$wl_theme_options['blog_slide_item']= '5';
	update_option('health_pro_options', $wl_theme_options);
}

function hc_testi_setting(){
	$testi_background= get_template_directory_uri().'/images/back1.jpg';
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['testi_heading']= 'What Our Clients Say';
	$wl_theme_options['testi_icon']= 'fa fa-heart';
	$wl_theme_options['testi_num']= '5';
	$wl_theme_options['testi_background']= $testi_background;
	update_option('health_pro_options', $wl_theme_options);
}

 function health_pro_options(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['client_heading']= 'Our Clients';
	$wl_theme_options['client_desc']= 'I am realy satisfied with services of Healthsure Medical Service All the staffs are very friendly and helpful.';
	$wl_theme_options['client_icon']= 'fa fa-heart';
	$wl_theme_options['client_count']= '8';
	update_option('health_pro_options', $wl_theme_options);
 }
 
function hc_callout_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['callout_text']= 'Welcome to Health Care';
	$wl_theme_options['callout_link']= '#';
	$wl_theme_options['call_out_button_target']= 'on';
	$wl_theme_options['callout_button']= 'Enter';
	update_option('health_pro_options', $wl_theme_options);
}

function hc_deppt_setting(){
	$department_img= get_template_directory_uri().'/images/department.jpg';
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['deptt_heading']= 'Departments';
	$wl_theme_options['deptt_desc']= 'We Are Trusted And Riliable.';
	$wl_theme_options['deptt_icon']= 'fa fa-heart';
	$wl_theme_options['deptt_background']= $department_img;
	update_option('health_pro_options', $wl_theme_options);
}

function hc_member_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['member_heading']= 'Meet Our Doctor';
	$wl_theme_options['member_icon']= 'fa fa-heart';
	update_option('health_pro_options', $wl_theme_options);
}

function hc_facts_setting(){
	$facts_background= get_template_directory_uri().'/images/banner.jpg';
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['facts_heading']= 'Our Interesting Facts';
	$wl_theme_options['facts_icon']= 'fa fa-heart';
	$wl_theme_options['facts_desc']= 'I am realy satisfied with services of Healthsure Medical Service All the staffs are very friendly and helpful.';
	$wl_theme_options['fact_icon_1']= 'fa fa-cogs';
	$wl_theme_options['fact_value_1']= '720';
	$wl_theme_options['fact_name_1']= 'Our Machine';
	$wl_theme_options['fact_icon_2']= 'fa fa-home';
	$wl_theme_options['fact_value_2']= '127';
	$wl_theme_options['fact_name_2']= 'Our Room';
	$wl_theme_options['fact_icon_3']= 'fa fa-thumbs-up';
	$wl_theme_options['fact_value_3']= '2660';
	$wl_theme_options['fact_name_3']= 'Facebook Likes';
	$wl_theme_options['fact_icon_4']= 'fa fa-users';
	$wl_theme_options['fact_value_4']= '240';
	$wl_theme_options['fact_name_4']= 'Our Staff';
	$wl_theme_options['facts_background']= $facts_background;
	update_option('health_pro_options', $wl_theme_options);
}

function hc_feedback_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['feedback_mail']= '';
	$wl_theme_options['feedback_heading']= 'Book Appointament';
	$wl_theme_options['feedback_icon']= 'fa fa-calendar';
	$wl_theme_options['feedback_btn']= 'Booking Appointament';
	update_option('health_pro_options', $wl_theme_options);
	}
	
function hc_skin_layout_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$default_bg= get_template_directory_uri().'/images/bg/16.jpg';
	$wl_theme_options['theme_color']= '#0098ff';
	$wl_theme_options['site_layout']= 'wide';
	$wl_theme_options['site_bg']= $default_bg;
	$wl_theme_options['custom_bg']= 'off';
	update_option('health_pro_options', $wl_theme_options);
}

function hc_contact_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['contact_detail_heading']= __('Contact Us','weblizar');
	$wl_theme_options['contact_icon_1']='fa fa-map-marker' ;
	$wl_theme_options['contact_icon_2']= 'fa fa-phone';
	$wl_theme_options['contact_icon_3']= 'fa fa-map-marker';
	$wl_theme_options['contact_icon_4']= 'fa fa-envelope';
	$wl_theme_options['contact_heading_1']= __('Address','weblizar');
	$wl_theme_options['contact_heading_2']= __('Phone','weblizar');
	$wl_theme_options['contact_heading_3']= __('Headquarter','weblizar');
	$wl_theme_options['contact_heading_4']= __('Email','weblizar');
	$wl_theme_options['contact_detail_1']= __('Honey Buiseness Twenty Fourth St, Los Angeles, USA','weblizar');
	$wl_theme_options['contact_detail_2']= __('+91 9876543210, 1234567890','weblizar');
	$wl_theme_options['contact_detail_3']= __('Joney Buiseness Twenty Fourth Angeles, 1 USA, Earth, Milkyway Galaxy.','weblizar');
	$wl_theme_options['contact_detail_4']= __('weblizar@gmail.com','weblizar');
	$wl_theme_options['contact_form_heading']= __('Send Your Message','weblizar');
	$wl_theme_options['contact_form_button']=  __('Send Message','weblizar');
	$wl_theme_options['contact_footer_icon']= 'fa fa-phone';
	$wl_theme_options['contact_footer_text']= __('EMERGENCY CALL','weblizar');
	$wl_theme_options['contact_footer_number']= __('+0 578 878 8543','weblizar');
	$wl_theme_options['google_map_url']= 'https://www.google.com/maps/embed?pb=!1m24!1m8!1m3!1d19867.645669670597!2d-0.08854!3d51.504855!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0x487604a931761995%3A0x1ffc6f23e06b9be7!2sTate+Modern%2C+Bankside%2C+London+SE1+9TG%2C+United+Kingdom!3m2!1d51.5075953!2d-0.0993564!4m5!1s0x48760349331f38dd%3A0xa8bf49dde1d56467!2sTower+of+London%2C+London+EC3N+4AB%2C+United+Kingdom!3m2!1d51.508112399999995!2d-0.0759493!5e0!3m2!1sen!2sus!4v1443011812035';
	update_option('health_pro_options', $wl_theme_options);
}
//footer options	
function hc_footer_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['footer_copyright_text']= __('Copyright 2016 Designed By','weblizar');
	$wl_theme_options['footer_link']= 'https://weblizar.com';
	$wl_theme_options['footer_link_text']= 'Weblizar';
	update_option('health_pro_options', $wl_theme_options);
}

//social Options
function hc_social_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['header_social_icon']= 'on';
	$wl_theme_options['footer_social_icon']= 'on';
	$wl_theme_options['social_email']= __('admin@site.com','weblizar');
	$wl_theme_options['social_phone']= __('+1234567890','weblizar');
	$wl_theme_options['social_timing']= __(' Opening Hours: Monday To Saturday - 8am To 9pm','weblizar');
	$wl_theme_options['social_icon_1']= 'fa fa-facebook';
	$wl_theme_options['social_icon_2']= 'fa fa-twitter';
	$wl_theme_options['social_icon_3']= 'fa fa-envelope';
	$wl_theme_options['social_icon_4']= 'fa fa-instagram';
	$wl_theme_options['social_icon_5']= 'fa fa-google';
	$wl_theme_options['social_icon_6']= 'fa fa-rss';
	for($i=1; $i<=6; $i++){
	$wl_theme_options['social_link_'.$i]= '#';
	}
	update_option('health_pro_options', $wl_theme_options);
}

//custom Links
function hc_cptlinks(){
	$wl_theme_options = get_option('health_pro_options');
	$wl_theme_options['cpt_deptt']= 'hc_deptts';
	$wl_theme_options['cpt_service']= 'hc_services';
	$wl_theme_options['cpt_portfolio']= 'hc_portfolios';
	$wl_theme_options['cpt_testimonial']= 'hc_testimonials';
	$wl_theme_options['cpt_member']= 'hc_member';
	update_option('health_pro_options', $wl_theme_options);
	
}
function hc_maintenance_setting(){
	$wl_theme_options = get_option('health_pro_options');
	$facts_background= get_template_directory_uri().'/images/banner.jpg';
	$wl_theme_options['maintenance_switch']= 'off';
	$wl_theme_options['maintenance_text']= __('Site Under Construction','weblizar');
	$wl_theme_options['maintenance_date']= '';
	$wl_theme_options['maintenance_social']= 'on';
	$wl_theme_options['maintenance_video']= 'https://www.youtube.com/embed/NQKC24th90U';
	$wl_theme_options['maintenance_image']= $facts_background;
	update_option('health_pro_options', $wl_theme_options);
}