<?php //Template name: Blog With Right Sidebar
get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space">
	<div class="container blogs">

<!-- Right Start -->
<div class="col-md-9 rightside gallery">
<div <?php post_class();?>>
				<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$args = array( 'post_type' => 'post', 'post_status'=>'publish', 'paged' => $paged );
					$blog_post = new WP_Query( $args );
					if($blog_post->have_posts()): $i=6;
					while($blog_post->have_posts()):
					$blog_post->the_post(); ?>
					<?php get_template_part('post','content'); ?>
					<?php endwhile; 
				endif; ?>
				<div class="col-md-12 left-pagi">
						<?php if (function_exists("template_pagination")) {
							template_pagination($blog_post->max_num_pages); } ?>
							</div>
	</div>	
</div>
<!-- Right end -->
<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>