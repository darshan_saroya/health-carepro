<?php get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space">
	<div class="container blogs">
	<!-- Right Start -->
<div class="col-md-9 rightside gallery">
<div <?php post_class();?>>
				<?php if ( have_posts()){ 
					while ( have_posts() ): the_post(); ?>
					<?php get_template_part('post','content'); ?>
					<?php endwhile; 
				}else{ ?>
					<div class="error">
			<h2><span class="fa fa-exclamation-circle"></span> <?php _e('Sorry','weblizar'); ?></h2>
			<h3><?php _e('No Posts found','weblizar'); ?></h3>
			<a href="<?php echo esc_url(home_url( '/' )); ?>" class="btn"><?php _e('Go back to homepage','weblizar'); ?></a>
</div>
				<?php } ?>
				<?php health_care_navigation(); ?>
	</div>	
</div>
<!-- Right end -->
<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>