<?php $health_data = health_care_get_options(); 
?>
<style>
.breadcrumb li a:hover{
	color:<?php echo $health_data['theme_color']; ?> ;
}
.navbar-brand:hover{
	color:<?php echo $health_data['theme_color']; ?> ;
}

.navbar-collapse .navbar-nav li a:hover::before, .navbar-collapse .navbar-nav li a:focus::before,
.navbar-collapse .navbar-nav > .active > a:focus::before, .navbar-collapse  .navbar-nav > .active > a:hover::before{
	color:<?php echo $health_data['theme_color']; ?> ;
} 

.top a:hover{
    color:<?php echo $health_data['theme_color']; ?>  !important;
}

.navbar-collapse .navbar-nav > .active > a, .navbar-collapse .navbar-nav > .active > a:focus, .navbar-collapse  .navbar-nav > .active > a:hover {
	color: <?php echo $health_data['theme_color']; ?> !important ;
}
.dropdown-menu > .active > a, .dropdown-menu > .active > a:focus, .dropdown-menu > .active > a:hover {
  background-color: <?php echo $health_data['theme_color']; ?> !important;
}
.navbar-collapse .navbar-nav > li > a:focus, .navbar-collapse .navbar-nav > li > a:hover{
	color:<?php echo $health_data['theme_color']; ?> !important;
}
.dropdown-menu > li > a:hover{
background-color:<?php echo $health_data['theme_color']; ?> !important ;
}
.nav .open > a, .nav .open > a:focus, .nav .open > a:hover{
color:<?php echo $health_data['theme_color']; ?> !important;
}
.dropdown-menu  .open > a, .dropdown-menu  .open > a:focus, .dropdown-menu  .open > a:hover{
background-color:<?php echo $health_data['theme_color']; ?> !important ;
}
.carousel-control .glyphicon-chevron-left, .carousel-control .icon-prev{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
 .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.health-dept .btn ,
  .health-dept .btn:hover{
  background-color: <?php echo $health_data['theme_color']; ?> ;
}
.slider-caption{
border-color:<?php echo $health_data['theme_color']; ?> !important;
}
.carousel-caption .btn{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.carousel-indicators li{
border: 1px solid <?php echo $health_data['theme_color']; ?> ;
}
.carousel-indicators .active{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.text .link{
    background-color: <?php echo $health_data['theme_color']; ?> ;
}
.comment .col-xs-2 img {
    border: 5px solid <?php echo $health_data['theme_color']; ?> ;
}
.doctors .img-thumbnail .overlay button {
  color:<?php echo $health_data['theme_color']; ?> ;
}
.testimony{
    background-color: <?php echo $health_data['theme_color']; ?> ;
}
.swiper-pagination-bullet-active{
	background-color: <?php echo $health_data['theme_color']; ?> !important;
    border: 1px solid <?php echo $health_data['theme_color']; ?> !important;
}
.news   .owl-theme .owl-controls .owl-buttons div{
  background-color:<?php echo $health_data['theme_color']; ?> ;
  }
.news .owl-theme .owl-controls .owl-page.active span, .news .owl-theme .owl-controls.clickable .owl-page:hover span{
  background-color: <?php echo $health_data['theme_color']; ?> ;
  }
.news .overlay,
.gallerys  .overlay,
.widget-footer .overlay,
.widget-footer .col-xs-4 .overlay,
.sidebar-widget .col-xs-3 .overlay,
 .sidebar-widget .pics .overlay,
 .sidebar-widget .col-xs-4 .overlay,
 .rightside .right-widget .img-thumbnail  .overlay,
 .masanary .masanary-pic .img-thumbnail .overlay,
 .rightside .blog-desc .img-thumbnail  .overlay,
 .rightside .posts .pics .img-thumbnail .overlay,
 .portfolio .element-item .overlay,
 .doctors  .img-thumbnail .overlay,
 .portfolio-details .related-post .overlay,
 .grid-item .overlay{
background:<?php echo $health_data['theme_color']; ?> ;
}
.news .overlay a:hover,
.gallerys  .overlay a:hover,
 .sidebar-widget .pics .overlay a:hover,
 .rightside .right-widget .img-thumbnail .overlay a:hover,
 .masanary .masanary-pic  .img-thumbnail .overlay a:hover,
 .rightside .blog-desc .img-thumbnail .overlay a:hover,
 .rightside .posts .pics .img-thumbnail .overlay a:hover,
 .portfolio .element-item .overlay a:hover,
 .doctors  .img-thumbnail .overlay a:hover{
color:<?php echo $health_data['theme_color']; ?> !important;
}
  .space .pagination > .active > a, .space .pagination > .active > a:focus, .space .pagination > .active > a:hover, .space .pagination > .active > span, .space .pagination > .active > span:focus, .space .pagination > .active > span:hover {
  background-color: <?php echo $health_data['theme_color']; ?> ;
  border-color: <?php echo $health_data['theme_color']; ?> ;
}
.blogs .rightside  .author .col-xs-2{
  border: 5px solid <?php echo $health_data['theme_color']; ?> ;
  }
  .blogs .rightside .line{
border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.avatar {
  border: 2px solid <?php echo $health_data['theme_color']; ?> ;
}
.comment a{
color: <?php echo $health_data['theme_color']; ?> ;
}
.feedback  form .btn{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.emergency .icon{
 background-color:<?php echo $health_data['theme_color']; ?> ;
}
.doctors .button{
border:1px solid <?php echo $health_data['theme_color']; ?> ;
}
.doctors   .img-thumbnail  .overlay .btn{
color:<?php echo $health_data['theme_color']; ?> !important;
}
 .department .tab-content .col-md-6 .line {
    border: 2px solid <?php echo $health_data['theme_color']; ?> ;
}
.department .tab-content .col-md-4 .icon {
    color: <?php echo $health_data['theme_color']; ?> ;
}
.department  .tab-content .btn {
    background-color: <?php echo $health_data['theme_color']; ?> ;
}
.btn,
.hc_tags a{
border:1px solid <?php echo $health_data['theme_color']; ?> !important;
}
.hc_tags a:hover{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.color{
color:<?php echo $health_data['theme_color']; ?> ;
}
.services .heart,
.services .ln2,
.services .ln3{
color:<?php echo $health_data['theme_color']; ?> ;;
}
.service .icon{
color: <?php echo $health_data['theme_color']; ?> ;
}
.pics .img-thumbnail{
 border-color:<?php echo $health_data['theme_color']; ?> ;
}
.news .date a:hover{
  color:<?php echo $health_data['theme_color']; ?> ;
  }
  .news .category a:hover{
  color:<?php echo $health_data['theme_color']; ?> ;
  }
  .news .btn,
  .news .btn:hover{
background-color:<?php echo $health_data['theme_color']; ?> ;
  }
   .news .overlay a:hover{
  color:<?php echo $health_data['theme_color']; ?> !important;
    border:2px solid <?php echo $health_data['theme_color']; ?> ;
  }
  .gallerys .overlay  a:hover{
  color:<?php echo $health_data['theme_color']; ?> !important;
    border:2px solid <?php echo $health_data['theme_color']; ?> ;
  }
  .appoinment{
  background-color:<?php echo $health_data['theme_color']; ?> ;
  }
.footer .widget-footer a:hover{
color:<?php echo $health_data['theme_color']; ?> ;
}
.tagcloud a:hover {
  border: 1px solid <?php echo $health_data['theme_color']; ?> ;
  background-color:<?php echo $health_data['theme_color']; ?>  !important;
}
#wp-calendar caption {
  background-color: <?php echo $health_data['theme_color']; ?> ;
}
.footer-bottom a:hover{
color:<?php echo $health_data['theme_color']; ?> ;
}
.sidebar-widget .tagcloud a {
  border: 1px solid <?php echo $health_data['theme_color']; ?> ;
}
.sidebar-widget .tagcloud a:hover{
  border: 1px solid <?php echo $health_data['theme_color']; ?> ;
}
.sidebar-widget .line{
border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.sidebar-widget .nav a:focus,
	.sidebar-widget .nav a:hover{
	color:<?php echo $health_data['theme_color']; ?> ;
	}
	.sidebar-widget .img-thumbnail .overlay a:hover{
	color:<?php echo $health_data['theme_color']; ?> ;
	border:2px solid <?php echo $health_data['theme_color']; ?> ;
	}
	.sidebar .recent .nav li a{
	background-color:<?php echo $health_data['theme_color']; ?> ;
	}
.rightside a:hover{
color:<?php echo $health_data['theme_color']; ?> ;;
}
.rightside  .blog-desc .img-thumbnail .overlay a:hover{
background-color:#eee;
color:<?php echo $health_data['theme_color']; ?> ;
  border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
  .sidebar .recent .nav-tabs > li.active > a,
  .sidebar .recent .nav-tabs > li.active > a:focus,
  .sidebar .recent  .nav-tabs > li.active > a:hover,
  .sidebar .recent .nav-tabs > li a:hover {
	color:<?php echo $health_data['theme_color']; ?> ;

	}
.bg{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.bg:hover{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.rightside .right-widget .img-thumbnail  .overlay a:hover{
color:<?php echo $health_data['theme_color']; ?> ;
  border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.space .pagination a:hover{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.masanary a:hover{
color:<?php echo $health_data['theme_color']; ?> ;
}
.services .line{
color:<?php echo $health_data['theme_color']; ?> ;
}
.service-icon a{
color:<?php echo $health_data['theme_color']; ?> ;
}
.services .service  a{
color:<?php echo $health_data['theme_color']; ?> ;
}
.services .service a .icon{
border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.services .service a:hover .icon{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.service4 .col-xs-3 .icon{
color:<?php echo $health_data['theme_color']; ?> ;
}
.services .service4  a{
color:<?php echo $health_data['theme_color']; ?> ;
}
.services .service4 a .icon{
border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.services .service4 a:hover .icon{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.portfolio .button{
border:1px solid <?php echo $health_data['theme_color']; ?> ;
}
.portfolio .button:hover,
.portfolio .button-group .button.is-checked{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.portfolio .element-item .overlay a:hover{
color:<?php echo $health_data['theme_color']; ?> !important ;
border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.contact .line{
border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.doctors .button:hover,
.doctors .button-group .button.is-checked{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.blue{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.doctors  .img-thumbnail .overlay a:hover{
color:<?php echo $health_data['theme_color']; ?> !important;
}
.doctors  .img-thumbnail .overlay .btn:hover{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.contact .btn{
background-color:<?php echo $health_data['theme_color']; ?> !important ;
}
.about_doctors .docs{
 border: 2px solid <?php echo $health_data['theme_color']; ?> ;
 }
  .about_doctors  .docs .btn{
   background-color:<?php echo $health_data['theme_color']; ?> ;
   }
    .about_doctors  .docs .name{
	border:2px solid <?php echo $health_data['theme_color']; ?> ;
	color:<?php echo $health_data['theme_color']; ?> ;
   }
	.about_doctors .docs:hover .btn{
	color:<?php echo $health_data['theme_color']; ?> !important ;
	}
.about_doctors  .docs:hover .caption, 
.about_doctors  .docs:hover p,
.about_doctors  .docs:hover a,
.about_doctors .docs:hover .name{
	background-color:<?php echo $health_data['theme_color']; ?>!important ;
}	
.about-department .btn:after {
	color:<?php echo $health_data['theme_color']; ?> ;
 }
  .accordion-details  .collapse.in > .btn-lg  {
  color:<?php echo $health_data['theme_color']; ?> ;
  }
   .about-department .btn .icon{
	background-color: <?php echo $health_data['theme_color']; ?> ;
}
.events .up-event .health-event{
border-bottom:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.events .up-event .dates{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.events .up-event .btn{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.about-appoinment .mobile  .btn{
color:<?php echo $health_data['theme_color']; ?> ;
}
.about-desc blockquote{
border-left:2px solid <?php echo $health_data['theme_color']; ?> ;
border-right:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.about-desc .btn{
color:<?php echo $health_data['theme_color']; ?> ;
}
  .about-doctors .about-doc-details {
  background-color: <?php echo $health_data['theme_color']; ?>;
}
.about-doctors .about-doc-details .img-thumbnail {
    border: 2px solid <?php echo $health_data['theme_color']; ?>;
  }
.about-doctors .button-group .button.button.is-checked {
    color: <?php echo $health_data['theme_color']; ?> ;
}
.about-doctors .button-group .button.button.is-checked .icon{
background-color:#fff;
color:<?php echo $health_data['theme_color']; ?> ;
}
.about-doctors .button-group .button .icon{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.about-doctors .about-doc-details .btn{
color:<?php echo $health_data['theme_color']; ?>;
border:1px solid <?php echo $health_data['theme_color']; ?>; 
}
.about-doctors .about-doc-details:hover .btn{
color:<?php echo $health_data['theme_color']; ?> ;
}
.about-doctors .about-doc-details:hover{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.about-doctors .about-doc-details:hover .view{
color:<?php echo $health_data['theme_color']; ?> ;
}
.about-doctors .about-doc-details:hover .img-thumbnail{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.client{
 background-color:<?php echo $health_data['theme_color']; ?> ;
 }
.abt-appoinment .feed .btn{
color:<?php echo $health_data['theme_color']; ?> ;
}
.line{
border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.doctors-pic .icon:hover {
color:<?php echo $health_data['theme_color']; ?> ;
}
.doctors-pic .btn{
color:<?php echo $health_data['theme_color']; ?> ;
}
.tabs  .tab-content ul li::before{
 color: <?php echo $health_data['theme_color']; ?> ;
}
.tabs .nav > li a{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.tabs .nav-tabs > li.active > a,.tabs  .nav-tabs > li.active > a:focus, .tabs .nav-tabs > li.active > a:hover {
background-color:transparent;
color:<?php echo $health_data['theme_color']; ?> ;
border:2px solid <?php echo $health_data['theme_color']; ?> ;
}
.tabs .nav > li a:hover{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.accordion .accordion-details .type3  .btn-lg {
	background-color:<?php echo $health_data['theme_color']; ?> ;
}
.accordion .accordion-details .type4  .btn-lg {
	background-color:<?php echo $health_data['theme_color']; ?> ;
}
.about-department .btn::after {
    color: <?php echo $health_data['theme_color']; ?> ;
}
.error .btn {
  background-color: <?php echo $health_data['theme_color']; ?> ;
 }
 .exp1 blockquote{
border-left:5px solid <?php echo $health_data['theme_color']; ?> ;
}
.portfolio-pic .detail .icon {
  color:<?php echo $health_data['theme_color']; ?> ;
  }
.portfolio-pic  .btn{
color:<?php echo $health_data['theme_color']; ?>  !important;
border:1px solid <?php echo $health_data['theme_color']; ?> ;
}
.portfolio-pic  .btn:hover{
background-color:<?php echo $health_data['theme_color']; ?> ;
border:1px solid <?php echo $health_data['theme_color']; ?> ;
}
.portfolio-details .pager li > a{
color:<?php echo $health_data['theme_color']; ?> ;
border:1px solid <?php echo $health_data['theme_color']; ?> ;
}
.portfolio-details .pager li > a:hover,
.portfolio-details .pager li > a:focus{
background-color:<?php echo $health_data['theme_color']; ?> ;
}
.portfolio-details .related-post  .overlay a:hover {
  border: 1px solid <?php echo $health_data['theme_color']; ?> ;
  color: <?php echo $health_data['theme_color']; ?> ;
}
.portfolio-details .related-post  .owl-theme .owl-controls .owl-buttons div {
  background-color: <?php echo $health_data['theme_color']; ?> ;
}
.abt-appoinment {
  background-color: <?php echo $health_data['theme_color']; ?> ;
}
.hc_tags a {
  border: 1px solid <?php echo $health_data['theme_color']; ?> ;
}
.sidebar-widget  a:hover { 
color:<?php echo $health_data['theme_color']; ?>  !important;
}
.pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
  border-color: <?php echo $health_data['theme_color']; ?> ;
}
.woocommerce span.onsale{
	background-color:<?php echo $health_data['theme_color']; ?> !important;
}
.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button{
	background-color:<?php echo $health_data['theme_color']; ?> !important;
	border:1px solid <?php echo $health_data['theme_color']; ?> !important;
}
.woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover{
	color:<?php echo $health_data['theme_color']; ?> !important;
	border:1px solid <?php echo $health_data['theme_color']; ?> !important;
}
.stars a,
.stars a.active,
.woocommerce .star-rating{
  color:<?php echo $health_data['theme_color']; ?> ;
}
.woocommerce .woocommerce-message::before,
.woocommerce .woocommerce-info::before{
	color:<?php echo $health_data['theme_color']; ?> !important;
}
.woocommerce .woocommerce-info,
.woocommerce .woocommerce-message {
  border-top-color:<?php echo $health_data['theme_color']; ?> !important;
}
  .pic .pics .overlay a:hover{
   color:<?php echo $health_data['theme_color']; ?> !important;
  }
  .theme-color {
  background-color: <?php echo $health_data['theme_color']; ?>;
}
   .form-control:focus {
  box-shadow: 0 0 2px 1px <?php echo $health_data['theme_color']; ?> !important;
    }
  .home-docs .element-item h3 a{
    color:<?php echo $health_data['theme_color']; ?>;
    }
	.swiper-button-prev,
.swiper-button-next {
  background-color: <?php echo $health_data['theme_color']; ?>;
}
</style>