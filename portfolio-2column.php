<?php //Template name: Portfolio 2 Column
get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid portfolio space">
	<div class="container ">
	<?php
		//for a given post type, return all
		$post_type = 'hc_portfolios';
		$tax = 'hc_portfolio_categories'; 
		$term_args=array( 'hide_empty' => true);
		$tax_terms = get_terms($tax, $term_args);					
		$defualt_tex_id = get_option('portfolio_default_categories_id'); 					
		?>
		<div class="button-group filter-button-group">
			<?php foreach ($tax_terms  as $tax_term) {
				$tax_term_name = str_replace(' ', '_', $tax_term->name);
				$tax_term_name = preg_replace('~[^A-Za-z\d\s-]+~u', 'wl', $tax_term_name);
				?>
      <button class="button" data-filter= ".<?php echo $tax_term_name; ?>"  ><?php echo $tax_term->name; ?></button>
			<?php } ?>
		</div>
		<div class="grid gallery">	
	<?php	$all_posts = wp_count_posts( 'hc_portfolios')->publish;
				$args = array('post_type' => 'hc_portfolios', 'posts_per_page' =>$all_posts);
					global $portfolio;
					$portfolio = new WP_Query( $args );			
					if( $portfolio->have_posts() )
					{	
						while ( $portfolio->have_posts() ) : $portfolio->the_post();							
						$terms_names = wp_get_post_terms($post->ID, "hc_portfolio_categories", array("fields" => "names"));	
					 if(has_post_thumbnail()):
					?>			
			<div class="element-item col-md-6 col-sm-6 wl-gallery icons <?php foreach ($terms_names as $term) {   $tax_term = str_replace(' ', '_', $term);  echo preg_replace('~[^A-Za-z\d\s-]+~u', 'wl', $tax_term). " "; } ?>" data-category="transition" >
				<?php $defalt_arg =array('class'=>"img-responsive home_porfolio_thumb");
					the_post_thumbnail('home_porfolio_thumb',$defalt_arg); ?>
					<div class="overlay">
						<h4><a class="port-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<span class="<?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_icon', true )); ?> icon"></span>
							<div class="ln"></div>
						<h5><?php 	$portfolio_categories_list = get_the_terms( get_the_ID(), 'hc_portfolio_categories' );
								if($portfolio_categories_list){
								$totalcat= count($portfolio_categories_list);
								$i=1;
									foreach($portfolio_categories_list as $list)
									{	echo $list->name;
										if($i <$totalcat){ echo ", ";}
										$i++;
									}  
								} ?></h5>
						<a class="home-port" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"><span class="fa fa-search"></span></a>
						<a href="<?php the_permalink(); ?>"><span class="fa fa-chain"></span></a>
					</div>
			</div>
			<?php endif;
				endwhile;
					}?>
		</div>				
					
	</div>
</div>

<?php get_footer(); ?>