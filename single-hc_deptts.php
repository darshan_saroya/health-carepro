<?php get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space">
	<div class="container deppt-single">
	<!-- Right Start -->
		<div class="col-md-12 rightside gallery">
			<div <?php post_class();?>>	
				<?php if ( have_posts()): 
					while ( have_posts() ): the_post(); 
					 ?>
					<?php if(has_post_thumbnail()): ?>
						<div class="img-thumbnail">
							<?php $defalt_arg =array('class'=>"img-responsive home_deppt_thumb");
							the_post_thumbnail('home_deppt_thumb', $defalt_arg);
							?>
						</div>
					<?php endif; ?>
						<?php the_content();
					endwhile; 
				endif; ?>
			</div>
		</div>
	<!-- Right end -->
	</div>
	<?php $category= get_the_title();
	$args = array( 'post_type' => 'hc_member','posts_per_page' =>-1);
				$members= new WP_Query($args); 
				if( $members->have_posts() ){ ?>
		<div class="container">
		<div class="line"></div>
		<h2><?php _e('Related Members','weblizar'); ?></h2>
		<div class="hc_relatedposts">
			<div id="related-members" class="swiper-wrapper gallery">
			 <?php while ( $members->have_posts() ) : $members->the_post();
			  $member_cat= get_post_meta( get_the_ID(), 'member_cat', true ); 
			  $cats=explode(" ,",$member_cat); 
			foreach($cats as $cat){ ?>
			   <?php if(has_post_thumbnail() && $cat == $category ): ?>
			   <div class="swiper-slide">
			   <div class="item">
			<div class="row related-members" >
				<div class="img-thumbnail">
					<?php $defalt_arg =array('class'=>"img-responsive home_member_thumb");
					the_post_thumbnail('home_member_thumb', $defalt_arg);
					?>
					<div class="overlay">
						<h4><?php the_title(); ?></h4>
						<h5><?php  foreach($cats as $cat){ echo $cat.' '; } ?></h5>
						<a href="<?php the_permalink(); ?>" ><button class="btn" type="button"><?php _e('View Profile','weblizar'); ?></button></a>
						<p class="member_social">
							<?php $member_twitter = sanitize_text_field( get_post_meta( get_the_ID(), 'member_twitter', true ));
								if($member_twitter!=''){ ?>
						<a href="<?php echo $member_twitter; ?>"><span class="fa fa-twitter icon"></span></a>
								<?php } 
						$member_fb = sanitize_text_field( get_post_meta( get_the_ID(), 'member_fb', true ));
						if($member_fb!=''){ ?>
						<a href="<?php echo $member_fb; ?>"><span class="fa fa-facebook icon"></span></a>
						<?php } 
						$member_google = sanitize_text_field( get_post_meta( get_the_ID(), 'member_google', true ));
						if($member_google!=''){ ?>
						<a href="<?php echo $member_google; ?>"><span class="fa fa-google-plus icon"></span></a>
						<?php } ?>
						</p>
					</div>
				</div>
			</div>
			</div>
			</div>
			<?php endif;
			}
			endwhile; ?>
			</div>
			<div class="swiper-button-next swiper-button-white"></div>
			<div class="swiper-button-prev swiper-button-white"></div>
			</div>
		</div>
			  <?php } ?>
</div>
<?php get_footer(); ?>