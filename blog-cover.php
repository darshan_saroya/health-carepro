<!-- Blog Start -->
<div class="cover">
  <div class="container-fluid space hc-cover">
    <div class="container">
      <h1 class="white"><?php if(is_home()){ _e('Home','weblizar'); }else{ the_title(); } ?></h1>
      <div class="blog-link white"><?php if (function_exists('health_care_breadcrumbs')) health_care_breadcrumbs(); ?></div>
    </div>
  </div>
</div>
<!-- Blog End -->