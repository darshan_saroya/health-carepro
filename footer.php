<!-- Footer start -->
<?php $health_data= health_care_get_options(); ?>
<div class="container-fluid footer">
	<div class="container">
	<?php if ( is_active_sidebar( 'footer-widget-area' ) ){ dynamic_sidebar('footer-widget-area');
		} else { 
		$footer_default = array(
	'before_widget' => '<div class="col-md-3 col-sm-6 widget-footer">',
	'after_widget'  => '</div>',
	'before_title' => '<div class="col-md-12 footer-heading"><h3>',
	'after_title' => '</h3><div class="ln white"></div><span class="fa fa-heart hrt"></span></div>',
	);
	the_widget('WP_Widget_Tag_Cloud', null, $footer_default);
	the_widget('WP_Widget_Categories', null, $footer_default);
	the_widget('WP_Widget_Pages', null, $footer_default); ?>
	<div class="col-md-3 col-sm-6 widget-footer gallery">
				<div class="col-md-12 footer-heading">
					<h3>Flicker Gallary</h3>
					<div class="ln white"></div>
					<span class="fa fa-heart hrt"></span>
				</div>
				<div class="row col-md-12 footer-text photo_gallery">
				<?php for($i=1; $i<=9; $i++){ ?>
					<div class="col-xs-4 footer-gallery">
						<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/flicker/flicker<?php echo $i; ?>.jpg" alt="" />
					</div>
				<?php } ?>
				</div>
		</div>
	<?php } ?>	
	</div>
</div>
<div class="container-fluid footer-bottom">
	<div class="container">
		<div class="col-md-12">
			<div class="col-md-6 col-sm-6 footer-copy">
				<?php echo $health_data['footer_copyright_text']; ?>
				<?php if($health_data['footer_link_text']!=''){  ?>
				<a href="<?php echo $health_data['footer_link']; ?>" title="<?php echo $health_data['footer_link_text']; ?>" ><?php echo $health_data['footer_link_text']; ?></a>
				<?php } ?>
			</div>
			<?php if($health_data['footer_social_icon']=='on'){ ?>
			<div class="col-md-6 col-sm-6 footer-social">
				<div class="right-align">
				<?php for($i=1; $i<=6; $i++){
					if($health_data['social_icon_'.$i]!=''){
				?>
				<a href="<?php if($health_data['social_link_'.$i]!=''){ echo $health_data['social_link_'.$i]; }else{ echo '#'; } ?>" target="_blank"><span class="<?php echo $health_data['social_icon_'.$i]; ?> footer-icon"></span></a>
					<?php }
				} ?>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<a href="#top" class="scrol hc_scrollup"><span class="fa fa-angle-up"></span></a>
<?php if($health_data['custom_css']!='') echo '<style>'.$health_data['custom_css'].'</style>'; 
if($health_data['google_analytics']!='') echo '<script>'.$health_data['google_analytics'].'</script>'; 
?>
<?php get_template_part('index', 'footer_switcher'); ?>	
<!-- Footer End -->
<script>
// photobos js
jQuery(function(){
		var gallery = jQuery('.gallery .lightbox').simpleLightbox();
		var gallery1 = jQuery('.gallery .lightbox1').simpleLightbox();
		var gallery2 = jQuery('.gallery .home-blog').simpleLightbox();
		var gallery3 = jQuery('.home-port').simpleLightbox();
		var gallery4 = jQuery('.gallery .related-post').simpleLightbox();
		var gallery5 = jQuery('.gallery .port-single').simpleLightbox();
  var gallery6 = jQuery('.recent .lightbox').simpleLightbox();
	});
</script>
<script>
//Testimonial JS

</script>
<?php wp_footer(); ?>
</div>
</div>
</body>
</html>