<?php
 if ( function_exists( 'add_image_size' ) ) 
 {
  add_image_size('home_post_thumb',480,350,true);
   add_image_size('single_post_thumb',800,300,true);
   add_image_size('mesonry_post_thumb',300,200,true);
   add_image_size('related_post_thumb',300,200,true);
	add_image_size('home_slider_thumb',1920,650,true);
	add_image_size('home_member_thumb',400,400,true);
	add_image_size('home_porfolio_thumb',300,200,true);
	add_image_size('portfolio_detail_thumb',800,400,true);
   add_image_size('service_detail_thumb',800,400,true);
   add_image_size('service_home_thumb',350,200,true);
	add_image_size('home_deppt_thumb',800,400,true);
	add_image_size('home_testi_thumb',300,300,true);
	add_image_size('home_client_thumb',300,200,true);
  add_image_size('single_member_thumb',800,400,true);
   add_image_size('recent_blog_img',50,50,true);
}
// code for image resize for according to image layout
add_filter( 'intermediate_image_sizes', 'hc_image_presets');
function hc_image_presets($sizes){
	if( isset($_POST['post_id']) ){
   $type = get_post_type($_REQUEST['post_id']);	
    foreach($sizes as $key => $value){
		if($type=='hc_slider' && $value != 'home_slider_thumb')
		{ unset($sizes[$key]);  }
		elseif($type=='hc_member' && $value != 'home_member_thumb' && $value != 'single_member_thumb')
		{ unset($sizes[$key]);  }
		elseif($type=='hc_portfolios' && $value != 'home_porfolio_thumb' && $value != 'portfolio_detail_thumb')
		{ unset($sizes[$key]);  }
		elseif($type=='hc_testimonials' && $value != 'home_testi_thumb')
		{ unset($sizes[$key]);  }
		elseif($type=='hc_clients' && $value != 'home_client_thumb')
		{ unset($sizes[$key]);  }
      elseif($type=='hc_services' && $value != 'service_detail_thumb' && $value != 'service_home_thumb')
		{ unset($sizes[$key]);  }
		elseif($type=='hc_deptts' && $value != 'home_deppt_thumb')
		{ unset($sizes[$key]);  }
		elseif($type=='post' && $value != 'home_post_thumb' && $value != 'single_post_thumb' && $value != 'mesonry_post_thumb' && $value != 'related_post_thumb')
		{ unset($sizes[$key]);  }
		}
    return $sizes;
	}
}
?>