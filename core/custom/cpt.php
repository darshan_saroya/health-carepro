<?php $health_data= health_care_get_options();
	$cpt_service = $health_data['cpt_service'];
	$cpt_portfolio = $health_data['cpt_portfolio'];
	$cpt_testimonial = $health_data['cpt_testimonial'];
	$cpt_member = $health_data['cpt_member'];
	$cpt_deptt = $health_data['cpt_deptt'];

/************* Home Service custom post type ***********************/	
function hc_slider_type()
{	register_post_type( 'hc_slider',
		array(
			'labels' => array(
			'name' => __('Healthcare Slider','weblizar'),
			'add_new' => __('Add New Slide', 'weblizar'),
			'add_new_item' => __('Add New Slide','weblizar'),
			'edit_item' => __('Add New Slide','weblizar'),
			'new_item' => __('New Slide','weblizar'),
			'all_items' => __('All Slides','weblizar'),
			'view_item' => __('View Slide','weblizar'),
			'search_items' => __('Search Slides','weblizar'),
			'not_found' =>  __('No Slides found','weblizar'),
			'not_found_in_trash' => __('No Slide found in Trash','weblizar'), 
			),
		'supports' => array('title','thumbnail'),
		'show_in' => true,
		'show_in_nav_menus' => false,	
		'public' => true,
		'publicly_queryable' => false,
		'menu_icon' => 'dashicons-admin-page',
		'exclude_from_search'=> true,
		)
	);
}
add_action( 'init', 'hc_slider_type' );

function hc_service_type()
{	register_post_type( 'hc_services',
		array(
			'labels' => array(
			'name' => __('Healthcare Services','weblizar'),
			'add_new' => __('Add New Service', 'weblizar'),
			'add_new_item' => __('Add New Service','weblizar'),
			'edit_item' => __('Edit Service','weblizar'),
			'new_item' => __('New Service','weblizar'),
			'all_items' => __('All Services','weblizar'),
			'view_item' => __('View Service','weblizar'),
			'search_items' => __('Search Service','weblizar'),
			'not_found' =>  __('No Services found','weblizar'),
			'not_found_in_trash' => __('No Service found in Trash','weblizar'), 
			),
		'supports' => array('title','thumbnail', 'editor'),
		'show_in' => true,
		'show_in_nav_menus' => false,	
		'public' => true,
		'rewrite' => array('slug' => $GLOBALS['cpt_service']),
		'menu_icon' => 'dashicons-admin-page',
		'exclude_from_search'=> true,
		)
	);
}
add_action( 'init', 'hc_service_type' );

function hc_deptt_type()
{	register_post_type( 'hc_deptts',
		array(
			'labels' => array(
			'name' => __('Healthcare Departments','weblizar'),
			'add_new' => __('Add New Department', 'weblizar'),
			'add_new_item' => __('Add New Department','weblizar'),
			'edit_item' => __('Edit Department','weblizar'),
			'new_item' => __('New Department','weblizar'),
			'all_items' => __('All Departments','weblizar'),
			'view_item' => __('View Department','weblizar'),
			'search_items' => __('Search Department','weblizar'),
			'not_found' =>  __('No Departments found','weblizar'),
			'not_found_in_trash' => __('No Department found in Trash','weblizar'), 
			),
		'supports' => array('title','thumbnail', 'editor'),
		'show_in' => true,
		'show_in_nav_menus' => false,	
		'public' => true,
		'rewrite' => array('slug' => $GLOBALS['cpt_deptt']),
		'menu_icon' => 'dashicons-admin-page',
		'exclude_from_search'=> true,
		)
	);
}
add_action( 'init', 'hc_deptt_type' );

function hc_portfolio_type()
{	register_post_type( 'hc_portfolios',
		array(
			'labels' => array(
			'name' => __('Healthcare Portfolios','weblizar'),
			'add_new' => __('Add New Portfolio', 'weblizar'),
			'add_new_item' => __('Add New Portfolio','weblizar'),
			'edit_item' => __('Edit Portfolio','weblizar'),
			'new_item' => __('New Portfolio','weblizar'),
			'all_items' => __('All Portfolios','weblizar'),
			'view_item' => __('View Portfolio','weblizar'),
			'search_items' => __('Search Portfolio','weblizar'),
			'not_found' =>  __('No Portfolios found','weblizar'),
			'not_found_in_trash' => __('No Portfolio found in Trash','weblizar'), 
			),
		'supports' => array('title','thumbnail', 'editor'),
		'show_in' => true,
		'show_in_nav_menus' => false,	
		'public' => true,
		'rewrite' => array('slug' => $GLOBALS['cpt_portfolio']),
		'menu_icon' => 'dashicons-admin-page',
		'exclude_from_search'=> true,
		)
	);
}
add_action( 'init', 'hc_portfolio_type' );

function hc_testimonial_type()
{	register_post_type( 'hc_testimonials',
		array(
			'labels' => array(
			'name' => __('Healthcare Testimonials','weblizar'),
			'add_new' => __('Add New Testimonial', 'weblizar'),
			'add_new_item' => __('Add New Testimonial','weblizar'),
			'edit_item' => __('Edit Testimonial','weblizar'),
			'new_item' => __('New Testimonial','weblizar'),
			'all_items' => __('All Testimonials','weblizar'),
			'view_item' => __('View Testimonial','weblizar'),
			'search_items' => __('Search Testimonial','weblizar'),
			'not_found' =>  __('No Testimonials found','weblizar'),
			'not_found_in_trash' => __('No Testimonial found in Trash','weblizar'), 
			),
		'supports' => array('title','thumbnail'),
		'show_in' => true,
		'show_in_nav_menus' => false,	
		'public' => true,
		'rewrite' => array('slug' => $GLOBALS['cpt_testimonial']),
		'menu_icon' => 'dashicons-admin-page',
		'exclude_from_search'=> true,
		)
	);
}
add_action( 'init', 'hc_testimonial_type' );

function hc_client_type()
{	register_post_type( 'hc_clients',
		array(
			'labels' => array(
			'name' => __('Healthcare Clients','weblizar'),
			'add_new' => __('Add New Client', 'weblizar'),
			'add_new_item' => __('Add New Client','weblizar'),
			'edit_item' => __('Edit Client','weblizar'),
			'new_item' => __('New Client','weblizar'),
			'all_items' => __('All Clients','weblizar'),
			'view_item' => __('View Client','weblizar'),
			'search_items' => __('Search Client','weblizar'),
			'not_found' =>  __('No Clients found','weblizar'),
			'not_found_in_trash' => __('No Client found in Trash','weblizar'), 
			),
		'supports' => array('title','thumbnail'),
		'show_in' => true,
		'show_in_nav_menus' => false,	
		'public' => true,
		'rewrite' => true,
		'menu_icon' => 'dashicons-admin-page',
		'exclude_from_search'=> true,
		)
	);
}
add_action( 'init', 'hc_client_type' );

function hc_member_type()
{	register_post_type( 'hc_member',
		array(
			'labels' => array(
			'name' => __('Healthcare Members','weblizar'),
			'add_new' => __('Add New Member', 'weblizar'),
			'add_new_item' => __('Add New Member','weblizar'),
			'edit_item' => __('Edit Member','weblizar'),
			'new_item' => __('New Member','weblizar'),
			'all_items' => __('All Members','weblizar'),
			'view_item' => __('View Member','weblizar'),
			'search_items' => __('Search Member','weblizar'),
			'not_found' =>  __('No Members found','weblizar'),
			'not_found_in_trash' => __('No Member found in Trash','weblizar'), 
			),
		'supports' => array('title','thumbnail', 'editor'),
		'show_in' => true,
		'show_in_nav_menus' => false,	
		'public' => true,
		'rewrite' => array('slug' => $GLOBALS['cpt_member']),
		'menu_icon' => 'dashicons-admin-page',
		'exclude_from_search'=> true,
		)
	);
}
add_action( 'init', 'hc_member_type' );

function hc_portfolio_taxonomy() {
    register_taxonomy('hc_portfolio_categories','hc_portfolios',
    array(  'hierarchical' => true,
			'show_in_nav_menus' => false,
            'label' => 'Portfolio Categories',
            'query_var' => true));
	
	// Get Default Category Id 
	$defualt_categories_id = get_option('portfolio_default_categories_id');	
	//quick update category
	if((isset($_POST['action'])) && (isset($_POST['taxonomy']))){		
		wp_update_term($_POST['tax_ID'], 'hc_portfolio_categories', array(
		  'name' => $_POST['name'],
		  'slug' => $_POST['slug']
		));	
		update_option('portfolio_default_categories_id', $defualt_categories_id);
	} 
	else 
	{ 	// Add default taxonomy 
		if(!$defualt_categories_id){
			wp_insert_term('ALL','hc_portfolio_categories', array('description'=> 'Default Category','slug' => 'ALL'));
			$Current_text_id = term_exists('ALL', 'hc_portfolio_categories');
			update_option('portfolio_default_categories_id', $Current_text_id['term_id']);		
		}
		
	}
	 // update  default taxonomy 
	if(isset($_POST['submit']) && isset($_POST['action']) )
	{
		if(isset($_POST['tag_ID']) && isset($_POST['name']) && isset($_POST['slug']) && isset($_POST['description']) ) {
		wp_update_term($_POST['tag_ID'], 'hc_portfolio_categories', array(
			  'name' => $_POST['name'],
			  'slug' => $_POST['slug'],
			  'description' =>$_POST['description']
			));
		}
	}
	// Delete default category
	if(isset($_POST['action']) && isset($_POST['tag_ID']) )
	{	if(($_POST['tag_ID'] == $defualt_tex_id) && $_POST['action'] == "delete-tag")
		{	
			delete_option('portfolio_default_categories_id'); 
		} 
	}	
}
add_action( 'init', 'hc_portfolio_taxonomy' );
add_action('admin_init','hc_init');
function hc_init()
	{	
		add_meta_box('hc_slide', 'Slider Details', 'hc_meta_slider', 'hc_slider', 'normal', 'high');	
		add_meta_box('hc_service', 'Service Details', 'hc_meta_service', 'hc_services', 'normal', 'high');	
		add_meta_box('hc_portfolio', 'Portfolio Details', 'hc_meta_portfolio', 'hc_portfolios', 'normal', 'high');
		add_meta_box('hc_testimonial', 'Testimonial Details', 'hc_meta_testimonial', 'hc_testimonials', 'normal', 'high');	
		add_meta_box('hc_client', 'Client Details', 'hc_meta_client', 'hc_clients', 'normal', 'high');	
		add_meta_box('hc_deptt', 'Department Details', 'hc_meta_deptt', 'hc_deptts', 'normal', 'high');	
		add_meta_box('hc_member', 'Member Details', 'hc_meta_member', 'hc_member', 'normal', 'high');	
		add_action('save_post','hc_meta_save');
	}
	/*Slider Meta Fields*/
	function hc_meta_slider()
	{ 
		global $post;
		$slider_icon = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_icon', true ));
		$slider_desciption = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_desciption', true ));		
		$slider_button_text = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_button_text', true ));
		$slider_button_link = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_button_link', true ));
		$slider_button_target = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_button_target', true ));
	?>
	<p><h4 ><?php _e('Slider FontAwesome Icon','weblizar');?></h4></p>
	<p><input  name="slider_icon" id="slider_icon" style="width: 480px" placeholder="Enter The FontAwesome Icon Class" type="text" value="<?php if (!empty($slider_icon)){ echo esc_attr($slider_icon); }else{ echo 'fa fa-heart';} ?>"/>
	<br><span style="font-size:14px;">Click her for Service <a href="https://fortawesome.github.io/Font-Awesome/icons/" style="text-decoration:none;" target="_blank"> Font-Awesome icons</a> For example : <b>fa fa-check-square-o</b></span>
	</p>
	<p><h4 ><?php _e('Slider Description','weblizar');?></h4></p>
	<p><textarea name="slider_desciption" id="slider_desciption" style="width: 480px; height: 56px; padding: 0px;" placeholder="Enter slider description"  rows="3" cols="10" ><?php if (!empty($slider_desciption)) echo esc_textarea( $slider_desciption ); ?></textarea></p>
	<p><h4 ><?php _e('Slider Button','weblizar');?></h4></p>
	<p><input type="checkbox" id="slider_button_target" name="slider_button_target" <?php if($slider_button_target) echo "checked"; ?> ><?php _e('Open link in a new window/tab','weblizar'); ?></p>
	<p><input  name="slider_button_link" id="slider_button_link" style="width: 480px" placeholder="Enter the button link" type="text" value="<?php if (!empty($slider_button_link)) echo esc_attr($slider_button_link); ?>"/></p>
	<p><input  name="slider_button_text" id="slider_button_text" style="width: 480px" placeholder="Enter the Button text" type="text" value="<?php if (!empty($slider_button_text)) echo esc_attr($slider_button_text); ?>"/></p>
	<?php
	}
	/******** weblizar meta service ***********/
	function hc_meta_service()
	{ 
		global $post;
		$service_font_awesome_icons = sanitize_text_field( get_post_meta( get_the_ID(), 'service_font_awesome_icons', true ));		
		$service_button_text = sanitize_text_field( get_post_meta( get_the_ID(), 'service_button_text', true ));			
		$service_button_target = sanitize_text_field( get_post_meta( get_the_ID(), 'service_button_target', true ));
		$service_button_link = sanitize_text_field( get_post_meta( get_the_ID(), 'service_button_link', true ));	
	?>
	<p><h4 ><?php _e('Service font-awesome Icons','weblizar');?></h4></p>
	<p><input  name="service_font_awesome_icons" id="service_font_awesome_icons" style="width: 480px" placeholder="Enter Service font-awesome icons" type="text" value="<?php if (!empty($service_font_awesome_icons)) echo esc_attr($service_font_awesome_icons); ?>"/>
	<br><span style="font-size:14px;">Click her for Service <a href="https://fortawesome.github.io/Font-Awesome/icons/" style="text-decoration:none;" target="_blank"> Font-Awesome icons</a> For example : <b>fa fa-check-square-o</b></span>
	</p>
	<p><input type="checkbox" id="service_button_target" name="service_button_target" <?php if($service_button_target) echo "checked"; ?> ><?php _e('Open link in a new window/tab','weblizar'); ?></p>
	<p><input  name="service_button_link" id="service_button_link" style="width: 480px" placeholder="Enter the service link" type="text" value="<?php if (!empty($service_button_link)) echo esc_attr($service_button_link); ?>"/></p>
	<?php
	}
	
	/******** weblizar meta testimonial ***********/
	function hc_meta_testimonial()
	{ 
		global $post;
		$testimonial_description = sanitize_text_field( get_post_meta( get_the_ID(), 'testimonial_description', true ));	
		$testimonial_designation = sanitize_text_field( get_post_meta( get_the_ID(), 'testimonial_designation', true ));	
	?>
	<p><h4 ><?php _e('Testimonial Description','weblizar');?></h4></p>
	<p><textarea  name="testimonial_description" id="testimonial_description" style="width: 480px" placeholder="Enter Testimonial Description"><?php if (!empty($testimonial_description)) echo esc_attr($testimonial_description); ?></textarea></p>
	<p><h4 ><?php _e('Testimonial Designation','weblizar');?></h4></p>
	<p><input  name="testimonial_designation" id="testimonial_designation" style="width: 480px" placeholder="Enter Testimonial Designation" type="text" value="<?php if (!empty($testimonial_designation)) echo esc_attr($testimonial_designation); ?>" /></p>	
	<?php
	}
	
	/*Portfolio Meta Fields*/
	function hc_meta_portfolio()
	{ 
		global $post;
		$portfolio_desc_title = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_desc_title', true ));	
		$portfolio_desc = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_desc', true ));	
		$portfolio_url = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_url', true ));
		$portfolio_link_target = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_link_target', true ));
		$portfolio_url_text = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_url_text', true ));
		$portfolio_skill = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_skill', true ));
		$portfolio_client = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_client', true ));
		$portfolio_icon = sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_icon', true ));
	?>
	<p><h4><?php _e('Portfolio Description Title','weblizar');?></h4></p>
	<p><input type="text" name="portfolio_desc_title" id="portfolio_desc_title" style="width: 480px" value="<?php if (!empty($portfolio_desc_title)){ echo esc_attr($portfolio_desc_title); }else{ echo 'Portfolio Description'; } ?>" /></p>
	<p><h4><?php _e('Portfolio Description','weblizar');?></h4></p>
	<p><textarea name="portfolio_desc" id="portfolio_desc" style="width: 480px" ><?php if (!empty($portfolio_desc)) echo esc_attr($portfolio_desc); ?></textarea>
	</p>
	<p><h4 ><?php _e('Portfolio Icon','weblizar');?></h4></p>
	<p><input name="portfolio_icon" id="portfolio_icon" style="width: 480px" placeholder="Enter the button link Ex:http://weblizar.com/" type="text" value="<?php if (!empty($portfolio_icon)){ echo esc_attr($portfolio_icon); }else{ echo 'fa fa-heart'; } ?>"/>
	<br><span style="font-size:14px;">Click her for Service <a href="https://fortawesome.github.io/Font-Awesome/icons/" style="text-decoration:none;" target="_blank"> Font-Awesome icons</a> For example : <b>fa fa-check-square-o</b></span>
	</p>
	<p><h4 ><?php _e('Portfolio URL','weblizar');?></h4></p>
	<p><input type="checkbox" id="portfolio_link_target" name="portfolio_link_target" <?php if($portfolio_link_target) echo "checked"; ?> ><?php _e('Open link in a new window/tab','weblizar'); ?></p>
	<p><input  name="portfolio_url" id="portfolio_url" style="width: 480px" placeholder="Enter the button link Ex:http://weblizar.com/" type="text" value="<?php if (!empty($portfolio_url)) echo esc_attr($portfolio_url); ?>"/></p>
	<p><input  name="portfolio_url_text" id="portfolio_url_text" style="width: 480px" placeholder="Enter the button Text" type="text" value="<?php if (!empty($portfolio_url_text)) echo esc_attr($portfolio_url_text); ?>"/></p>
	<p><h4 ><?php _e('Portfolio Details','weblizar');?></h4></p>
	<p><input  name="portfolio_skill" id="portfolio_skill" style="width: 480px" placeholder="Enter the Portfolio Skill" type="text" value="<?php if (!empty($portfolio_skill)) echo esc_attr($portfolio_skill); ?>"/></p>
	<p><input  name="portfolio_client" id="portfolio_client" style="width: 480px" placeholder="Enter the Portfolio Client" type="text" value="<?php if (!empty($portfolio_client)) echo esc_attr($portfolio_client); ?>"/></p>
	<?php
	}
	
	/******** weblizar meta client ***********/
	function hc_meta_client()
	{ 
		global $post;
		$client_link = sanitize_text_field( get_post_meta( get_the_ID(), 'client_link', true ));		
		$client_link_target = sanitize_text_field( get_post_meta( get_the_ID(), 'client_link_target', true ));		
	?>
	<p><h4 ><?php _e('Client Details','weblizar');?></h4></p>
	<p><input type="checkbox" id="client_link_target" name="client_link_target" <?php if($client_link_target) echo "checked"; ?> ><?php _e('Open link in a new window/tab','weblizar'); ?></p>
	<p><input  name="client_link" id="client_link" style="width: 480px" placeholder="Enter Client Link" type="text" value="<?php if (!empty($client_link)) echo esc_attr($client_link); ?>" /></p>	
	<?php
	}
	
	/******** weblizar meta department ***********/
	function hc_meta_deptt()
	{ 
		global $post;
		$deptt_icon = sanitize_text_field( get_post_meta( get_the_ID(), 'deptt_icon', true ));	
	?>
	<p><h4 ><?php _e('Department Icon','weblizar');?></h4></p>
	<p><input  name="deptt_icon" id="deptt_icon" style="width: 480px" placeholder="Enter Department Icon" type="text" value="<?php if (!empty($deptt_icon)) echo esc_attr($deptt_icon); ?>" />
	<br><span style="font-size:14px;">Click her for Service <a href="https://fortawesome.github.io/Font-Awesome/icons/" style="text-decoration:none;" target="_blank"> Font-Awesome icons</a> For example : <b>fa fa-check-square-o</b></span>
	</p>
		
	<?php
	}
	
	/******** weblizar meta members ***********/
	function hc_meta_member()
	{ 
		global $post;
		$member_cat = sanitize_text_field( get_post_meta( get_the_ID(), 'member_cat', true ));	
		$member_exp = sanitize_text_field( get_post_meta( get_the_ID(), 'member_exp', true ));	
		$member_skill = sanitize_text_field( get_post_meta( get_the_ID(), 'member_skill', true ));	
		$member_post = sanitize_text_field( get_post_meta( get_the_ID(), 'member_post', true ));	
		$member_twitter = sanitize_text_field( get_post_meta( get_the_ID(), 'member_twitter', true ));	
		$member_fb = sanitize_text_field( get_post_meta( get_the_ID(), 'member_fb', true ));	
		$member_google = sanitize_text_field( get_post_meta( get_the_ID(), 'member_google', true ));	
		$member_mail = sanitize_text_field( get_post_meta( get_the_ID(), 'member_mail', true ));
		$member_contact = sanitize_text_field( get_post_meta( get_the_ID(), 'member_contact', true ));
		$member_link = sanitize_text_field( get_post_meta( get_the_ID(), 'member_link', true ));
		$member_btn_text = sanitize_text_field( get_post_meta( get_the_ID(), 'member_btn_text', true ));
		$cats=explode(" ,",$member_cat);	?>
		<p><h4 ><?php _e('Designation','weblizar');?></h4></p>
		<p><input  name="member_post" id="member_post" style="width: 480px" placeholder="Enter Member's Designation" type="text" value="<?php if (!empty($member_post)) echo esc_attr($member_post); ?>" /></p>
		<p><h4 ><?php _e('Member Experiance','weblizar');?></h4></p>
		<p><input  name="member_exp" id="member_exp" style="width: 480px" placeholder="Enter Member's Experiance" type="text" value="<?php if (!empty($member_exp)) echo esc_attr($member_exp); ?>" /></p>
		<p><h4 ><?php _e('Skills','weblizar');?></h4></p>
		<p><input  name="member_skill" id="member_skill" style="width: 480px" placeholder="Enter Member's Skills" type="text" value="<?php if (!empty($member_skill)) echo esc_attr($member_skill); ?>" /></p>
		<p><h4 ><?php _e('Member Twitter Link','weblizar');?></h4></p>
		<p><input  name="member_twitter" id="member_twitter" style="width: 480px" placeholder="Enter Member's Twitter Link" type="text" value="<?php if (!empty($member_twitter)) echo esc_attr($member_twitter); ?>" /></p>
		<p><h4 ><?php _e('Member Facebook Link','weblizar');?></h4></p>
		<p><input  name="member_fb" id="member_fb" style="width: 480px" placeholder="Enter Member's Facebook Link" type="text" value="<?php if (!empty($member_fb)) echo esc_attr($member_fb); ?>" /></p>
		<p><h4 ><?php _e('Member Google Link','weblizar');?></h4></p>
		<p><input  name="member_google" id="member_google" style="width: 480px" placeholder="Enter Member's Google Link" type="text" value="<?php if (!empty($member_google)) echo esc_attr($member_google); ?>" /></p>
		<p><h4 ><?php _e('Member Email','weblizar');?></h4></p>
		<p><input  name="member_mail" id="member_mail" style="width: 480px" placeholder="Enter Member's Email Address" type="text" value="<?php if (!empty($member_mail)) echo esc_attr($member_mail); ?>" /></p>
		<p><h4 ><?php _e('Member Contact Number','weblizar');?></h4></p>
		<p><input  name="member_contact" id="member_contact" style="width: 480px" placeholder="Enter Member's Contact Number" type="text" value="<?php if (!empty($member_contact)) echo esc_attr($member_contact); ?>" /></p>
		<p><h4 ><?php _e('External Link','weblizar');?></h4></p>
		<p><input  name="member_link" id="member_link" style="width: 480px" placeholder="Enter Member's External Link" type="text" value="<?php if (!empty($member_link)) echo esc_attr($member_link); ?>" /></p>
		<p><h4 ><?php _e('External Link Button Text','weblizar');?></h4></p>
		<p><input  name="member_btn_text" id="member_btn_text" style="width: 480px" placeholder="Enter Member's External Link Text" type="text" value="<?php if (!empty($member_btn_text)) echo esc_attr($member_btn_text); ?>" /></p>
		<p><h4 ><?php _e('Member Category','weblizar');?></h4></p>
		<?php $args = array( 'post_type' => 'hc_deptts','posts_per_page' =>-1);
		$deptts= new WP_Query($args); 
		if( $deptts->have_posts() ){ ?>
			<select name="ary[]" multiple="multiple">
				<?php while ( $deptts->have_posts() ) : $deptts->the_post(); ?>
					<option value="<?php echo get_the_title(); ?>" <?php foreach($cats as $cat){ if($cat== get_the_title()){ echo 'selected="selected"'; } } ?>><?php echo get_the_title(); ?></option>
				<?php endwhile; ?>
			</select>
		<?php } ?>	
	<?php
	}
	
	function hc_meta_save($post_id) 
	{	
		if((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']))
			return;
			
		if ( ! current_user_can( 'edit_page', $post_id ) )
		{     return ;	} 		
		if(isset($_POST['post_ID']))
		{ 	
			$post_ID = $_POST['post_ID'];				
			$post_type=get_post_type($post_ID);
			
			if($post_type=='hc_slider') {			
				update_post_meta($post_ID, 'slider_icon', sanitize_text_field($_POST['slider_icon']));
				update_post_meta($post_ID, 'slider_desciption', sanitize_text_field($_POST['slider_desciption']));
				update_post_meta($post_ID, 'slider_button_text', sanitize_text_field($_POST['slider_button_text']));
				update_post_meta($post_ID, 'slider_button_link', sanitize_text_field($_POST['slider_button_link']));
				update_post_meta($post_ID, 'slider_button_target', sanitize_text_field($_POST['slider_button_target']));
			}
			else if($post_type=='hc_services') {
				update_post_meta($post_ID, 'service_font_awesome_icons', sanitize_text_field($_POST['service_font_awesome_icons']));
				update_post_meta($post_ID, 'service_description', sanitize_text_field($_POST['service_description']));
				update_post_meta($post_ID, 'service_button_link', sanitize_text_field($_POST['service_button_link']));
				update_post_meta($post_ID, 'service_button_target', sanitize_text_field($_POST['service_button_target']));			
			}
			else if($post_type=='hc_portfolios') {
				update_post_meta($post_ID, 'portfolio_desc_title', sanitize_text_field($_POST['portfolio_desc_title']));			
				update_post_meta($post_ID, 'portfolio_desc', sanitize_text_field($_POST['portfolio_desc']));			
				update_post_meta($post_ID, 'portfolio_url', sanitize_text_field($_POST['portfolio_url']));			
				update_post_meta($post_ID, 'portfolio_link_target', sanitize_text_field($_POST['portfolio_link_target']));			
				update_post_meta($post_ID, 'portfolio_url_text', sanitize_text_field($_POST['portfolio_url_text']));			
				update_post_meta($post_ID, 'portfolio_skill', sanitize_text_field($_POST['portfolio_skill']));			
				update_post_meta($post_ID, 'portfolio_client', sanitize_text_field($_POST['portfolio_client']));
				update_post_meta($post_ID, 'portfolio_icon', sanitize_text_field($_POST['portfolio_icon']));
			}
			else if($post_type=='hc_testimonials') {
				update_post_meta($post_ID, 'testimonial_description', sanitize_text_field($_POST['testimonial_description']));		
				update_post_meta($post_ID, 'testimonial_designation', sanitize_text_field($_POST['testimonial_designation']));		
			}
			else if($post_type=='hc_clients') {
				update_post_meta($post_ID, 'client_link_target', sanitize_text_field($_POST['client_link_target']));		
				update_post_meta($post_ID, 'client_link', sanitize_text_field($_POST['client_link']));		
			}
			else if($post_type=='hc_deptts') {
				update_post_meta($post_ID, 'deptt_icon', sanitize_text_field($_POST['deptt_icon']));		
			}else if($post_type=='hc_member') {	
				$values = $_POST['ary'];	
					foreach($values as $val){
						$data.= $val.' ,';
					}
				update_post_meta($post_ID, 'member_cat', $data);		
				update_post_meta($post_ID, 'member_exp', $_POST['member_exp']);	
				update_post_meta($post_ID, 'member_post', $_POST['member_post']);		
				update_post_meta($post_ID, 'member_twitter', $_POST['member_twitter']);		
				update_post_meta($post_ID, 'member_fb', $_POST['member_fb']);		
				update_post_meta($post_ID, 'member_google', $_POST['member_google']);
				update_post_meta($post_ID, 'member_skill', $_POST['member_skill']);
				update_post_meta($post_ID, 'member_mail', $_POST['member_mail']);
				update_post_meta($post_ID, 'member_contact', $_POST['member_contact']);
				update_post_meta($post_ID, 'member_link', $_POST['member_link']);
				update_post_meta($post_ID, 'member_btn_text', $_POST['member_btn_text']);
			}
		}			
	} 
	
?>