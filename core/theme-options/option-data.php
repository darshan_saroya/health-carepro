 <?php $wl_theme_options = health_care_get_options(); 
	//Maintenance Mode Ready
	if(isset($_POST['weblizar_settings_save_home-maintenance']))
	{	
		if($_POST['weblizar_settings_save_home-maintenance'] == 1) 
		{
				foreach($_POST as  $key => $value)
				{
					$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
				}				
				
				if($_POST['maintenance_switch']){
				echo $wl_theme_options['maintenance_switch']=sanitize_text_field($_POST['maintenance_switch']); } 
				else
				{ echo $wl_theme_options['maintenance_switch']="off"; } 
				
				if($_POST['maintenance_social']){
				echo $wl_theme_options['maintenance_social']=sanitize_text_field($_POST['maintenance_social']); } 
				else
				{ echo $wl_theme_options['maintenance_social']="off"; }
					
				update_option('health_pro_options', stripslashes_deep($wl_theme_options));
		}	
		if($_POST['weblizar_settings_save_home-maintenance'] == 2) 
		{
			hc_maintenance_setting();
		}
	}
	
	
	/*
	* LOGO Fevicon settings save
	*/
	if(isset($_POST['weblizar_settings_save_general']))
	{	
		if($_POST['weblizar_settings_save_general'] == 1) 
		{
				foreach($_POST as  $key => $value)
				{
					$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
				}				
				
				
				if($_POST['front_page']){
				echo $wl_theme_options['front_page']=sanitize_text_field($_POST['front_page']); } 
				else
				{ echo $wl_theme_options['front_page']="off"; }	
				
				if($_POST['fix_header']){
				echo $wl_theme_options['fix_header']=sanitize_text_field($_POST['fix_header']); } 
				else
				{ echo $wl_theme_options['fix_header']="off"; }	
			
				if($_POST['custom_cover']){
				echo $wl_theme_options['custom_cover']=sanitize_text_field($_POST['custom_cover']); } 
				else
				{ echo $wl_theme_options['custom_cover']="off"; }	
			
				update_option('health_pro_options', stripslashes_deep($wl_theme_options));
		}	
		if($_POST['weblizar_settings_save_general'] == 2) 
		{
			hc_general_setting();
		}
	}
	/*
	* slider settings
	*/
	if(isset($_POST['weblizar_settings_save_home-slider']))
	{	
		if($_POST['weblizar_settings_save_home-slider'] == 1) 
		{
				foreach($_POST as  $key => $value)
				{
					$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
				}
				if($_POST['slide_cycle']){
				echo $wl_theme_options['slide_cycle']=sanitize_text_field($_POST['slide_cycle']); } 
				else
				{ echo $wl_theme_options['slide_cycle']="off"; }	
					
				if($_POST['slide_autoplay']){
				echo $wl_theme_options['slide_autoplay']=sanitize_text_field($_POST['slide_autoplay']); } 
				else
				{ echo $wl_theme_options['slide_autoplay']="off"; }	
				if($_POST['slide_autoplay']){
				echo $wl_theme_options['slide_pause']=sanitize_text_field($_POST['slide_pause']); } 
				else
				{ echo $wl_theme_options['slide_pause']="off"; }
				update_option('health_pro_options', stripslashes_deep($wl_theme_options));
		}	
		if($_POST['weblizar_settings_save_home-slider'] == 2) 
		{
			hc_slider_setting();
		}
	}
	/*
	* Home Service setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-service']))
	{	
		if($_POST['weblizar_settings_save_home-service'] == 1) 
		{	
			foreach($_POST as $key => $value)
			{
				$wl_theme_options[$key] = sanitize_text_field($_POST[$key]);
			}
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-service'] == 2) 
		{	
			hc_service_setting();
		}
	}
	
	/*
	* Home portfolio setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-portfolio']))
	{	
		if($_POST['weblizar_settings_save_home-portfolio'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-portfolio'] == 2) 
		{	
			hc_portfolio_setting();
		}
	}
	
	/*
	* Home blog setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-blog']))
	{	
		if($_POST['weblizar_settings_save_home-blog'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-blog'] == 2) 
		{	
			hc_blog_setting();
		}
	}
	
	/*
	* Home Testimonial setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-testi']))
	{	
		if($_POST['weblizar_settings_save_home-testi'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-testi'] == 2) 
		{	
			hc_testi_setting();
		}
	}
	
	/*
	* Home Client setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-client']))
	{	
		if($_POST['weblizar_settings_save_home-client'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-client'] == 2) 
		{	
			hc_client_setting();
		}
	}
	
	
	/*
	* Home Theme call out area setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-call-out']))
	{	
		if($_POST['weblizar_settings_save_home-call-out'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			if($_POST['call_out_button_target']){
			
			$wl_theme_options['call_out_button_target']=sanitize_text_field($_POST['call_out_button_target']); } 
			else
			{  $wl_theme_options['call_out_button_target']="off"; }
			
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-call-out'] == 2) 
		{	
			hc_callout_setting();
		}
	}
	
	/*
	* Home Theme department area setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-deppt']))
	{	
		if($_POST['weblizar_settings_save_home-deppt'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}			
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-deppt'] == 2) 
		{	
			hc_deppt_setting();
		}
	}
	/*
	* Home Theme members area setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-members']))
	{	
		if($_POST['weblizar_settings_save_home-members'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}			
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-members'] == 2) 
		{	
			hc_member_setting();
		}
	}
	/*
	* Home Theme facts area setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-facts']))
	{	
		if($_POST['weblizar_settings_save_home-facts'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}			
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-facts'] == 2) 
		{	
			hc_facts_setting();
		}
	}
	
	/*
	* Home Theme feedback area setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-feedback']))
	{	
		if($_POST['weblizar_settings_save_home-feedback'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}			
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-feedback'] == 2) 
		{	
			hc_feedback_setting();
		}
	}
	/*
	*  Theme  skin setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-skin']))
	{	
		if($_POST['weblizar_settings_save_home-skin'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}			
			if($_POST['custom_bg']){
				echo $wl_theme_options['custom_bg']=sanitize_text_field($_POST['custom_bg']); } 
				else
				{ echo $wl_theme_options['custom_bg']="off"; }
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-skin'] == 2) 
		{	
			hc_skin_layout_setting();
		}
	}
	
	/*
	* Home Theme contact details setting 
	*/
	if(isset($_POST['weblizar_settings_save_home-contact']))
	{	
		if($_POST['weblizar_settings_save_home-contact'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			//google map on conatact page
			if(isset($_POST['google_map_on_contact']))
			{  $wl_theme_options['google_map_on_contact'] = $_POST['google_map_on_contact'];
			} else {  $wl_theme_options['google_map_on_contact'] = "off";	} 
			
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-contact'] == 2) 
		{	
			hc_contact_setting();
			
		}
	}
	
	
	/*
	* social media link Settings
	*/
	if(isset($_POST['weblizar_settings_save_home-social']))
	{	
		if($_POST['weblizar_settings_save_home-social'] == 1) 
		{
			
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			
			
			// Social Icons footer section yes or on
			if(isset($_POST['header_social_icon']))
			{  $wl_theme_options['header_social_icon'] = $_POST['header_social_icon'];
			} else {  $wl_theme_options['header_social_icon'] = "off";	} 
			
			// Social Icons social_media_on_contact section yes or on
			if(isset($_POST['footer_social_icon']))
			{  $wl_theme_options['footer_social_icon'] = $_POST['footer_social_icon'];
			} else {  $wl_theme_options['footer_social_icon'] = "off";	} 
			
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-social'] == 2) 
		{
			hc_social_setting();
		}
	}
	
	/*
	* footer customization Settings
	*/
	if(isset($_POST['weblizar_settings_save_home-footer']))
	{	
		if($_POST['weblizar_settings_save_home-footer'] == 1) 
		{	
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
			
		}	
		if($_POST['weblizar_settings_save_home-footer'] == 2) 
		{	
			hc_footer_setting();
			
		}
	}
	
	/*
	* Custom Links Settings
	*/
	if(isset($_POST['weblizar_settings_save_home-custom-url']))
	{	
		if($_POST['weblizar_settings_save_home-custom-url'] == 1) 
		{
			foreach($_POST as  $key => $value)
			{
				$wl_theme_options[$key]=sanitize_text_field($_POST[$key]);	
			}
			
			update_option('health_pro_options', stripslashes_deep($wl_theme_options));
		}	
		if($_POST['weblizar_settings_save_home-custom-url'] == 2) 
		{
			hc_cptlinks();
		}
	}
?>