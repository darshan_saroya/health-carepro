<?php
/**
 * Weblizar Admin Menu
 */
add_action('admin_menu', 'hc_admin_menu_pannel');  
function hc_admin_menu_pannel() {
	$page = add_theme_page( 'theme', 'Theme Options', 'edit_theme_options', 'weblizar', 'hc_option_panal_function' ); 
 	add_action('admin_print_styles-'.$page, 'hc_admin_enqueue_script');
 } 
/**
 * Weblizar Admin Menu CSS
 */
function hc_admin_enqueue_script() {		
	wp_enqueue_script('weblizar-tab-js', get_template_directory_uri().'/core/theme-options/js/option-js.js',array('media-upload', 'jquery-ui-sortable'));	
	wp_enqueue_script('weblizar-bt-toggle', get_template_directory_uri().'/core/theme-options/js/bt-toggle.js');	
	wp_enqueue_style('thickbox');	
	wp_enqueue_style('weblizar-option-style', get_template_directory_uri().'/core/theme-options/css/option-style.css');
	wp_enqueue_style('weblizar-boot', get_template_directory_uri(). '/css/bootstrap/css/bootstrap.min.css');
	
	wp_enqueue_style('weblizar-fa', get_template_directory_uri(). '/css/font-awesome.min.css');
	wp_enqueue_style('weblizar-calendrical', get_template_directory_uri(). '/css/calendrical.css');
	wp_enqueue_style('weblizar-bootstrap-responsive', get_template_directory_uri().'/core/theme-options/css/bootstrap-responsive.css');
	wp_enqueue_style('optionpanal-dragdrop',get_template_directory_uri().'/core/theme-options/css/weblizar-dragdrop.css');
	/****custom color ****/
	wp_enqueue_style( 'farbtastic' );
	wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'weblizar-custom-color', get_template_directory_uri().'/core/theme-options/js/weblizar-custom-color-picker.js', array( 'wp-color-picker','farbtastic' ), false, true );
	wp_enqueue_script('weblizar-bootjs', get_template_directory_uri(). '/css/bootstrap/js/bootstrap.min.js');
	wp_enqueue_script('calendricaljs', get_template_directory_uri(). '/js/jquery.calendrical.js');
}
/**
 * Weblizar Theme Option Form
 */
function hc_option_panal_function() { ?>
  <header>
		<div class="container-fluid top">
			<div class="col-md-6">
				<h2><img src="<?php echo get_stylesheet_directory_uri(); ?>/core/theme-options/images/logo.png" alt="Weblizar" height="50" style="margin-top:-10px; margin-right:10px;" /> <?php echo wp_get_theme(); ?></h2>
			</div>	  
			<div class="col-md-6 search1">
				<a href="https://weblizar.com/forum/viewforum.php?f=51" target="_blank"><span class="fa fa-comment-o"></span> Support Forum</a>
				<a href="https://weblizar.com/documentation/themes/healthcare-premium-documentation/" target="_blank"><span class="fa fa-pencil-square-o"></span> View Documentation</a>
			</div>	  
		</div>  
  </header>
	<div class="container-fluid support">
		<div class="row left-sidebar">
			<div class="col-md-12 menu">
			  <!-- tabs left -->
			<div class="col-xs-12 tabbable tabs-left">
			  
				<ul class="col-xs-2 nav nav-tabs collapsible collapsible-accordion">
				<li class=" user-details">
					<div class="profile-pic">
					<?php global $current_user;
      				get_currentuserinfo();
          $args= array('class' => 'img-responsive img-circle');
					echo get_avatar( $current_user->user_email, 80, 'monsterid', '', $args ); ?>
					</div>
					<div class="caption">
						<h3 class="site-title"><?php echo get_bloginfo('name'); ?></h3>
						<p><?php echo get_bloginfo('description'); ?></p>
					</div>
				</li>
				
				
				  <li class="active"><a class="" href="#general" data-toggle="tab"><i class="fa fa-home icon"></i> <?php _e('Home Option','weblizar'); ?></a></li>
				  <li><a href="#deppt" data-toggle="tab"><i class="fa fa-desktop icon"></i><?php _e('Department Option','weblizar'); ?></a></li>
				  <li><a href="#members" data-toggle="tab"><i class="fa fa-user icon"></i> <?php _e('Members Options','weblizar'); ?></a></li>
				  <li><a href="#facts" data-toggle="tab"><i class="fa fa-connectdevelop icon"></i><?php _e('Fun Facts Options','weblizar'); ?></a></li>
				  <li><a href="#feedback" data-toggle="tab"><i class="fa fa-commenting-o icon"></i> <?php _e('Feedback Options','weblizar'); ?></a></li>
				  <li><a href="#skin" data-toggle="tab"><i class="fa fa-file-image-o icon"></i> <?php _e('Skin Layout Options','weblizar'); ?></a></li>
				  <li><a href="#contact" data-toggle="tab"><i class="fa fa-phone icon"></i><?php _e('Contact Options','weblizar'); ?></a></li>
				  <li><a href="#footer" data-toggle="tab"><i class="fa fa-file-text-o icon"></i> <?php _e('Footer Options','weblizar'); ?></a></li>
				  <li><a href="#social" data-toggle="tab"><i class="fa fa-twitter icon"></i> <?php _e('Social Media Options','weblizar'); ?></a></li>
				  <li><a href="#layout" data-toggle="tab"><i class="fa fa-file-text-o icon"></i> <?php _e('Home Page Manager','weblizar'); ?></a></li>
				  <li><a href="#maintenance" data-toggle="tab"><i class="fa fa-shopping-cart icon"></i> <?php _e('Maintainance Mode','weblizar'); ?></a></li>
				  <li><a href="#curl" data-toggle="tab"><i class="fa fa-chain icon"></i> <?php _e('Custom URLs Options','weblizar'); ?></a></li>
				</ul>
				<!--------Option Data saving ------->
					<?php require_once('option-data.php'); ?>	
					<!--------Option Settings form ------->
					<?php require_once('option-settings.php'); ?>	
				</div>
			  <!-- /tabs -->
			  
			</div>
		</div>
	</div>	
<?php
	
}
	// Restore all defaults
	if(isset($_POST['restore_all_defaults'])) {
		$weblizar_theme_options = health_care_default_settings();	
		update_option('health_pro_options', $weblizar_theme_options);
	}
	
	//maintenance enable function 'template redirect' 
        function maintenance_template_redirect()
        {
			$wl_theme_options = get_option('health_pro_options');
			if($wl_theme_options['maintenance_switch']=='on')
			{			
				if(!is_feed())
				{
					//if user not login page is redirect on coming soon template page
					if ( !is_user_logged_in()  )
					{
						
						$file = get_template_directory() . '/core/theme-options/maintenance.php';
						include($file);
						exit();
					}
				}
				//if user is log-in then we check role.
				if (is_user_logged_in() )
				{
					//get logined in user role
					global $current_user;
					get_currentuserinfo();
					$LoggedInUserID = $current_user->ID;
					$UserData = get_userdata( $LoggedInUserID );
					//if user role not 'administrator' redirect him to message page
					if($UserData->roles[0] != "administrator")
					{
						if(!is_feed())
						{
							$file =get_template_directory(). '/core/theme-options/maintenance.php';
							exit();
						}
					}
				}
			}
        }

        //add action to call function maintenance_template_redirect 
        add_action( 'template_redirect', 'maintenance_template_redirect' );
?>