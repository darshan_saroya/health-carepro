<!DOCTYPE html>
<html lang="en">
<?php 
	$wl_theme_options = get_option('health_pro_options');
?>
	<head>
	<meta charset="utf-8">
	<title>
		<?php bloginfo( 'name' ); $site_description = get_bloginfo( 'description' ); ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap/css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/font-awesome.min.css'; ?>" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri().'/js/countdown.js'; ?>"></script>
	<style>
	header {
  bottom: 0;
  position: absolute;
  top: 0;
  width: 100%;
}
.video {
  background-color: rgba(0, 0, 0, 0.5);
  margin: 0;
  min-height: 789px;
  overflow: hidden;
  position: relative;
  width: 100%;
  background-attachment:fixed;
  background-size:100%;
}
.video iframe {
  height: 900px;
  position: absolute;
  width: 100%;
}
.maintance-detail{
padding:150px 20px;
text-align:center;
color: #fff;
}
.m-social {
  list-style: none;
  display: inline-block;
}
.m-social li{
  display: inline-block;
}
.m-social li a{
text-align:center;
}
.m-social li .icon {
  border: 1px solid #ccc;
  border-radius: 50%;
  color: #fff;
  font-size: 30px;
  height: 50px;
  margin: 10px;
  padding: 10px;
  width: 50px;
}
.maintance-cover h1 {
  font-size: 72px !important;
}
@media (max-width:480px){
	.maintance-cover h1 {
    font-size: 22px !important;
}
.countDown .days, .countDown .hours, .countDown .minutes, .countDown .seconds{
	font-size:24px !important;
	margin:0 5px 0 0 !important;
	width:auto !important;
}
}
.maintance-image {
  background-image: url("<?php echo esc_url($wl_theme_options['maintenance_image']); ?>");
  margin: 0;
  background-size: 100% 100%;
  background-attachment: fixed;
  height:100%;
}
.maintance-cover {
    background-color: rgba(0, 0, 0, 0.7);
	height:100%;
}
.maintance-detail h4::before {
  content: "";
  height: 3px;
  width: 80px;
  background-color: #fff;
  position: absolute;
  margin-left: -90px;
  margin-top: 17px;
}
.maintance-detail h4::after {
  background-color: #fff;
  content: "";
  height: 3px;
  margin-left: 10px;
  margin-top: 17px;
  position: absolute;
  width: 80px;
}
.countDown {
margin:60px auto;
width:100%;
text-align:center;
}
.countDown .text {
  font-size:18px;
  display:block;
  background-color:transparent;
}
.countDown .rotate {
  display: inline-block;
}
.countDown .days,
 .countDown .hours,
 .countDown .minutes,
 .countDown .seconds {
  display: inline-block;
  font-size: 84px;
  margin: 0 auto !important;
  text-align: center;
   width: 180px;
}
.maintance-detail h4 {
  font-size: 30px;
  font-weight: bold;
}
@media (min-width:481px) and (max-width:768px){
	.video {
    min-height: 1024px;
}
	.video iframe {
    height: 1024px;
}
}
@media (max-width:480px){
	.maintance-cover h1 {
    font-size: 22px !important;
}
.countDown .days, .countDown .hours, .countDown .minutes, .countDown .seconds{
	font-size:24px !important;
	margin:0 5px 0 0 !important;
	width:auto !important;
}
.m-social li .icon {
    font-size: 16px;
    height: 30px;
    margin: 0px;
    padding: 5px;
    width: 30px;
}
.maintance-detail {
	padding:0;
    padding-top: 90px;
}
.m-social {
    width: 100%;
    margin: 0 !important;
    padding: 0 !important;
}
.video {
    min-height: 480px;
}
	.video iframe {
    height: 480px;
}
}
	</style>
	</head>
	<body>
<div class="wrapper">
<?php if($wl_theme_options['maintenance_video']!=''){ ?>
	<div class="row video"> 
		<iframe width="560" height="315" src="<?php echo $wl_theme_options['maintenance_video']; ?>?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>
		<header>
			<?php page_data(); ?>
		</header>
	</div>
<?php }else{ ?>
	<header>
		<div class="row maintance-image">
			<?php page_data(); ?>
		</div>
	</header>
<?php } ?>
</div>
<!-- Maintains Page End -->
<?php if($wl_theme_options['maintenance_date']!=''){ ?>
<script>         
  countdown('<?php echo $wl_theme_options['maintenance_date']; ?>  12:00:00 AM',callback); // Date format ('MM/DD/YYYY  HH:MM:SS TT');
            
  function callback(){
       jQuery('.countDown').css('display','none'); //Callback Function Definition
  };
</script>
<?php } ?>
<?php function page_data(){ 
$wl_theme_options = get_option('health_pro_options'); ?>
<div class="container-fluid maintance-cover">
				<div class="container">
					<div class="row maintance-detail">
						<h4><?php bloginfo( 'name' ); ?></h4>
						<h1><?php echo $wl_theme_options['maintenance_text']; ?></h1>
						<?php if($wl_theme_options['maintenance_date']!=''){ ?>
						<div class="row countDown" data-sr="enter bottom">
							<div class="rotate"><div class="days wow fadeInUp" id="days" data-sr="enter bottom over 1s and move 110px wait 0.3s">00 <span class='text'>Days</span> </div></div>    
							<div class="rotate"><div class="hours wow fadeInUp" id="hours" data-sr="enter bottom over 1s and move 110px wait 0.3s">00 <span class='text'>Hours</span></div></div>  
							<div class="rotate"><div class="minutes wow fadeInUp" id="minutes" data-sr="enter bottom over 1s and move 110px wait 0.3s">00 <span class='text'>Minutes</span></div></div> 
							<div class="rotate"><div class="seconds wow fadeInUp" id="seconds" data-sr="enter bottom over 1s and move 110px wait 0.3s">00 <span class='text'>Seconds</span></div></div>  
						</div>
						<?php } ?>
						<?php if($wl_theme_options['maintenance_social']=='on'){ ?>
						<ul class="m-social">
							<?php for($i=1; $i<=6; $i++){
									if($wl_theme_options['social_icon_'.$i]!=''){
								?>
								<li><a href="<?php echo $wl_theme_options['social_link_'.$i]; ?>" target="_blank"><i class="<?php echo $wl_theme_options['social_icon_'.$i]; ?> icon"></i></a></li>
									<?php }
								} ?>
						</ul>
					<?php } ?>
					</div>
				</div>
			</div>
<?php }  ?>
</body>
</html>