<?php $wl_theme_options = health_care_get_options(); ?>
<div class="col-xs-10 tab-content" id="spa_general">
					<div class="tab-pane col-md-12 active" id="general">
						<div class="col-md-12 option">
							<h1><?php _e('General Settings','weblizar');?></h1>
								 <ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#home"><?php _e('General Settings','weblizar');?></a></li>
									<li><a data-toggle="tab" href="#menu1"><?php _e('Slider Settings','weblizar');?></a></li>
									<li><a data-toggle="tab" href="#menu2"><?php _e('Service Settings','weblizar');?></a></li>
									<li><a data-toggle="tab" href="#menu3"><?php _e('Gallery Settings','weblizar');?></a></li>
									<li><a data-toggle="tab" href="#menu4"><?php _e('Blog Settings','weblizar');?></a></li>
									<li><a data-toggle="tab" href="#menu5"><?php _e('Testimoinal Settings','weblizar');?></a></li>
									<li><a data-toggle="tab" href="#menu6"><?php _e('Client Settings','weblizar');?></a></li>
									<li><a data-toggle="tab" href="#menu7"><?php _e('Call Out Settings','weblizar');?></a></li>
									
								  </ul>
									<div class="tab-content">
										<div id="home" class="tab-pane fade in active">
											<form method="post" id="weblizar_theme_options_general">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_general_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_general_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_general_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('general')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('general');">
												</div>
												<div class="col-md-6 form-group checkbox">
													<label><?php _e('Home-Page or Custom Page','weblizar'); ?></label> <br/>
													<div class="info">
														<input data-toggle="toggle" data-offstyle="off" type="checkbox" <?php if($wl_theme_options['front_page']=='on') echo "checked='checked'"; ?> id="front_page" name="front_page" >
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Enable Custom Static Front-Page.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a> </label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Your Site Logo','weblizar'); ?></label> <br/>	
													<div class="col-md-6 no-pad">
														<div class="row">
															<input class="form-control wp-img" type="text" value="<?php if($wl_theme_options['upload_image_logo']!='') { echo esc_attr($wl_theme_options['upload_image_logo']); } ?>" id="upload_image_logo" name="upload_image_logo" size="36" class="upload has-file"/>
															<button type="button" class="btn upload_image_button"><?php _e('Custom-Logo','weblizar'); ?></button>
														</div>
													</div>	
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Add your site logo here suggested size is 150X50','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-6 form-group checkbox">
													<label><?php _e('Custom Cover Background','weblizar'); ?></label> <br/>
													<div class="info">
														<input data-toggle="toggle" data-offstyle="off" type="checkbox" <?php if($wl_theme_options['custom_cover']=='on') echo "checked='checked'"; ?> id="custom_cover" name="custom_cover" >
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Enable Custom Background For Cover.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a> </label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Breadcrmps Background Image','weblizar'); ?></label> <br/>	
													<div class="col-md-6 no-pad">
														<div class="row">
															<input class="form-control wp-img" type="text" value="<?php if($wl_theme_options['cover_bg_image']!='') { echo esc_attr($wl_theme_options['cover_bg_image']); } ?>" id="cover_bg_image" name="cover_bg_image" size="36" class="upload has-file"/>
															<button type="button" class="btn upload_image_button"><?php _e('Upload Image','weblizar'); ?></button>
														</div>
													</div>	
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Add Breadcrmps Bakcground Image Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Logo Height','weblizar'); ?></label> <br/>
													<div class="col-md-6 form-group">
														<input class="form-control"  type="text" name="logo_height" id="logo_height" value="<?php echo $wl_theme_options['logo_height']; ?>" > 
													</div>
													<div class="col-md-6 form-group">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Default Logo Height : 55px, if you want to increase than specify your value','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Logo Width','weblizar'); ?></label> <br/>
													<div class="col-md-6 form-group">
														<input  class="form-control" type="text" name="logo_width" id="logo_width"  value="<?php echo $wl_theme_options['logo_width']; ?>" > 
													</div>
													<div class="col-md-6 form-group">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Default Logo Width : 150px, if you want to increase than specify your value','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-6 form-group checkbox">
													<label><?php _e('Fixed Header','weblizar'); ?></label> <br/>
													<div class="info">
														<input data-toggle="toggle" data-offstyle="off" type="checkbox" <?php if($wl_theme_options['fix_header']=='on') echo "checked='checked'"; ?> id="fix_header" name="fix_header" >
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Enable Fixed Header.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a> </label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Your Site Favicon','weblizar'); ?></label> <br/>	
													<div class="col-md-6 no-pad">
														<div class="row">
															<input class="form-control wp-img" type="text" value="<?php if($wl_theme_options['upload_image_favicon']!='') { echo esc_attr($wl_theme_options['upload_image_favicon']); } ?>" id="upload_image_favicon" name="upload_image_favicon" size="36" class="upload has-file"/>
															<input type="button" id="upload_button" value="Favicon Icon" class="btn upload_image_button" />
														</div>
													</div>	
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Make sure you upload .ico image type which is not more then 25X25 px.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Google Analytic Tracking Code ','weblizar'); ?><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Paste your Google Analytics tracking code here. This will be added into themes footer. Copy only scripting code i.e no need to use script tag ','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													<textarea class="form-control" rows="8" cols="8" id="google_analytics" name="google_analytics" ><?php if($wl_theme_options['google_analytics']!='') { echo esc_attr($wl_theme_options['google_analytics']); } ?></textarea>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Custom CSS Editor ','weblizar'); ?><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('This is a powerful feature provided here. No need to use custom css plugin, just paste your css code and see the magic.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													<textarea class="form-control" rows="8" cols="8" id="custom_css" name="custom_css"><?php if($wl_theme_options['custom_css']!='') { echo esc_attr($wl_theme_options['custom_css']); } ?></textarea>
												</div>
												<div class="row restore">
													<input type="hidden" value="1" id="weblizar_settings_save_general" name="weblizar_settings_save_general" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('general');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('general')" >
												</div>
											</form>
										</div>
										<div id="menu1" class="tab-pane fade">
											<form method="post" id="weblizar_theme_options_home-slider">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-slider_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-slider_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-slider_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-slider')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-slider');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Auto Play','weblizar'); ?></label> <br/>
													<div class="col-md-6">
													<label class="checkbox-inline">
													<input data-toggle="toggle" data-offstyle="off" type="checkbox" id="slide_autoplay" name="slide_autoplay" >
													</label>
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('AutoPlay Slider','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Pause On Hover','weblizar'); ?></label> <br/>
													<div class="col-md-6">
													<label class="checkbox-inline">
													<input data-toggle="toggle" data-offstyle="off" type="checkbox" id="slide_pause" name="slide_pause" >
													</label>
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Pause Slider on hover.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Slide show Interval','weblizar') ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="500" max="10000" min="2000" value="" class="hc_range">
													<input type="text" class="form-control hc_text" name="slide_interval" value="<?php echo $wl_theme_options['slide_interval']; ?>" id="slide_interval" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Select Slide show Interval.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12" >
														<label><?php _e('Rotate Slider','weblizar'); ?></label> <br/>
														<div class="col-md-6">
														<label class="checkbox-inline">
													<input data-toggle="toggle" data-offstyle="off" type="checkbox" id="slide_cycle" name="slide_cycle" >
													<a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Rotate Slides.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Slider Height','weblizar'); ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="10" max="650" min="400" class="hc_range">
													<input type="text" class="form-control hc_text" name="slide_height" value="<?php echo $wl_theme_options['slide_height']; ?>" id="slide_height" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Set Slider Height.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												
												<div class="row restore">
													<input type="hidden" value="1" id="weblizar_settings_save_home-slider" name="weblizar_settings_save_home-slider" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-slider');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-slider')" >
												</div>
											</form>
										</div>
										<div id="menu2" class="tab-pane fade">
											<form action="post" id="weblizar_theme_options_home-service">
												<div class="row restore">
													<div class="weblizar_settings_loding" id="weblizar_loding_home-service_image"></div>
													<div class="weblizar_settings_massage" id="weblizar_settings_save_home-service_success" ><?php _e('Options Data updated','weblizar');?></div>
													<div class="weblizar_settings_massage" id="weblizar_settings_save_home-service_reset" ><?php _e('Options data Reset','weblizar');?></div>
													<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-service')" >
													<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-service');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Service Title','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="service_heading" id="service_heading"  value="<?php if($wl_theme_options['service_heading']!='') { echo esc_attr($wl_theme_options['service_heading']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Service title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Service Description','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<textarea  class="form-control" type="text" name="service_desc" id="service_desc" ><?php if($wl_theme_options['service_desc']!='') { echo esc_attr($wl_theme_options['service_desc']); } ?></textarea>
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Service Description','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Service Icon','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="service_icon" id="service_icon"  value="<?php if($wl_theme_options['service_icon']!='') { echo esc_attr($wl_theme_options['service_icon']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Add Service Title Icon.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Home Page Service Column','weblizar'); ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="1" max="4" min="2" class="hc_range">
													<input type="text" class="form-control hc_text" name="service_layout" value="<?php echo $wl_theme_options['service_layout']; ?> " id="service_layout" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Select your home page Service Column.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Number Of Service on home page','weblizar'); ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="1" max="8" min="2" class="hc_range">
													<input type="text" class="form-control hc_text" name="service_count" value="<?php echo $wl_theme_options['service_count']; ?> " id="service_count" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Select your Number Of Service on home page.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="row restore">
													<input type="hidden" value="1" id="weblizar_settings_save_home-service" name="weblizar_settings_save_home-service" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-service');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-service')" >
												</div>
											</form>
										</div>
										<div id="menu3" class="tab-pane fade">
											<form action="post" id="weblizar_theme_options_home-portfolio" >
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-portfolio_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-portfolio_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-portfolio_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-portfolio')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-portfolio');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Gallery Title','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="gallery_heading" id="gallery_heading"  value="<?php if($wl_theme_options['gallery_heading']!='') { echo esc_attr($wl_theme_options['gallery_heading']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Gallery title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Gallery Description','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="gallery_desc" id="gallery_desc"  value="<?php if($wl_theme_options['gallery_desc']!='') { echo esc_attr($wl_theme_options['gallery_desc']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Gallery Description','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Gallery Icon','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="gallery_icon" id="gallery_icon"  value="<?php if($wl_theme_options['gallery_icon']!='') { echo esc_attr($wl_theme_options['gallery_icon']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Add Gallery Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Gallery Column','weblizar'); ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="1" max="5" min="3" class="hc_range">
													<input type="text" class="form-control hc_text" name="gallery_column" value="<?php echo $wl_theme_options['gallery_column']; ?> " id="gallery_column" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Select Gallery Column Layout.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Number of Gallery Items','weblizar'); ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="2" max="10" min="6" class="hc_range">
													<input type="text" class="form-control hc_text" name="gallery_count" value="<?php echo $wl_theme_options['gallery_count']; ?> " id="gallery_count" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Numbers of Gallery Item on home page.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="row restore">
													<input type="hidden" value="1" id="weblizar_settings_save_home-portfolio" name="weblizar_settings_save_home-portfolio" />			
												<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-portfolio');">
												<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-portfolio')" >
												</div>
											</form>
										</div>
										<div id="menu4" class="tab-pane fade">
											<form action="post" id="weblizar_theme_options_home-blog">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-blog_image"></div>
													<div class="weblizar_settings_massage" id="weblizar_settings_save_home-blog_success" ><?php _e('Options Data updated','weblizar');?></div>
													<div class="weblizar_settings_massage" id="weblizar_settings_save_home-blog_reset" ><?php _e('Options data Reset','weblizar');?></div>
													<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-blog')" >
													<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-blog');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Blog Title','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="blog_heading" id="blog_heading"  value="<?php if($wl_theme_options['blog_heading']!='') { echo esc_attr($wl_theme_options['blog_heading']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Blog title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Blog Description','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="blog_desc" id="blog_desc"  value="<?php if($wl_theme_options['blog_desc']!='') { echo esc_attr($wl_theme_options['blog_desc']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Blog Description','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Blog Icon','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="blog_icon" id="blog_icon"  value="<?php if($wl_theme_options['blog_icon']!='') { echo esc_attr($wl_theme_options['blog_icon']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Blog Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Blog Slide Item','weblizar'); ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="1" max="6" min="2" class="hc_range">
													<input type="text" class="form-control hc_text" name="blog_slide_item" value="<?php echo $wl_theme_options['blog_slide_item']; ?> " id="blog_slide_item" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Select your Blog Slide Items.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="row restore">
													<input type="hidden" value="1" id="weblizar_settings_save_home-blog" name="weblizar_settings_save_home-blog" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-blog');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-blog')" >
												</div>
											</form>
										</div>
										<div id="menu5" class="tab-pane fade">
											<form action="post" id="weblizar_theme_options_home-testi">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-testi_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-testi_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-testi_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-testi')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-testi');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Testimonial Title','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="testi_heading" id="testi_heading"  value="<?php if($wl_theme_options['testi_heading']!='') { echo esc_attr($wl_theme_options['testi_heading']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Testimonial title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Testimonial Icon','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="testi_icon" id="testi_icon"  value="<?php if($wl_theme_options['testi_icon']!='') { echo esc_attr($wl_theme_options['testi_icon']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Testimonial Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Background Image','weblizar'); ?></label><br/>
													<div class="col-md-6 no-pad">
													<div class="row">
														<input class="form-control wp-img" type="text" value="<?php if($wl_theme_options['testi_background']!='') { echo esc_attr($wl_theme_options['testi_background']); } ?>" id="testi_background" name="testi_background" size="36" class="upload has-file"/>
														<input type="button" id="upload_button" value="Background Image" class="btn upload_image_button" />
														</div>
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Add background image here.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Testimonial Slide Item','weblizar'); ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="2" max="9" min="3" class="hc_range">
													<input type="text" class="form-control hc_text" name="testi_num" value="<?php echo $wl_theme_options['testi_num']; ?> " id="testi_num" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Select your Testimonial Slide Items.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="row restore">
													<input type="hidden" value="1" id="weblizar_settings_save_home-testi" name="weblizar_settings_save_home-testi" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-testi');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-testi')" >
												</div>
											</form>
										</div>
										<div id="menu6" class="tab-pane fade">
											<form action="post" id="weblizar_theme_options_home-client">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-client_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-client_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-client_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-client')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-client');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Client Title','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="client_heading" id="client_heading"  value="<?php if($wl_theme_options['client_heading']!='') { echo esc_attr($wl_theme_options['client_heading']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Cleint title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Client Description','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="client_desc" id="client_desc"  value="<?php if($wl_theme_options['client_desc']!='') { echo esc_attr($wl_theme_options['client_desc']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Cleint Description','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Client Icon','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="client_icon" id="client_icon"  value="<?php if($wl_theme_options['client_icon']!='') { echo esc_attr($wl_theme_options['client_icon']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Cleint Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												
												<div class="col-md-12 form-group">
													<label><?php _e('Client Slide Item','weblizar'); ?></label><br/>
													<div class="col-md-6">
													<input type="range" step="2" max="10" min="6" class="hc_range">
													<input type="text" class="form-control hc_text" name="client_count" value="<?php echo $wl_theme_options['client_count']; ?> " id="client_count" readonly  />
													</div>
													<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Select your Client Slide Items.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="row restore">
													<input type="hidden" value="1" id="weblizar_settings_save_home-client" name="weblizar_settings_save_home-client" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-client');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-client')" >
												</div>
											</form>
										</div>
										<div id="menu7" class="tab-pane fade">
											<form action="post" id="weblizar_theme_options_home-call-out">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-call-out_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-call-out_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-call-out_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-call-out')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-call-out');">
												</div>
												
												<div class="col-md-12 form-group">
													<label><?php _e('Call Out Area Description','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="callout_text" id="callout_text"  value="<?php if($wl_theme_options['callout_text']!='') { echo esc_attr($wl_theme_options['callout_text']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your call out  Description','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Button Text','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="callout_button" id="callout_button"  value="<?php if($wl_theme_options['callout_button']!='') { echo esc_attr($wl_theme_options['callout_button']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your call out Button Text ','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12" >
														<label><?php _e('Button Link Target','weblizar'); ?></label> <br/>
														<div class="col-md-6">
														<label class="checkbox-inline">
													<input data-toggle="toggle" data-offstyle="off" type="checkbox" id="call_out_button_target" name="call_out_button_target" >
													</label>
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Open link in a new window/tab.','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
												<label><?php _e('Button Link','weblizar'); ?></label> <br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="callout_link" id="callout_link"  value="<?php if($wl_theme_options['callout_link']!='') { echo esc_attr($wl_theme_options['callout_link']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your call out area button link','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label><br/>
													</div>
												</div>
												
												<div class="row restore">
													<input type="hidden" value="1" id="weblizar_settings_save_home-call-out" name="weblizar_settings_save_home-call-out" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-call-out');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-call-out')" >
												</div>
											</form>
										</div>
									</div>
						</div>
					</div>
					<div class="tab-pane col-md-12" id="deppt">
						<div class="col-md-12 option">
							<h1><?php _e('Department Options', 'weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#foot-home"><?php _e('Department Settings', 'weblizar'); ?></a></li>
							</ul>
							<div class="tab-content">
								<div id="foot-home" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-deppt">
										<div class="row restore">
											<div class="weblizar_settings_loding" id="weblizar_loding_home-deppt_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-deppt_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-deppt_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-deppt')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-deppt');">
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Department Title','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="deptt_heading" id="deptt_heading"  value="<?php if($wl_theme_options['deptt_heading']!='') { echo esc_attr($wl_theme_options['deptt_heading']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Department title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
													<label><?php _e('Department Description','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<textarea  class="form-control" type="text" name="deptt_desc" id="deptt_desc" ><?php if($wl_theme_options['deptt_desc']!='') { echo esc_attr($wl_theme_options['deptt_desc']); } ?></textarea>
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Department Description','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Department Icon','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="deptt_icon" id="deptt_icon"  value="<?php if($wl_theme_options['deptt_icon']!='') { echo esc_attr($wl_theme_options['deptt_icon']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Department Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
										</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Background Image','weblizar'); ?></label><br/>
													<div class="col-md-6 no-pad">
													<div class="row">
														<input class="form-control wp-img" type="text" value="<?php if($wl_theme_options['deptt_background']!='') { echo esc_attr($wl_theme_options['deptt_background']); } ?>" id="deptt_background" name="deptt_background" size="36" class="upload has-file"/>
														<input type="button" id="upload_button" value="Background Image" class="btn upload_image_button" />
														</div>
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Add background image here.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
										<div class="row restore">
											<input type="hidden" value="1" id="weblizar_settings_save_home-deppt" name="weblizar_settings_save_home-deppt" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-deppt');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-deppt')" >
										</div>
									</form>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane col-md-12" id="members">
						<div class="col-md-12 option">
							<h1><?php _e('Members Options', 'weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#foot-home"><?php _e('Members Settings', 'weblizar'); ?></a></li>
							</ul>
							<div class="tab-content">
								<div id="foot-home" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-members">
										<div class="row restore">
											<div class="weblizar_settings_loding" id="weblizar_loding_home-members_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-members_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-members_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-members')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-members');">
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Member Title','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="member_heading" id="member_heading"  value="<?php if($wl_theme_options['member_heading']!='') { echo esc_attr($wl_theme_options['member_heading']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Member title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Member Icon','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="member_icon" id="member_icon"  value="<?php if($wl_theme_options['member_icon']!='') { echo esc_attr($wl_theme_options['member_icon']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Member Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
										</div>
												</div>
										<div class="row restore">
											<input type="hidden" value="1" id="weblizar_settings_save_home-members" name="weblizar_settings_save_home-members" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-members');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-members')" >
										</div>
									</form>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane col-md-12" id="facts">
						<div class="col-md-12 option">
							<h1><?php _e('Fun Facts Options', 'weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#foot-home"><?php _e('Fun-facts Settings', 'weblizar'); ?></a></li>
							</ul>
							<div class="tab-content">
								<div id="foot-home" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-facts">
										<div class="row restore">
											<div class="weblizar_settings_loding" id="weblizar_loding_home-facts_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-facts_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-facts_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-facts')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-facts');">
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Fun-facts Title','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="facts_heading" id="facts_heading"  value="<?php if($wl_theme_options['facts_heading']!='') { echo esc_attr($wl_theme_options['facts_heading']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Fun-facts title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
													<label><?php _e('Fun-facts Description','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<textarea  class="form-control" type="text" name="facts_desc" id="facts_desc" ><?php if($wl_theme_options['facts_desc']!='') { echo esc_attr($wl_theme_options['facts_desc']); } ?></textarea>
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Fun-facts Description','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
										</div>
										<div class="col-md-12 form-group">
													<label><?php _e('Background Image','weblizar'); ?></label><br/>
													<div class="col-md-6 no-pad">
													<div class="row">
														<input class="form-control wp-img" type="text" value="<?php if($wl_theme_options['facts_background']!='') { echo esc_attr($wl_theme_options['facts_background']); } ?>" id="facts_background" name="facts_background" size="36" class="upload has-file"/>
														<input type="button" id="upload_button" value="Background Image" class="btn upload_image_button" />
														</div>
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Add background image here.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Fun-facts Icon','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="facts_icon" id="facts_icon"  value="<?php if($wl_theme_options['facts_icon']!='') { echo esc_attr($wl_theme_options['facts_icon']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Fun-facts Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<?php for($i=1; $i<=4; $i++){ ?>
										<div class="col-md-12 form-group">
											<label><?php _e('Fun-facts ','weblizar'); echo $i; ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="fact_icon_<?php echo $i; ?>" id="fact_icon_<?php echo $i; ?>"  value="<?php if($wl_theme_options['fact_icon_'.$i]!='') { echo esc_attr($wl_theme_options['fact_icon_'.$i]); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Fun-facts Icon ','weblizar'); echo $i; ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<div class="col-md-6">
												<input  class="form-control" type="text" name="fact_value_<?php echo $i; ?>" id="fact_value_<?php echo $i; ?>"  value="<?php if($wl_theme_options['fact_value_'.$i]!='') { echo esc_attr($wl_theme_options['fact_value_'.$i]); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Fun-facts Value ','weblizar'); echo $i;?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<div class="col-md-6">
												<input  class="form-control" type="text" name="fact_name_<?php echo $i; ?>" id="fact_name_<?php echo $i; ?>"  value="<?php if($wl_theme_options['fact_name_'.$i]!='') { echo esc_attr($wl_theme_options['fact_name_'.$i]); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Fun-facts Name ','weblizar'); echo $i; ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<?php } ?>
										<div class="row restore">
											<input type="hidden" value="1" id="weblizar_settings_save_home-facts" name="weblizar_settings_save_home-facts" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-facts');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-facts')" >
										</div>
									</form>
								</div>
							</div>
						</div>	
					</div>
					 <div class="tab-pane col-md-12" id="feedback">
						<div class="col-md-12 option">
							<h1><?php _e('Feedback Section Options', 'weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#foot-home"><?php _e('Feedback Settings', 'weblizar'); ?></a></li>
							</ul>
							<div class="tab-content">
								<div id="foot-home" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-feedback">
										<div class="row restore">
											<div class="weblizar_settings_loding" id="weblizar_loding_home-feedback_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-feedback_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-feedback_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-feedback')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-feedback');">
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Contact Mail','weblizar'); ?></label><br/>
											<div class="col-md-6">
											<?php $email =get_option('admin_email'); ?>
												<input  class="form-control" type="text" name="feedback_mail" id="feedback_mail"  value="<?php if($wl_theme_options['feedback_mail']!=''){ echo $wl_theme_options['feedback_mail'];}else{ echo $email; } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Feedback Mail Address','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Feedback Title','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="feedback_heading" id="feedback_heading"  value="<?php if($wl_theme_options['feedback_heading']!='') { echo esc_attr($wl_theme_options['feedback_heading']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Feedback title','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Feedback Icon','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="feedback_icon" id="feedback_icon"  value="<?php if($wl_theme_options['feedback_icon']!='') { echo esc_attr($wl_theme_options['feedback_icon']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Feedback Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Feedback Button Text','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="feedback_btn" id="feedback_btn"  value="<?php if($wl_theme_options['feedback_btn']!='') { echo esc_attr($wl_theme_options['feedback_btn']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here your Feedback Button Text','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="row restore">
											<input type="hidden" value="1" id="weblizar_settings_save_home-feedback" name="weblizar_settings_save_home-feedback" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-feedback');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-feedback')" >
										</div>
									</form>
								</div>
							</div>
						</div>	
					</div>
					 <div class="tab-pane col-md-12" id="skin">
						<div class="col-md-12 option">
							<h1><?php _e('Skin Layout','weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#skin-layout"><?php _e('Skin Layout Setting', 'weblizar'); ?></a></li>
						    </ul>
							<div class="tab-content">
								<div id="skin-layout" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-skin">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-skin_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-skin_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-skin_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-skin')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-skin');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Theme Color','weblizar'); ?></label><br/>
													<div class="col-md-2 ">
														<div class="color-picker" style="position: relative;">
															<input type="text" class="color-control" name="theme_color" id="color" value="<?php echo $wl_theme_options['theme_color']; ?>" />
															<div style="position: absolute;" id="colorpicker"></div>
														</div>	
													</div>
													<div class="col-md-4 ">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Select Color for your Theme','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a> </label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Theme Layout','weblizar'); ?></label><br/>
													<div class="col-md-2 ">
														<?php $site_layout =$wl_theme_options['site_layout']; ?>
														<select id="site_layout" name="site_layout" class="form-control">
															<option <?php echo selected($site_layout, 'wide' ); ?> ><?php _e('wide','weblizar'); ?></option>
															<option <?php echo selected($site_layout, 'boxed' ); ?> ><?php _e('boxed','weblizar'); ?></option>
														</select>
													</div>
													<div class="col-md-6 ">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('With the Help of box layout you can show sites background','weblizar'); ?> "><i class="fa fa-info-circle tt-icon"></i></a> </label>
													</div>
												</div>
												<div class="col-md-6 form-group checkbox">
													<label><?php _e('Custom Background','weblizar'); ?></label> <br/>
													<div class="info">
														<input data-toggle="toggle" data-offstyle="off" type="checkbox" <?php if($wl_theme_options['custom_bg']=='on') echo "checked='checked'"; ?> id="custom_bg" name="custom_bg" >
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Enable Custom Background.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a> </label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Custom Background','weblizar'); ?></label><br/>
													<div class="col-md-5">
													<input type="text" class="form-control" name="site_bg" id="site_bg" value="<?php echo $wl_theme_options['site_bg']; ?>" readonly />
														<ul class="color-code">
														<?php for($i=1; $i<=20; $i++){ ?>
															<li><img src="<?php echo get_template_directory_uri().'/images/bg/'.$i.'.jpg';?>" <?php if($wl_theme_options['site_bg']==get_template_directory_uri().'/images/bg/'.$i.'.jpg') echo 'class="hc_active"'; ?> height="50" width="50" /></li>
														<?php } ?>
														</ul>
													</div>
												</div>
											<div class="row restore">
											<input type="hidden" value="1" id="weblizar_settings_save_home-skin" name="weblizar_settings_save_home-skin" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-skin');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-skin')" >
										</div>
									</form>
								</div>
							</div>
						</div>	
					 </div>
					 
					<div class="tab-pane col-md-12" id="contact">
						<div class="col-md-12 option">
							<h1><?php _e('Contact Option','weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#contact-setting"><?php _e('Contact Option','weblizar'); ?></a></li>
						    </ul>
							<div class="tab-content">
								<div id="contact-setting" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-contact">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-contact_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-contact_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-contact_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-contact')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-contact');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Contact Details Heading','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="contact_detail_heading" id="contact_detail_heading"  value="<?php if($wl_theme_options['contact_detail_heading']!='') { echo esc_attr($wl_theme_options['contact_detail_heading']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Your Contact Details Heading','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<?php for($i=1; $i<=4; $i++){ ?>
												<div class="col-md-12 form-group">
													<label><?php _e('Detail Heading ','weblizar'); echo $i; ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="contact_heading_<?php echo $i; ?>" id="contact_heading_<?php echo $i; ?>"  value="<?php if($wl_theme_options['contact_heading_'.$i]!='') { echo esc_attr($wl_theme_options['contact_heading_'.$i]); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Your Details Heading ','weblizar'); echo $i; ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Detail Icon ','weblizar'); echo $i; ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="contact_icon_<?php echo $i; ?>" id="contact_icon_<?php echo $i; ?>"  value="<?php if($wl_theme_options['contact_icon_'.$i]!='') { echo esc_attr($wl_theme_options['contact_icon_'.$i]); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Your Details Icon ','weblizar'); echo $i;?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Detail Information ','weblizar'); echo $i; ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="contact_detail_<?php echo $i; ?>" id="contact_detail_<?php echo $i; ?>"  value="<?php if($wl_theme_options['contact_detail_'.$i]!='') { echo esc_attr($wl_theme_options['contact_detail_'.$i]); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Your Details Information ','weblizar'); echo $i;?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
											<?php } ?>
											<div class="col-md-12 form-group">
												<label><?php _e('Contact Form Heading','weblizar'); ?></label><br/>
												<div class="col-md-6">
													<input  class="form-control" type="text" name="contact_form_heading" id="contact_form_heading"  value="<?php if($wl_theme_options['contact_form_heading']!='') { echo esc_attr($wl_theme_options['contact_form_heading']); } ?>" >
												</div>
												<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Contact Form Heading','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
												</div>
											</div>
											<div class="col-md-12 form-group">
												<label><?php _e('Contact Form Button Text','weblizar'); ?></label><br/>
												<div class="col-md-6">
													<input  class="form-control" type="text" name="contact_form_button" id="contact_form_button"  value="<?php if($wl_theme_options['contact_form_button']!='') { echo esc_attr($wl_theme_options['contact_form_button']); } ?>" >
												</div>
												<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Contact Form Button Text','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
												</div>
											</div>
											<div class="col-md-12 form-group">
												<label><?php _e('Contact Callout Icon','weblizar'); ?></label><br/>
												<div class="col-md-6">
													<input  class="form-control" type="text" name="contact_footer_icon" id="contact_footer_icon"  value="<?php if($wl_theme_options['contact_footer_icon']!='') { echo esc_attr($wl_theme_options['contact_footer_icon']); } ?>" >
												</div>
												<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Contact Callout Icon','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
												</div>
											</div>
											<div class="col-md-12 form-group">
												<label><?php _e('Contact Callout Text','weblizar'); ?></label><br/>
												<div class="col-md-6">
													<input  class="form-control" type="text" name="contact_footer_text" id="contact_footer_text"  value="<?php if($wl_theme_options['contact_footer_text']!='') { echo esc_attr($wl_theme_options['contact_footer_text']); } ?>" >
												</div>
												<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Contact Callout Text','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
												</div>
											</div>
											<div class="col-md-12 form-group">
												<label><?php _e('Contact Callout Contact Number','weblizar'); ?></label><br/>
												<div class="col-md-6">
													<input  class="form-control" type="text" name="contact_footer_number" id="contact_footer_number"  value="<?php if($wl_theme_options['contact_footer_number']!='') { echo esc_attr($wl_theme_options['contact_footer_number']); } ?>" >
												</div>
												<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Contact Callout Contact Number','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
												</div>
											</div>
											<div class="col-md-12 form-group">
												<label><?php _e('Google Map Embeded URL','weblizar'); ?></label><br/>
												<div class="col-md-6">
													<textarea  class="form-control" type="text" name="google_map_url" id="google_map_url" ><?php if($wl_theme_options['google_map_url']!='') { echo esc_attr($wl_theme_options['google_map_url']); } ?></textarea>
												</div>
												<div class="col-md-6">
													<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Google Map Embeded URL','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
												</div>
											</div>
											<div class="row restore">
											<input type="hidden" value="1" id="weblizar_settings_save_home-contact" name="weblizar_settings_save_home-contact" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-contact');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-contact')" >
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
						
					  <div class="tab-pane col-md-12" id="footer">
						<div class="col-md-12 option">
							<h1><?php _e('Footer Options','weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#footer-setting"><?php _e('Footer Options','weblizar'); ?></a></li>
						    </ul>
							<div class="tab-content">
								<div id="footer-setting" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-footer">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-footer_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-footer_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-footer_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-footer')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-footer');">
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Footer Copyright Text','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="footer_copyright_text" id="footer_copyright_text"  value="<?php if($wl_theme_options['footer_copyright_text']!='') { echo esc_attr($wl_theme_options['footer_copyright_text']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Enter Footer Copyright Text Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Footer Link','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="footer_link" id="footer_link"  value="<?php if($wl_theme_options['footer_link']!='') { echo esc_attr($wl_theme_options['footer_link']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Enter Footer Copyright Link Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
												<div class="col-md-12 form-group">
													<label><?php _e('Footer Link Text','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="footer_link_text" id="footer_link_text"  value="<?php if($wl_theme_options['footer_link_text']!='') { echo esc_attr($wl_theme_options['footer_link_text']); } ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Enter Footer Link Text Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
												</div>
											<div class="row restore">
											<input type="hidden" value="1" id="weblizar_settings_save_home-footer" name="weblizar_settings_save_home-footer" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-footer');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-footer')" >
										</div>
									</form>
								</div>
								
							</div>
						</div>	
					  </div>
					  <div class="tab-pane col-md-12" id="social">
						<div class="col-md-12 option">
							<h1><?php _e('Social Media Options','weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#page-social"><?php _e('Social Media Options','weblizar'); ?></a></li>
								
						    </ul>
							<div class="tab-content">
								<div id="page-social" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-social">
												<div class="row restore">
												<div class="weblizar_settings_loding" id="weblizar_loding_home-social_image"></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-social_success" ><?php _e('Options Data updated','weblizar');?></div>
												<div class="weblizar_settings_massage" id="weblizar_settings_save_home-social_reset" ><?php _e('Options data Reset','weblizar');?></div>
												<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-social')" >
												<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-social');">
												</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Show Social Icons','weblizar'); ?></label> <br/>
											<div class="col-md-3">
											<label class="checkbox-inline">
											<input data-toggle="toggle" data-offstyle="off" type="checkbox" <?php if($wl_theme_options['header_social_icon']=='on') { echo 'checked="checked"' ; } ?> id="header_social_icon" name="header_social_icon" >
											</label>
											<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Show Social Icons On Header','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
											<div class="col-md-3">
											<label class="checkbox-inline">
											<input data-toggle="toggle" data-offstyle="off" type="checkbox" <?php if($wl_theme_options['footer_social_icon']=='on') { echo 'checked="checked"' ; } ?> id="footer_social_icon" name="footer_social_icon" >
											</label>
											<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Show Social Icons On Footer','weblizar'); ?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
													<label><?php _e('Header Contact Number','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="social_phone" id="social_phone"  value="<?php echo esc_attr($wl_theme_options['social_phone']); ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Enter Header Contact Number Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
										</div>
										<div class="col-md-12 form-group">
													<label><?php _e('Header Contact Email','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="social_email" id="social_email"  value="<?php echo esc_attr($wl_theme_options['social_email']); ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Enter Header Contact Email Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
										</div>
										<div class="col-md-12 form-group">
													<label><?php _e('Header Text','weblizar'); ?></label><br/>
													<div class="col-md-6">
														<input  class="form-control" type="text" name="social_timing" id="social_timing"  value="<?php echo esc_attr($wl_theme_options['social_timing']); ?>" >
													</div>
													<div class="col-md-6">
														<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Enter Header Text Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
													</div>
										</div>
										<?php for($i=1; $i<=6; $i++){ ?>
										<div class="col-md-12 form-group">
											<label><?php _e('Social Icon ','weblizar'); echo $i; ?></label><br/>
											<div class="col-md-10">
												<input  class="form-control" type="text" name="social_icon_<?php echo $i; ?>" id="social_icon_<?php echo $i; ?>"  value="<?php if($wl_theme_options['social_icon_'.$i]!='') { echo esc_attr($wl_theme_options['social_icon_'.$i]); } ?>" >
											</div>
											<div class="col-md-2">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Enter FontAwesome Social Icon Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
											<div class="col-md-10">
												<input  class="form-control" type="text" name="social_link_<?php echo $i; ?>" id="social_link_<?php echo $i; ?>"  value="<?php if($wl_theme_options['social_link_'.$i]!='') { echo esc_attr($wl_theme_options['social_link_'.$i]); } ?>" >
											</div>
											<div class="col-md-2">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Enter Social Link Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<?php } ?>
										<div class="row restore">
											<input type="hidden" value="1" id="weblizar_settings_save_home-social" name="weblizar_settings_save_home-social" />			
													<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-social');">
													<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-social')" >
										</div>
									</form>
								</div>
							</div>
						</div>	
					  </div>
					 
					  <div class="tab-pane col-md-12" id="layout">
						<div class="col-md-12 option">
							<h1> <?php _e('Home Page Manager','weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#home-layout"><?php _e('Home Page Manager','weblizar'); ?></a></li>
							</ul>
							<div class="tab-content">
								<div id="home-layout" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_layoutmanger">
										<div class="row restore">
										<div class="weblizar_settings_loding" id="weblizar_loding_layoutmanger_image"></div>
										<div class="weblizar_settings_massage" id="weblizar_settings_save_layoutmanger_success" ><?php _e('Options Data updated','weblizar');?></div>
										<div class="weblizar_settings_massage" id="weblizar_settings_save_layoutmanger_reset" ><?php _e('Options data Reset','weblizar');?></div>
										<input class="button button-primary right" type="button" value="Save Options" id="weblizar_home_layout_save_button" >
										<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('layoutmanger');">
										</div>
										<div class="section">
											<table  class="form-table">						
												<div class="dhe-example-section-content">
													<div id="weblizar_home_page">
														<div class="column left first">
															<font color="#333333" size="+2"> <?php _e('Disabled','weblizar');?></font><p></p>							
															<div class="sortable-list" id="disable">
																<?php 	
																	$data = $wl_theme_options['home_page_layout'];									
																	$home_page_data=array('callout','services','deptt','doctor','testimonial','blog','gallery','facts','clients','appointment');
																	$todisable=array_diff($home_page_data, $data);
																	if($todisable != '')
																	{	foreach($todisable as $value)
																		{ ?>
																			<div class="sortable-item" id="<?php echo $value ?>">
																			<?php echo ucfirst($value); ?>
																			</div>
																<?php 	}		 
																	} ?>
															</div>
														</div>
														<div class="column left">
															<font color="#333333" size="+2"> <?php _e('Enabled','weblizar'); ?></font><p></p>
															<div class="sortable-list" id="enable">
																<?php 
																$enable =$wl_theme_options['home_page_layout'];								
																if($enable[0]!="")
																{	foreach($enable as $value)
																	{ ?>
																		<div class="sortable-item" id="<?php echo $value ?>">
																		<?php  echo ucfirst($value); ?>
																		</div> <?php 
																	} 
																}								
																?>
															</div>
														</div>
													</div>			
												</div><!--end redify_frontpage--->
											</table>				
										</div>
										<div class="section">
										<p> <b><?php _e('Healthcare Slider will always remain top of the Home page','weblizar'); ?></b></p>
										<p> <b><?php _e('Note:','weblizar'); ?> </b> <?php _e('By default all the section are enable on Home Page. If you want to disable any section just drag this section to the disabled box.','weblizar'); ?><p>
										</div>
										<div class="row restore">
										<input type="hidden" value="1" id="weblizar_settings_save_layoutmanger" name="weblizar_settings_save_layoutmanger" />			
										<input class="button" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('layoutmanger');">
										<input class="button button-primary" type="button" value="Save Options" id="weblizar_home_layout_save" >
										</div>
									</form>
									<script type="text/javascript">
										jQuery(document).ready(function(){	
											//drag drop tabs
											jQuery('#weblizar_home_page .sortable-list').sortable({ connectWith: '#weblizar_home_page .sortable-list' });	
											
											// Get items id you can chose
											function weblizartems(weblizar)
											{
												var columns = [];
												jQuery(weblizar + ' div.sortable-list').each(function(){
													columns.push(jQuery(this).sortable('toArray').join(','));
												});
												return columns.join('|');
											}
											
											//onclick check id
											jQuery('#weblizar_home_layout_save').click(function(){ 
												var data = weblizartems('#weblizar_home_page');		
												var dataStringfirst = 'weblizar_home_data='+ data;
												
												jQuery('#weblizar_loding_layoutmanger_image').show();				 
												var url = '?page=weblizar';				 
													jQuery.ajax({
														dataType : 'html',
														type: 'POST',
														url : url,
														data : dataStringfirst,
														complete : function() {  },
														success: function(data) 
														{	jQuery('#weblizar_loding_layoutmanger_image').fadeOut();
															jQuery("#weblizar_settings_save_layoutmanger_success").show();
															jQuery("#weblizar_settings_save_layoutmanger_success").fadeOut(5000);
														}
												});
											});
											jQuery('#weblizar_home_layout_save_button').click(function(){ 
												var data = weblizartems('#weblizar_home_page');		
												var dataStringfirst = 'weblizar_home_data='+ data;
												
												jQuery('#weblizar_loding_layoutmanger_image').show();				 
												var url = '?page=weblizar';				 
													jQuery.ajax({
														dataType : 'html',
														type: 'POST',
														url : url,
														data : dataStringfirst,
														complete : function() {  },
														success: function(data) 
														{	jQuery('#weblizar_loding_layoutmanger_image').fadeOut();
															jQuery("#weblizar_settings_save_layoutmanger_success").show();
															jQuery("#weblizar_settings_save_layoutmanger_success").fadeOut(5000);
														}
												});
											});				
										});
									</script>
										<?php 
										if(isset($_POST['weblizar_settings_save_layoutmanger']))
										{	if($_POST['weblizar_settings_save_layoutmanger'] == 2) 
											{
												$wl_theme_options['home_page_layout']=array('callout','services','deptt','doctor','testimonial','blog','gallery','facts','clients','appointment');
												update_option('health_pro_options', $wl_theme_options);
											}
										}
										
										if(isset($_POST['weblizar_home_data']))
										{
											if($_POST['weblizar_home_data'] ) 
											{		
												/*send data hold*/
												$datashowredify = $_POST['weblizar_home_data'];
												$hold = strstr($datashowredify,'|');
												$datashowredify = str_replace('|', '' ,$hold);				
												$data = explode(",",$datashowredify);				
												/*data save*/
												$wl_theme_options['home_page_layout']=$data;
												/*update all field*/
												update_option('health_pro_options' , $wl_theme_options);				
											}
										}
									?>
								</div>
							</div>
						</div>	
					  </div>
					  <div class="tab-pane col-md-12" id="maintenance">
						<div class="col-md-12 option">
							<h1><?php _e('Maintainance Mode','weblizar'); ?> </h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#maintenance-home"><?php _e('Maintainance Mode','weblizar'); ?></a></li>
							</ul>
							<div class="tab-content">
								<div id="maintenance-home" class="tab-pane fade in active">
									<form action="post" id="weblizar_theme_options_home-maintenance">
									<div class="row restore">
									<div class="weblizar_settings_loding" id="weblizar_loding_home-maintenance_image"></div>
									<div class="weblizar_settings_massage" id="weblizar_settings_save_home-maintenance_success" ><?php _e('Options Data updated','weblizar');?></div>
									<div class="weblizar_settings_massage" id="weblizar_settings_save_home-maintenance_reset" ><?php _e('Options data Reset','weblizar');?></div>
									<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-maintenance')" >
									<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-maintenance');">
									</div>
										<div class="col-md-6 form-group checkbox">
											<label><?php _e('Enable Maintainance Mode','weblizar'); ?></label> <br/>
											<div class="info">
												<input data-toggle="toggle" data-offstyle="off" type="checkbox" <?php if($wl_theme_options['maintenance_switch']=='on') echo "checked='checked'"; ?> id="maintenance_switch" name="maintenance_switch" >
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Enable Maintainance Mode on Site.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a> </label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Maintainance Heading','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="maintenance_text" id="maintenance_text"  value="<?php if($wl_theme_options['maintenance_text']!='') { echo esc_attr($wl_theme_options['maintenance_text']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Maintenance Heading','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-6 form-group checkbox">
											<label><?php _e('Show Social Icons','weblizar'); ?></label> <br/>
											<div class="info">
												<input data-toggle="toggle" data-offstyle="off" type="checkbox" <?php if($wl_theme_options['maintenance_social']=='on') echo "checked='checked'"; ?> id="maintenance_social" name="maintenance_social" >
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Enable Maintainance Mode on Site.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a> </label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label id="maintenance_time-label"><?php _e('Time to Live','weblizar'); ?></label><br/>
											<div class="col-md-6">
													<input  class="form-control" type="text" name="maintenance_date" id="maintenance_date"  value="<?php if($wl_theme_options['maintenance_date']!='') { echo esc_attr($wl_theme_options['maintenance_date']); } ?>" placeholder="YYYY/MM/DD" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Add Live time here.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Background Image','weblizar'); ?></label> <br/>	
											<div class="col-md-6 no-pad">
												<div class="row">
													<input class="form-control wp-img" type="text" value="<?php if($wl_theme_options['maintenance_image']!='') { echo esc_attr($wl_theme_options['maintenance_image']); } ?>" id="maintenance_image" name="maintenance_image" size="36" class="upload has-file"/>
													<button type="button" class="btn upload_image_button"><?php _e('Upload Image','weblizar'); ?></button>
												</div>
											</div>	
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php _e('Add Bakcground Image Here','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="col-md-12 form-group">
											<label><?php _e('Video Background','weblizar'); ?></label><br/>
											<div class="col-md-6">
												<input  class="form-control" type="text" name="maintenance_video" id="maintenance_video"  value="<?php if($wl_theme_options['maintenance_video']!='') { echo esc_attr($wl_theme_options['maintenance_video']); } ?>" >
											</div>
											<div class="col-md-6">
												<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Add Youtube Video Embeded URL Here.','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
											</div>
										</div>
										<div class="row restore">
										<input type="hidden" value="1" id="weblizar_settings_save_home-maintenance" name="weblizar_settings_save_home-maintenance" />			
										<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-maintenance');">
										<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-maintenance')" >
									</div>
									</form>
								</div>
							</div>
						</div>	
					  </div>
					<div class="tab-pane col-md-12" id="curl">
						<div class="col-md-12 option">
							<h1><?php _e('Custom URLs Options','weblizar'); ?></h1>
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#custom-urls"><?php _e('Custom URLs Options','weblizar'); ?></a></li>
							</ul>
							<div class="tab-content">
								<div id="custom-urls" class="tab-pane fade in active">
								<form action="post" id="weblizar_theme_options_home-custom-url">
									<div class="row restore">
									<div class="weblizar_settings_loding" id="weblizar_loding_home-custom-url_image"></div>
									<div class="weblizar_settings_massage" id="weblizar_settings_save_home-custom-url_success" ><?php _e('Options Data updated','weblizar');?></div>
									<div class="weblizar_settings_massage" id="weblizar_settings_save_home-custom-url_reset" ><?php _e('Options data Reset','weblizar');?></div>
									<input class="button button-primary right" type="button" value="Save Options" onclick="weblizar_option_data_save('home-custom-url')" >
									<input class="button right" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-custom-url');">
									</div>
									<div class="col-md-12 form-group">
										<label><?php _e('Service Custom URL','weblizar'); ?></label><br/>
										<div class="col-md-6">
											<input  class="form-control" type="text" name="cpt_service" id="cpt_service"  value="<?php if($wl_theme_options['cpt_service']!='') { echo esc_attr($wl_theme_options['cpt_service']); } ?>" >
										</div>
										<div class="col-md-6">
											<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Service Custom Slug','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<label><?php _e('Portfolio Custom URL','weblizar'); ?></label><br/>
										<div class="col-md-6">
											<input  class="form-control" type="text" name="cpt_portfolio" id="cpt_portfolio"  value="<?php if($wl_theme_options['cpt_portfolio']!='') { echo esc_attr($wl_theme_options['cpt_portfolio']); } ?>" >
										</div>
										<div class="col-md-6">
											<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Portfolio Custom Slug','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<label><?php _e('Testimonial Custom URL','weblizar'); ?></label><br/>
										<div class="col-md-6">
											<input  class="form-control" type="text" name="cpt_testimonial" id="cpt_testimonial"  value="<?php if($wl_theme_options['cpt_testimonial']!='') { echo esc_attr($wl_theme_options['cpt_testimonial']); } ?>" >
										</div>
										<div class="col-md-6">
											<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Testimonial Custom Slug','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<label><?php _e('Department Custom URL','weblizar'); ?></label><br/>
										<div class="col-md-6">
											<input  class="form-control" type="text" name="cpt_deptt" id="cpt_deptt"  value="<?php if($wl_theme_options['cpt_deptt']!='') { echo esc_attr($wl_theme_options['cpt_deptt']); } ?>" >
										</div>
										<div class="col-md-6">
											<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Department Custom Slug','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
										</div>
									</div>
									<div class="col-md-12 form-group">
										<label><?php _e('Member Custom URL','weblizar'); ?></label><br/>
										<div class="col-md-6">
											<input  class="form-control" type="text" name="cpt_member" id="cpt_member"  value="<?php if($wl_theme_options['cpt_member']!='') { echo esc_attr($wl_theme_options['cpt_member']); } ?>" >
										</div>
										<div class="col-md-6">
											<label><a href="#" data-toggle="tooltip" data-placement="right" title="<?php  _e('Type here Member Custom Slug','weblizar');?>"><i class="fa fa-info-circle tt-icon"></i></a></label>
										</div>
									</div>
									<div class="row restore">
										<input type="hidden" value="1" id="weblizar_settings_save_home-custom-url" name="weblizar_settings_save_home-custom-url" />			
										<input class="button left" type="button" name="reset" value="Restore Defaults" onclick="weblizar_option_data_reset('home-custom-url');">
										<input class="button button-primary left" type="button" value="Save Options" onclick="weblizar_option_data_save('home-custom-url')" >
									</div>
								</form>
								</div>
							</div>
						</div>	
					</div>
					
					</div>