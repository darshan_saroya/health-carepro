/*Admin options pannal data value*/
	function weblizar_option_data_save(name) 
	{ 	//tinyMCE.triggerSave();
		var weblizar_settings_save= "#weblizar_settings_save_"+name;
		var weblizar_theme_options = "#weblizar_theme_options_"+name;
		var weblizar_settings_save_success = weblizar_settings_save+"_success";
		var loding_image ="#weblizar_loding_"+name;
		var weblizar_loding_image = loding_image +"_image";			
		
		jQuery(weblizar_loding_image).show();
		jQuery(weblizar_settings_save).val("1");        
	    jQuery.ajax({
				url:'themes.php?page=weblizar',
				type:'post',
				data : jQuery(weblizar_theme_options).serialize(),
				 success : function(data)
				 {  jQuery(weblizar_loding_image).fadeOut();						
					jQuery(weblizar_settings_save_success).show();
					jQuery(weblizar_settings_save_success).fadeOut(5000);
				}			
		});
	}	
/*Admin options value reset */
	function weblizar_option_data_reset(name) 
	{  
		var r=confirm("Do you want reset your theme setting!")
		if (r==true)
		{		var weblizar_settings_save= "#weblizar_settings_save_"+name;
				var weblizar_theme_options = "#weblizar_theme_options_"+name;
				var weblizar_settings_save_reset = weblizar_settings_save+"_reset";				
				jQuery(weblizar_settings_save).val("2");       
				jQuery.ajax({
				   url:'themes.php?page=weblizar',
				   type:'post',
				   data : jQuery(weblizar_theme_options).serialize(),
				   success : function(data){
					jQuery(weblizar_settings_save_reset).show();
					jQuery(weblizar_settings_save_reset).fadeOut(5000);
				}			
			});
		} else  {
		alert("Cancel! reset theme setting process");  }		
	}
// js to active the link of option pannel
 jQuery(document).ready(function() {
	 var rangeval='';
	 jQuery('.hc_range').click(function(){
	 var p= jQuery(this).val(); 
	 rangeval = jQuery(this).nextAll('input');
	 rangeval.val(p);
}); 


	if(getCookie('currentabChild')!=""){
		jQuery('ul.options_tabs li a#'+getCookie('currentabChild')).parent().addClass('currunt');
		jQuery('ul li.active ul').slideDown();
	}else if(getCookie('currentab')!=""){
		jQuery('ul.options_tabs li a#'+getCookie('currentab')).parent().addClass('currunt active');
		jQuery('ul.options_tabs li a#'+getCookie('currentab')).addClass('active');
		jQuery('ul.options_tabs li:first-child').removeClass('active');
	}else{
		jQuery('ul li.active ul').slideDown();
		}
	// menu click	
	jQuery('#nav > li > a').click(function(){		
		if (jQuery(this).attr('class') != 'active')
		{ 		
			jQuery('#nav li ul').slideUp(350);
			jQuery(this).next().slideToggle(350);
			jQuery('#nav li a').removeClass('active');
			jQuery(this).addClass('active');
		  
			jQuery('ul.options_tabs li').removeClass('active');
			jQuery(this).parent().addClass('active');		
			var divid =  jQuery(this).attr("id");
			document.cookie="currentabChild=;expires="+Date(jQuery.now());
			document.cookie="currentab="+divid;
			var add="div#option-"+divid;
			var strlenght = add.length;
			
			if(strlenght<17)
			{	
				var add="div#option-ui-id-"+divid;
				var ulid ="#ui-id-"+divid;
				jQuery('ul.options_tabs li li ').removeClass('currunt');
				jQuery(ulid).parent().addClass('currunt');	
			}			
			jQuery('div.ui-tabs-panel').addClass('deactive').fadeIn(1000);
			jQuery('div.ui-tabs-panel').removeClass('active');
			jQuery(add).removeClass('deactive');		
			jQuery(add).addClass('active');		
		}
	});
	
	// child submenu click
	jQuery('ul.options_tabs li li ').click(function() {			
		jQuery('ul.options_tabs li li ').removeClass('currunt');
		jQuery(this).addClass('currunt');
		var option_name =  jQuery(this).children("a").attr("id");
		document.cookie="currentab=;expires="+Date(jQuery.now());
		document.cookie="currentabChild="+option_name;
		var option_add="div#option-"+option_name;
		jQuery('div.ui-tabs-panel').addClass('deactive').fadeIn(1000);
		jQuery('div.ui-tabs-panel').removeClass('active');
		jQuery(option_add).removeClass('deactive');		
		jQuery(option_add).addClass('active');
		
	});
	if(getCookie('currentab')!=""){
			var divid = getCookie('currentab');
			var add="div#option-"+divid;
			var strlenght = add.length;
			
			if(strlenght<17)
			{	
				var add="div#option-ui-id-"+divid;
				var ulid ="#ui-id-"+divid;
				jQuery('ul.options_tabs li li ').removeClass('currunt');
				jQuery(ulid).parent().addClass('currunt');	
			}			
			jQuery('div.ui-tabs-panel').addClass('deactive').fadeIn(1000);;
			jQuery('div.ui-tabs-panel').removeClass('active');
			jQuery(add).removeClass('deactive');		
			jQuery(add).addClass('active');	
		}else if(getCookie('currentabChild')!=""){
			var option_name = getCookie('currentabChild');
			var option_add="div#option-"+option_name;
		jQuery('div.ui-tabs-panel').addClass('deactive').fadeIn(1000);;
		jQuery('div.ui-tabs-panel').removeClass('active');
		jQuery(option_add).removeClass('deactive');		
		jQuery(option_add).addClass('active');
		
		}

/* function modifyOffset() {
    var el, newPoint, newPlace, offset, siblings, k;
    width    = this.offsetWidth;
    newPoint = (this.value - this.getAttribute("min")) / (this.getAttribute("max") - this.getAttribute("min"));
    offset   = -1;
    if (newPoint < 0) { newPlace = 0;  }
    else if (newPoint > 1) { newPlace = width; }
    else { newPlace = width * newPoint + offset; offset -= newPoint;}
    siblings = this.parentNode.childNodes;
    for (var i = 0; i < siblings.length; i++) {
        sibling = siblings[i];
        if (sibling.id == this.id) { k = true; }
        if ((k == true) && (sibling.nodeName == "OUTPUT")) {
            outputTag = sibling;
        }
    }
    outputTag.style.left       = newPlace + "px";
    outputTag.style.marginLeft = offset + "%";
    outputTag.innerHTML        = this.value;
}

function modifyInputs() {
    
    var inputs = document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].getAttribute("type") == "range") {
            inputs[i].onchange = modifyOffset;

            // the following taken from http://stackoverflow.com/questions/2856513/trigger-onchange-event-manually
            if ("fireEvent" in inputs[i]) {
                inputs[i].fireEvent("onchange");
            } else {
                var evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", false, true);
                inputs[i].dispatchEvent(evt);
            }
        }
    }
}

modifyInputs();
 */
		
	/* Function to get cookie */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

	/********media-upload******/
	// media upload js
	var uploadID = ''; /*setup the var*/
	jQuery('.upload_image_button').click(function() {
		uploadID = jQuery(this).prev('input'); /*grab the specific input*/
		formfield = jQuery('.upload').attr('name');
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		
		window.send_to_editor = function(html)
		{	imgurl = jQuery(html).attr('src');
			uploadID.val(imgurl); /*assign the value to the input*/
			tb_remove();
		};		
		return false;
	});		
});

// iphone switch
jQuery(document).ready(function() {
    
    // Switch Click
		jQuery('.Switch').click(function() {
			
			// Check If Enabled (Has 'On' Class)
			if (jQuery(this).hasClass('On')){
				
				// Try To Find Checkbox Within Parent Div, And Check It
				jQuery(this).parent().find('input:checkbox').attr('checked', true);
				
				// Change Button Style - Remove On Class, Add Off Class
				jQuery(this).removeClass('On').addClass('Off');
				
			} else { // If Button Is Disabled (Has 'Off' Class)
			
				// Try To Find Checkbox Within Parent Div, And Uncheck It
				jQuery(this).parent().find('input:checkbox').attr('checked', false);
				
				// Change Button Style - Remove Off Class, Add On Class
				jQuery(this).removeClass('Off').addClass('On');
				
			}
			
		});
		
		// Loops Through Each Toggle Switch On Page
		jQuery('.Switch').each(function() {
			
			// Search of a checkbox within the parent
			if (jQuery(this).parent().find('input:checkbox').length){
				
				// This just hides all Toggle Switch Checkboxs
				// Uncomment line below to hide all checkbox's after Toggle Switch is Found
				 //$(this).parent().find('input:checkbox').hide();
				
				// For Demo, Allow showing of checkbox's with the 'show' class
				// If checkbox doesnt have the show class then hide it
				//if (!jQuery(this).parent().find('input:checkbox').hasClass("show")){ $(this).parent().find('input:checkbox').hide(); }
				// Comment / Delete out the above line when using this on a real site
				
				// Look at the checkbox's checkked state
				if (jQuery(this).parent().find('input:checkbox').is(':checked')){

					// Checkbox is not checked, Remove the 'On' Class and Add the 'Off' Class
					jQuery(this).removeClass('On').addClass('Off');
					
				} else { 
								
					// Checkbox Is Checked Remove 'Off' Class, and Add the 'On' Class
					jQuery(this).removeClass('Off').addClass('On');
					
				}
				
			}
			
		});
		
	});
	
jQuery(document).ready(function() {
	jQuery('[data-toggle="tooltip"]').tooltip(); 
	var bg= jQuery('#site_bg').val();
		jQuery('.color-code img').click(function() {
			var imgLink= jQuery(this).attr('src');
			jQuery('.color-code img').removeClass('hc_active');
			jQuery(this).addClass('hc_active');
			jQuery('#site_bg').val(imgLink);
		});
});

//calendar
 jQuery(function(){
        jQuery('#maintenance_date').calendricalDate({
			custom:1,
		});
    });