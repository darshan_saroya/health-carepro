<?php	
//get shortcodes pop-up editor == in the dashboard only, would be silly to load on the front end
if(defined('WP_ADMIN') && WP_ADMIN ) {	require_once('shortcode_popup.php'); }
/*--button--*/
function parse_shortcode_content( $content ) {

   /* Parse nested shortcodes and add formatting. */
	$content = trim( do_shortcode( shortcode_unautop( $content ) ) );

	/* Remove '' from the start of the string. */
	if ( substr( $content, 0, 4 ) == '' )
		$content = substr( $content, 4 );

	/* Remove '' from the end of the string. */
	if ( substr( $content, -3, 3 ) == '' )
		$content = substr( $content, 0, -3 );

	/* Remove any instances of ''. */
	$content = str_replace( array( '<p></p>' ), '', $content );
	$content = str_replace( array( '<p>  </p>' ), '', $content );
	return $content;
}
/*--------------------------------------*/
/*	Row
/*--------------------------------------*/

function wl_shortcode_row($params, $content = null) {
    extract(shortcode_atts(array(
        'class' => ''
    ), $params));	
    $result = '<div class="row">';
    $content = str_replace("]<br />", ']', $content);
    $content = str_replace("<br />\n[", '[', $content);
    $result .= do_shortcode($content);
    $result .= '</div>';

    return $result;
}
add_shortcode('row', 'wl_shortcode_row');
/*--------------------------------------*/
/*	Columns
/*--------------------------------------*/
function wl_column_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
	  	'offset' =>'',
      	'size' => 'col-md-6',
	  	//'position' =>'first'
      ), $atts ) );	
	$atts = shortcode_atts( array(	'offset' => '','size' => 'col-md-6'), $atts );
	$out = '<div class="'.$size.'"><p>' . do_shortcode($content). '</p></div>';
	 return $out;
}
add_shortcode('column', 'wl_column_shortcode');


function wl_button_shortcode( $atts,$content = null ){ 
   $atts = shortcode_atts(
    array(	'style' => '',
			'size' => 'small',
			'target'=> 'self',
			'url' => '#',
			'textdata' => 'Button'
		), $atts );		 
	$size = $atts['size'];
	$style = $atts['style'];
	$url = $atts['url'];
	$target = $atts['target'];	
	$target = ($target == 'blank') ? '_blank' : '';
	$style = ($style) ? ' '.$style : ''; 
	$out = '<a class="btn hc_btn ' .$style.' '. $size.'" href="' .$url. '" target="' .$target. '">' .do_shortcode($content). '</a>';
	return $out;
}

add_shortcode('button', 'wl_button_shortcode');

/*-----------Alert short code-----------*/
function wl_alert_shortcode( $atts, $content = null )
{
	$atts = shortcode_atts(  array(
							'alert_heading' => 'Please enter alert heading',     
							'alert_text' => 'Please enter text in alert text',
							'alert_style'=>'',
							
							),$atts 
						);
	$alert_heading = $atts['alert_heading'];
	$alert_text = $atts['alert_text'];
	$alert_style = $atts ['alert_style'];
	switch($alert_style){
		case "alert-success":
		$icon= 'fa-thumbs-up';
		break;
		case "alert-warning":
		$icon= 'fa-exclamation-circle';
		break;
		case "alert-danger":
		$icon= 'fa-close';
		break;
		default:
		$icon='fa-lightbulb-o';
	}
	$out='<div class="alert"><div class="alert-box" ><div class="alert '.$alert_style.'"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		   <strong><span class="fa '.$icon.' icon "></span> '.$alert_heading.'</strong>&nbsp;&nbsp;'.$alert_text. do_shortcode($content).'</div></div></div>';	
	return $out;
}
add_shortcode( 'alert', 'wl_alert_shortcode' );

/*-----------Dropcap-----------*/
function wl_dropcp_shortcode( $atts, $content = null ){
    $atts = shortcode_atts(array(      	
		'dropcp_style'=>'simple',
		'dropcp_text'=>'hasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet Vix sumo ferri an. pfs adodio fugit delenit ut qu.',
		'dropcp_first_letter' => 'P',
		'echo'=>false,
		),
      $atts );
	
	$dropcp_text = $atts['dropcp_text'];
	$dropcp_style = $atts ['dropcp_style'];
	switch($dropcp_style){
		case('suqare_color'):
		$drop='theme-color';
		break;
		case('circle_color'):
		$drop='round theme-color';
		break;
		default:
		$drop='';
	}
	$dropcp_first_letter = $atts ['dropcp_first_letter'];	
	$out='<div class="drop-caps"><p><span class="cap2 '.$drop.'">'.$dropcp_first_letter.'</span>'.$dropcp_text.'</p></div>';	
	return $out;
}
add_shortcode( 'dropcap', 'wl_dropcp_shortcode' );


/******* heading shortcode **********/
function wl_heading_function ($atts , $content = null)
{
	$atts= shortcode_atts(array (
						'heading_style' => 'h1',
						'title' => 'Heading'						
					),$atts );
	
	$heading_style = $atts['heading_style'];
	$title = $atts['title'];	
	$out ='<div class="hc_heading_section"><'.$heading_style.'>'.$title.'</'.$heading_style.'></div>';
	return $out;
}
add_shortcode('heading','wl_heading_function');

/*--------------------------------------*/
/*	Accordian
/*--------------------------------------*/
function wl_accordion_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(  array(
							'fields'=>'1',
							'accordian_title' => 'Accordions title',
							'accordian_text'=>'Accordions Description',
							'accordian_style'=>'type1',
							'echo'=>false,							
							),$atts 
						);
	$fields = $atts['fields'];
	$accordian_title = $atts['accordian_title'];
	$accordian_style = $atts['accordian_style'];
	$title = explode('~', $accordian_title);
	$accordian_text = $atts ['accordian_text'];
	$text = explode('~', $accordian_text);
	$rand = rand(1, 100);
	
	$out ='';
	$out .='<div class="col-md-12 accordion"><div class="row accordion-details"><div class="'.$accordian_style.'" id="accordion'.$rand.'">';
	for($i=1; $i<=$fields; $i++)
	{	//$title[$i] = preg_replace("/__/", ',', $title[$i]);
		//$text[$i] = preg_replace("/__/", ',', $text[$i]);
		$changetitle = preg_replace('~[^A-Za-z\d\s-]+~u', 'wr', $title[$i]);
		$changetitle = str_replace(' ', 'tt', $changetitle);
		 $out .='<button type="button" class="btn btn-lg collapsed" data-toggle="collapse" data-target="#'.$changetitle.'"><h3>'.$title[$i].'</h3></button>
				<div id="'.$changetitle.'" class="collapse">
				<p>'.$text[$i].'</p>
				</div>';
	} 
	$out .='</div></div></div>';
	return $out;
}
add_shortcode('accordian', 'wl_accordion_shortcode');


/*-----------Tabs short code-----------*/
if (!function_exists('wl_tabgroup')) {
	function wl_tabgroup( $atts, $content = null ) 
	{	$atts = shortcode_atts(array('tabs_title' => 'This is tabs heading','echo'=>false), $atts );	
		$tabs_title1 = $atts['tabs_title'];
		// Extract the tab titles for use in the tab shortcode
		preg_match_all( '/tabs_title="([^\"]+)"/i', $content, $matches, PREG_OFFSET_CAPTURE );
		$tab_titles = array(); $wl_tabs_title =array();
		if( isset($matches[1]) ){ $tab_titles = $matches[1]; }			
		$output = '<div class="tabs"><div class="type">';		
		if( count($tab_titles) )
		{	$output .= '<ul class="nav nav-tabs" id="myTab-'.preg_replace('~[^A-Za-z\d\s-]+~u', 'wr', $tabs_title1).'">';
			$count=0;			
			foreach( $tab_titles as $tabs_title )
			{	if($count==0){
				$wl_tabs_title[0] = str_replace(' ', 'wem', $tabs_title[0]);
				  $output .= '<li class="active" ><a data-toggle="tab" href="#'.preg_replace('~[^A-Za-z\d\s-]+~u', 'wr', $wl_tabs_title[0]).'">'.$tabs_title[0].'</a></li>';
				 } else {
				  $wl_tabs_title[0] = str_replace(' ', 'wem', $tabs_title[0]);	
				  $output .= '<li class="" ><a data-toggle="tab" href="#'.preg_replace('~[^A-Za-z\d\s-]+~u', 'wr', $wl_tabs_title[0]).'">'.$tabs_title[0].'</a></li>';
				 } 
				  $count++;
			}		    
			$output .= '</ul><div id="myTabContent" class="tab-content">';
			$output .= do_shortcode( $content );			
		} 		 
		 $output .= '</div></div></div>';
		return $output;	
	}
	add_shortcode( 'tabgroup', 'wl_tabgroup' );
}
function wl_tabs_shortcode($atts, $content = null ){
	
	$atts = shortcode_atts(array('tabs_title' => 'This is tabs heading','tabs_text' => 'Description','wrap'=>'yes','echo'=>false), $atts );	
	$tabs_title = $atts['tabs_title'];
	$tabs_text = $atts['tabs_text'];
	$wrap = $atts['wrap'];
	$wl_tabs_title = str_replace(' ', 'wem', $tabs_title);
	static $c=0;  
	$out ='';
    if($c==0 || $wrap=="yes")
	{
		$out .='<div id="'.preg_replace('~[^A-Za-z\d\s-]+~u', 'wr', $wl_tabs_title).'" class="tab-pane fade active in">';
	}
	else{
		$out .='<div id="'.preg_replace('~[^A-Za-z\d\s-]+~u', 'wr', $wl_tabs_title).'" class="tab-pane fade">';
	}
	$c++;
	$out .='<p class="enigma_short_tabs_content">'.$tabs_text.'</p>'.do_shortcode( $content ).'</div>';			  
	return $out;	
}
add_shortcode( 'tabs', 'wl_tabs_shortcode' );
/*-----------Alert short code-----------*/

/*-----------panel short code-----------*/
function wl_panle_shortcode( $atts, $content = null )
{
	$atts = shortcode_atts(  array(
							'panel_heading' => 'Please enter panel heading',     
							'panel_text' => 'Please enter text in panel text',
							'panel_color'=>'panel-default',
							
							),$atts 
						);
	$panel_heading = $atts['panel_heading'];
	$panel_text = $atts['panel_text'];
	$panel_color = $atts ['panel_color'];
	
	$out='<div class="panel '.$panel_color.'">
			<div class="panel-heading">'.$panel_heading.'</div>
			<div class="panel-body">'.$panel_text.' </div>
		</div>';	
	return $out;
}
add_shortcode('panel', 'wl_panle_shortcode' );

/*-----------PROGRESS BAR code-----------*/
function wl_progressbar_shortcode( $atts, $content = null )
{
	$atts = shortcode_atts(  array(
							'progress_heading' => 'Please enter progress heading',     
							'progress_percentag' => '85',
							'progress_style' => 'info',
							),$atts 
						);
	$progress_heading = $atts['progress_heading'];
	$progress_percentag = $atts['progress_percentag'];
	$progress_style = $atts['progress_style'];
	$out='<div class="process">	
	<div class="progress-bar-'.$progress_style.'">
					<span class="alignleft">'.$progress_heading .'</span>
					<span class="alignright">'. $progress_percentag.'%</span>
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="40"
					  aria-valuemin="0" aria-valuemax="100" style="width:'.$progress_percentag.'%">
					</div>
				</div>
			</div>
			</div>';	
	return $out;
}
add_shortcode('progressbar', 'wl_progressbar_shortcode' );

/*----------- tooltip code-----------*/
function wl_tooltip_shortcode( $atts, $content = null )
{
	$atts = shortcode_atts( array(	
	'tooltip_text' => 'tool tip Text',     
	'tooltip_over_text' => 'tool tip title',
	'tooltip_style' => 'top',
	), $atts );
	$tooltip_text = $atts['tooltip_text'];
	$tooltip_over_text = $atts['tooltip_over_text'];
	$tooltip_style = $atts['tooltip_style'];

	$out='<a href="#" data-original-title="'.$tooltip_over_text .'" title="" data-placement="'.$tooltip_style .'" data-toggle="tooltip" class="hc_btn">'.$tooltip_text.'</a>';
	return $out;
}
add_shortcode('tooltip', 'wl_tooltip_shortcode' );

/*----------- POPOVERS  shortcode code-----------*/
function wl_popover_shortcode( $atts, $content = null )
{
	$atts = shortcode_atts(  array(
							'popover_text' => 'Popover on top',     
							'popover_over_text' => 'Popover on top',
							'popover_style' => 'top',
							),$atts 
						);
	$popover_text = $atts['popover_text'];
	$popover_over_text = $atts['popover_over_text'];
	$popover_style = $atts['popover_style'];
	
	$out='<button class="hc_btn" data-toggle="popover" data-placement="'.$popover_style .'" title="" data-content="'.$popover_over_text .'" >'.$popover_text .'</button>';	
	return $out;
}
add_shortcode('popover', 'wl_popover_shortcode' );

function wl_portfolio_shortcode($atts, $content = null ){ ob_start();	
	$atts = shortcode_atts(
	array('portfolio_type' =>3,
		'port_cat_name' =>'all',
		'port_cat_show'=>'yes'), $atts );	
	$portfolio_type = $atts['portfolio_type'];
	$port_cat_name = $atts['port_cat_name'];
	$port_cat_name1 =$port_cat_name;
	$port_cat_show = $atts['port_cat_show'];
	$port_cat_name = explode(",", $port_cat_name);
	 ?>
	<div class="container-fluid portfolio"> 
	<?php if($port_cat_show=='yes') {
	?>
		
		<div class="button-group filter-button-group">
			<?php foreach ($port_cat_name  as $tax_term) {
				$tax_term_name = str_replace(' ', '_', $tax_term);
				$tax_term_name = preg_replace('~[^A-Za-z\d\s-]+~u', 'wl', $tax_term_name);
				?>
				<button class="button" data-filter= ".<?php echo $tax_term_name; ?>"  ><?php echo $tax_term; ?></button>
			<?php } ?>
		</div>
		<?php } ?>
		<div class="grid gallery">	
	<?php	$all_posts = wp_count_posts( 'hc_portfolios')->publish;
				$args = array('post_type' => 'hc_portfolios', 'posts_per_page' =>$all_posts,'hc_portfolio_categories'=>$port_cat_name1);
					global $portfolio;
					$portfolio = new WP_Query( $args );			
					if( $portfolio->have_posts() )
					{	
						while ( $portfolio->have_posts() ) : $portfolio->the_post();							
						$terms_names = wp_get_post_terms(get_the_ID(), 'hc_portfolio_categories', array("fields" => "names"));
					 if(has_post_thumbnail()):
					?>			
			<div class="element-item col-md-<?php if($portfolio_type==2) { echo "6"; } if($portfolio_type==3) { echo "4"; } if($portfolio_type==4) { echo "3"; }  ?> col-sm-6 wl-gallery icons <?php foreach ($terms_names as $term) {   $tax_term = str_replace(' ', '_', $term);  echo preg_replace('~[^A-Za-z\d\s-]+~u', 'wl', $tax_term). " "; } ?>" data-category="transition" >
				<?php $defalt_arg =array('class'=>"img-responsive home_porfolio_thumb");
					the_post_thumbnail('home_porfolio_thumb',$defalt_arg); ?>
					<div class="overlay">
						<h4><?php the_title(); ?></h4>
							<span class="<?php echo sanitize_text_field( get_post_meta( get_the_ID(), 'portfolio_icon', true )); ?> icon"></span>
							<div class="ln"></div>
						<h5><?php 	$portfolio_categories_list = get_the_terms( get_the_ID(), 'hc_portfolio_categories' );
								if($portfolio_categories_list){
								$totalcat= count($portfolio_categories_list);
								$i=1;
									foreach($portfolio_categories_list as $list)
									{	echo $list->name;
										if($i <$totalcat){ echo ", ";}
										$i++;
									}  
								} ?></h5>
						<a class="home-port" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"><span class="fa fa-search"></span></a>
						<a href="<?php the_permalink(); ?>"><span class="fa fa-chain"></span></a>
					</div>
			</div>
			<?php endif;
				endwhile;
					}?>
		</div>
	</div>
	<?php return ob_get_clean();
}
add_shortcode( 'portfolio', 'wl_portfolio_shortcode' );
/* Blog */
function wl_blog_shortcode($atts, $content = null ){ ob_start();	
	$atts = shortcode_atts(
	array('blog_cat_name' =>'all',
	'blog_type'=>3), $atts );
	$blog_type = $atts['blog_type'];
	//$blog_cat_show = $atts['blog_cat_show'];
	$blog_cat_name = $atts['blog_cat_name'];	
	$blog_cat_name1 =$blog_cat_name;
	$blog_cat_name = explode(",", $blog_cat_name); ?>
	<div class="container-fluid">
	<div class="masanary">
			<div id="wrapper">
				<div class="container2">
					<div class="row">
					<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$args = array( 'post_type' => 'post', 'post_status'=>'publish', 'paged' => $paged, 'category_name' =>$blog_cat_name1); 
					$blog_post = new WP_Query( $args );
					if($blog_post->have_posts()): ?>					
						<div class="col-md-12 gallery">
						<div class="gallery1 no-pad">
						<?php while($blog_post->have_posts()):
								$blog_post->the_post();	?>
												
								<div class="col-md-<?php if($blog_type==2) { echo "6"; } if($blog_type==3) { echo "4"; } if($blog_type==4) { echo "3"; }  ?> col-sm-6 wl-gallery" >
									<div class="row b-link-fade b-animate-go masonry-content">
										<div class="col-xs-12 no-pad border">
											<?php if(has_post_thumbnail()): ?>
											<div class="masanary-pic">
												<div class="img-thumbnail">
													<?php $defalt_arg =array('class'=>"img-responsive mesonry_post_thumb");
					the_post_thumbnail('mesonry_post_thumb', $defalt_arg);
					?>
													<div class="overlay">
														<a class="home-blog" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"><span class="fa fa-search"></span></a>
														<a href="<?php the_permalink(); ?>"><span class="fa fa-chain"></span></a>
													</div>
												</div>
											</div>
										<?php endif; ?>
										</div>
											<div class="col-xs-12 no-pad">
												<div class="line"></div>
												<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
												<div class="date">
													<span class="fa fa-clock-o"> <?php echo get_the_date(); ?> </span> | <span class="fa fa-comment-o"> <?php echo comments_number(__('No Comments','weblizar'), __('1 Comment','weblizar'), '% Comments'); ?></span>
												</div>
												<p><?php the_excerpt(); ?> </p>
												<a class="btn bg" href="<?php the_permalink(); ?>"> <?php _e('READ MORE','weblizar'); ?> </a>
											</div>
									</div>
								</div>
							
						<?php endwhile; ?>
						</div>
						<div class="col-md-12 center-pagi">
						<?php if (function_exists("template_pagination")) {
							template_pagination($blog_post->max_num_pages); } ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
	</div>
</div>
	<?php 
	return ob_get_clean();
}
add_shortcode( 'blog_post', 'wl_blog_shortcode' );
?>