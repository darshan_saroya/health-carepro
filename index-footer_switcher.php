<!-- remprod -->

<script type="text/x-handlebars-template" id="css-skin">
    .navigation > li:hover > a,.navigation > li > a:hover,
    .navigation > li > .activelink,.navigation > li:hover > a > i, .navigation > li > a:hover >
    span.label-nav-sub::before, .navigation > li > a:focus > span.label-nav-sub::before{
    color:  {{skin_color}};
    }
    .mobile-menu-button, .mobile-nav, .slider-fixed-container{
    background-color:  {{skin_color}};
    }

    .navigation > li > a:after, .navigation>li>.activelink:after{
    background-color:  {{skin_color}};
    }
    .navigation>li:hover > a > span.label-nav-sub::before,.navigation>li:hover> a > span.label-nav-sub::before{
    color:  {{skin_color}};
    }


    /* Page Content Colors */
    .body-wrapper a:hover , .tab a:hover, accordion .title:hover, .top-body a:hover, .bottom-body
    a:hover,.content-box.content-style2 h4 i
    ,.accordion .active h4, .accordion .title:hover h4, .side-navigation .menu-item.current-menu-item a,
    .side-navigation .menu-item:hover a:after,
    .side-navigation .menu-item:hover a, a.tool-tip, .team-member .team-member-position, .team-member-progress
    .team-member-position, .item-img-overlay i
    ,ul.icon-content-list-container li.icon-content-single .icon-box i,.item-img-overlay .portfolio-zoom:hover,
    .navigation ul li:hover>a, .blog_post_quote:after,
    .item-img-overlay .portfolio-zoom, body .white-text .feature-details a:hover{
    color:  {{skin_color}};
    }
    ::selection
    {
    background-color: {{skin_color}};
    }
    ::-moz-selection
    {
    background-color: {{skin_color}};
    }
    .item-img-overlay .portfolio-zoom:hover, .tab a.active {
    color:  {{skin_color}} !important;
    }
    .callout-box{
    border-left-color:  {{skin_color}}
    }
    .feature .feature-content,.team-member .team-member-content{
    border-top-color:  {{skin_color}};
    }

    .team-member-progress-bar{
    background-color:  {{skin_color}};
    }
    .blog-masonry .blog_post_quote{
    border-top: 2px solid  {{skin_color}};
    }
    .tab a.active:after {
    background-color: {{skin_color}};
    border-color:  {{skin_color}};
    }
    .item-img-overlay{
    background-color:  {{skin_color_rgba}};
    }
    .item-img-overlay i:hover{
    background-color:  {{skin_color_rgba}};
    }
    body .section-content.section-image{
    background-color:  {{skin_color_rgba}};
    }



    .button, .body-wrapper input[type="submit"], .body-wrapper input[type="button"], .section-content.section-color-bg,.content-box.content-style4 h4 i
    ,button.button-main,.body-wrapper .tags a:hover,.callout-box.callout-box2, .blog-search .blog-search-button, .top-title-wrapper, .testimonial-big
    ,.content-style3:hover .content-style3-icon, .content-box.style5 h4 i, table.table thead tr, .price-table .price-label-badge, .price-table .price-table-header, .section-subscribe .subscribe-button.icon-envelope-alt{
    background-color: {{skin_color}};
    }


    .blog-post-icon,.comments-list .children .comment:before,.portfolio-filter li a.portfolio-selected,
    .portfolio-filter li a:hover, .dropcaps.dropcaps-color-style, .carousel-container .carousel-icon:hover{
    background-color:  {{skin_color}};
    }
    .comments-list .children .comment:after{
    border-color: transparent transparent transparent  {{skin_color}};;
    }

    .highlighted-text{
    background-color:  {{skin_color}};
    color: #ffffff;
    }
    .icons-list.colored-list li:before, .blog-post-date .day, .blog-post-date .month, strong.colored, span.colored
    ,.content-style3 .content-style3-icon{
    color:  {{skin_color}};
    }


    .pagination .prev, .pagination .next, .pagination a:hover, .pagination a.current, .price-table .price-label{
    color: {{skin_color}};
    }


    /* Footer Area Colors */

    .footer .copyright{
    border-color:  {{skin_color}};
    }

    .footer .copyright a:hover{
    color:  {{skin_color}};
    }
    .flickr_badge_wrapper .flickr_badge_image img:hover{
    border-color:  {{skin_color}};
    }
</script>

<script type="text/javascript"> var $default_skin = "light-blue";</script>
<div id="skin-chooser-container" class="skin-chooser-container hide-skin-chooser">
    <!--<a href="" target="_blank" class="skin-save" id="skin-save"><i class="icon-arrow-down"></i> DOWNLOAD NEW SKIN </a>-->
    <div class="skin-chooser">
        <div class="skin-chooser-label">
            Layout mode
        </div>
        <div class="skin-chooser-row skin-chooser-row-open">
            <select id="layout-mode">
                <option value="">Wide</option>
                <option value="boxed">Boxed</option>
            </select>
        </div>
		<div class="skin-chooser-label">
            Header mode
        </div>
        <div class="skin-chooser-row skin-chooser-row-open">
            <select id="header-mode">
                <option value="static">Static</option>
                <option value="fixed">Fixed</option>
            </select>
        </div>
        <div class="skin-chooser-label">
            Custom Theme Skin
        </div>
        <div class="skin-chooser-row skin-chooser-row-open">
			<div id="picker" data-skinname=""></div>
			<button class="btn" id="color" name="color" value="#123456" style="float:right; border-radius:6px;">Apply</button>
        </div>        
        <div class="skin-chooser-label">
            Patterns Background
        </div>
        <div class="skin-chooser-row skin-chooser-row-open">
			<div class="pattern pattern-black-twill" data-body-class="bgpattern-black-twill"></div>
			<div class="pattern pattern-dark-fish-skin" data-body-class="bgpattern-dark-fish-skin"></div>
			<div class="pattern pattern-escheresque-ste" data-body-class="bgpattern-escheresque-ste"></div>
			<div class="pattern pattern-grey" data-body-class="bgpattern-grey"></div>
			<div class="pattern pattern-knitting250px" data-body-class="bgpattern-knitting250px"></div>
			<div class="pattern pattern-p4" data-body-class="bgpattern-p4"></div>
			<div class="pattern pattern-p5" data-body-class="bgpattern-p5"></div>
			<div class="pattern pattern-p6" data-body-class="bgpattern-p6"></div>
			<div class="pattern pattern-ps-neutral" data-body-class="bgpattern-ps-neutral"></div>
			<div class="pattern pattern-pw-maze-white" data-body-class="bgpattern-pw-maze-white"></div>
			<div class="pattern pattern-pw-pattern" data-body-class="bgpattern-pw-pattern"></div>
			<div class="pattern pattern-retina-wood" data-body-class="bgpattern-retina-wood"></div>
			<div class="pattern pattern-shattered" data-body-class="bgpattern-shattered"></div>
			<div class="pattern pattern-subtle-dots" data-body-class="bgpattern-subtle-dots"></div>
			<div class="pattern pattern-subtle-surface" data-body-class="bgpattern-subtle-surface"></div>
			<div class="pattern pattern-whitediamond" data-body-class="bgpattern-whitediamond"></div>
        </div>
    </div>
    
	<a href="#" class="arrow-left fa fa-cogs" id="show_hide_skin_chooser"></a>
	<!-- end remprod -->
	
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/_jquery.placeholder.js"></script>
	<!-- remprod -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/_colorpicker.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/_handlebars.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/_skins.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/_skinchooser.js"></script>
<script>
 jQuery(document).ready(function ($) {
	 
	 jQuery("#header-mode").change(function(e){
			if( jQuery(this).val() == 'static'){
				jQuery(".navbar").removeClass("menu-layout sticky-head");
			} 
			if( jQuery(this).val() == 'fixed'){
				jQuery(".navbar").addClass("menu-layout sticky-head");
			}
		});
		
	 jQuery('#picker').farbtastic('#color');
	 jQuery('#color').click(function(){
	var custom = jQuery(this).val();
	 var color_css = '<style>.dropdown-menu .open>a,.dropdown-menu .open>a:focus,.dropdown-menu .open>a:hover,.dropdown-menu>.active>a,.dropdown-menu>.active>a:focus,.dropdown-menu>.active>a:hover,.dropdown-menu>li>a:hover{background-color:'+custom+'!important;}.breadcrumb li a:hover,.navbar-brand:hover,.navbar-collapse .navbar-nav li a:focus::before,.navbar-collapse .navbar-nav li a:hover::before,.navbar-collapse .navbar-nav>.active>a:focus::before,.navbar-collapse .navbar-nav>.active>a:hover::before{color:'+custom+';}.nav .open>a,.nav .open>a:focus,.nav .open>a:hover,.navbar-collapse .navbar-nav>.active>a,.navbar-collapse .navbar-nav>.active>a:focus,.navbar-collapse .navbar-nav>.active>a:hover,.navbar-collapse .navbar-nav>li>a:focus,.navbar-collapse .navbar-nav>li>a:hover,.top a:hover{color:'+custom+'!important;}.carousel-caption .btn,.carousel-control .glyphicon-chevron-left,.carousel-control .glyphicon-chevron-right,.carousel-control .icon-next,.carousel-control .icon-prev,.carousel-indicators .active,.health-dept .btn,.health-dept .btn:hover,.testimony,.text .link{background-color:'+custom+';}.slider-caption{border-color:'+custom+'!important;}.carousel-indicators li{border:1px solid '+custom+';}.comment .col-xs-2 img{border:5px solid '+custom+';}.doctors .img-thumbnail .overlay button{color:'+custom+';}.swiper-pagination-bullet-active{background-color:'+custom+'!important;border:1px solid '+custom+'!important;}.appoinment,.department .tab-content .btn,.emergency .icon,.feedback form .btn,.hc_tags a:hover,.news .btn,.news .btn:hover,.news .owl-theme .owl-controls .owl-buttons div,.news .owl-theme .owl-controls .owl-page.active span,.news .owl-theme .owl-controls.clickable .owl-page:hover span{background-color:'+custom+';}.doctors .img-thumbnail .overlay,.gallerys .overlay,.grid-item .overlay,.masanary .masanary-pic .img-thumbnail .overlay,.news .overlay,.portfolio .element-item .overlay,.portfolio-details .related-post .overlay,.rightside .blog-desc .img-thumbnail .overlay,.rightside .posts .pics .img-thumbnail .overlay,.rightside .right-widget .img-thumbnail .overlay,.sidebar-widget .col-xs-3 .overlay,.sidebar-widget .col-xs-4 .overlay,.sidebar-widget .pics .overlay,.widget-footer .col-xs-4 .overlay,.widget-footer .overlay{background:'+custom+';}.doctors .img-thumbnail .overlay a:hover,.gallerys .overlay a:hover,.masanary .masanary-pic .img-thumbnail .overlay a:hover,.news .overlay a:hover,.portfolio .element-item .overlay a:hover,.rightside .blog-desc .img-thumbnail .overlay a:hover,.rightside .posts .pics .img-thumbnail .overlay a:hover,.rightside .right-widget .img-thumbnail .overlay a:hover,.sidebar-widget .pics .overlay a:hover{color:'+custom+'!important;}.space .pagination>.active>a,.space .pagination>.active>a:focus,.space .pagination>.active>a:hover,.space .pagination>.active>span,.space .pagination>.active>span:focus,.space .pagination>.active>span:hover{background-color:'+custom+';border-color:'+custom+';}.blogs .rightside .author .col-xs-2{border:5px solid '+custom+';}.avatar,.blogs .rightside .line{border:2px solid '+custom+';}.comment a{color:'+custom+';}.doctors .button{border:1px solid '+custom+';}.doctors .img-thumbnail .overlay .btn{color:'+custom+'!important;}.color,.department .tab-content .col-md-4 .icon,.news .category a:hover,.news .date a:hover,.service .icon,.services .heart,.services .ln2,.services .ln3{color:'+custom+';}.department .tab-content .col-md-6 .line{border:2px solid '+custom+';}.btn,.hc_tags a{border:1px solid '+custom+'!important;}.pics .img-thumbnail{border-color:'+custom+';}.gallerys .overlay a:hover,.news .overlay a:hover{color:'+custom+'!important;border:2px solid '+custom+';}.sidebar-widget .tagcloud a,.sidebar-widget .tagcloud a:hover,.tagcloud a:hover{border:1px solid '+custom+';}.footer .widget-footer a:hover{color:'+custom+';}.tagcloud a:hover{background-color:'+custom+'!important;}#wp-calendar caption,.sidebar .recent .nav li a{background-color:'+custom+';}.footer-bottom a:hover{color:'+custom+';}.sidebar-widget .line{border:2px solid '+custom+';}.sidebar-widget .nav a:focus,.sidebar-widget .nav a:hover{color:'+custom+';}.sidebar-widget .img-thumbnail .overlay a:hover{color:'+custom+';border:2px solid '+custom+';}.rightside a:hover{color:'+custom+';}.rightside .blog-desc .img-thumbnail .overlay a:hover{background-color:#eee;color:'+custom+';border:2px solid '+custom+';}.bg,.bg:hover,.blue,.doctors .button-group .button.is-checked,.doctors .button:hover,.doctors .img-thumbnail .overlay .btn:hover,.portfolio .button-group .button.is-checked,.portfolio .button:hover,.services .service a:hover .icon,.services .service4 a:hover .icon,.space .pagination a:hover{background-color:'+custom+';}.sidebar .recent .nav-tabs>li a:hover,.sidebar .recent .nav-tabs>li.active>a,.sidebar .recent .nav-tabs>li.active>a:focus,.sidebar .recent .nav-tabs>li.active>a:hover{color:'+custom+';}.rightside .right-widget .img-thumbnail .overlay a:hover{color:'+custom+';border:2px solid '+custom+';}.masanary a:hover,.service-icon a,.services .line,.services .service a{color:'+custom+';}.services .service a .icon{border:2px solid '+custom+';}.service4 .col-xs-3 .icon,.services .service4 a{color:'+custom+';}.services .service4 a .icon{border:2px solid '+custom+';}.portfolio .button{border:1px solid '+custom+';}.about-doctors .about-doc-details .img-thumbnail,.about_doctors .docs,.about_doctors .docs .name,.contact .line{border:2px solid '+custom+';}.portfolio .element-item .overlay a:hover{color:'+custom+'!important;border:2px solid '+custom+';}.doctors .img-thumbnail .overlay a:hover{color:'+custom+'!important;}.contact .btn{background-color:'+custom+'!important;}.about_doctors .docs .btn{background-color:'+custom+';}.about_doctors .docs .name{color:'+custom+';}.about_doctors .docs:hover .btn{color:'+custom+'!important;}.about_doctors .docs:hover .caption,.about_doctors .docs:hover .name,.about_doctors .docs:hover a,.about_doctors .docs:hover p{background-color:'+custom+'!important;}.about-department .btn .icon,.about-doctors .about-doc-details,.events .up-event .btn,.events .up-event .dates{background-color:'+custom+';}.about-department .btn:after,.accordion-details .collapse.in>.btn-lg{color:'+custom+';}.events .up-event .health-event{border-bottom:2px solid '+custom+';}.about-appoinment .mobile .btn{color:'+custom+';}.about-desc blockquote{border-left:2px solid '+custom+';border-right:2px solid '+custom+';}.about-desc .btn{color:'+custom+';}.about-doctors .button-group .button.button.is-checked{color:'+custom+';}.about-doctors .button-group .button.button.is-checked .icon{background-color:#fff;color:'+custom+';}.about-doctors .about-doc-details:hover,.about-doctors .about-doc-details:hover .img-thumbnail,.about-doctors .button-group .button .icon,.client,.tabs .nav>li a{background-color:'+custom+';}.about-doctors .about-doc-details .btn{color:'+custom+';border:1px solid '+custom+';}.about-doctors .about-doc-details:hover .btn{color:'+custom+';}.about-doctors .about-doc-details:hover .view{color:'+custom+';}.abt-appoinment .feed .btn{color:'+custom+';}.line{border:2px solid '+custom+';}.doctors-pic .btn,.doctors-pic .icon:hover,.tabs .tab-content ul li::before{color:'+custom+';}.tabs .nav-tabs>li.active>a,.tabs .nav-tabs>li.active>a:focus,.tabs .nav-tabs>li.active>a:hover{background-color:transparent;color:'+custom+';border:2px solid '+custom+';}.abt-appoinment,.accordion .accordion-details .type3 .btn-lg,.accordion .accordion-details .type4 .btn-lg,.error .btn,.portfolio-details .pager li>a:focus,.portfolio-details .pager li>a:hover,.portfolio-details .related-post .owl-theme .owl-controls .owl-buttons div,.tabs .nav>li a:hover{background-color:'+custom+';}.about-department .btn::after{color:'+custom+';}.exp1 blockquote{border-left:5px solid '+custom+';}.portfolio-pic .detail .icon{color:'+custom+';}.portfolio-pic .btn{color:'+custom+'!important;border:1px solid '+custom+';}.portfolio-details .pager li>a,.portfolio-details .related-post .overlay a:hover{color:'+custom+';border:1px solid '+custom+';}.portfolio-pic .btn:hover{background-color:'+custom+';border:1px solid '+custom+';}.hc_tags a{border:1px solid '+custom+';}.sidebar-widget a:hover{color:'+custom+'!important;}.pagination>.active>a,.pagination>.active>a:focus,.pagination>.active>a:hover,.pagination>.active>span,.pagination>.active>span:focus,.pagination>.active>span:hover{border-color:'+custom+';}.woocommerce span.onsale{background-color:'+custom+'!important;}.woocommerce #respond input#submit,.woocommerce a.button,.woocommerce button.button,.woocommerce input.button{background-color:'+custom+'!important;border:1px solid '+custom+'!important;}.swiper-button-next,.swiper-button-prev,.theme-color{background-color:'+custom+';}.woocommerce #respond input#submit:hover,.woocommerce a.button:hover,.woocommerce button.button:hover,.woocommerce input.button:hover{color:'+custom+'!important;border:1px solid '+custom+'!important;}.stars a,.stars a.active,.woocommerce .star-rating{color:'+custom+';}.pic .pics .overlay a:hover,.woocommerce .woocommerce-info::before,.woocommerce .woocommerce-message::before{color:'+custom+'!important;}.woocommerce .woocommerce-info,.woocommerce .woocommerce-message{border-top-color:'+custom+'!important;}.form-control:focus{box-shadow:0 0 2px 1px '+custom+'!important;}.home-docs .element-item h3 a{color:'+custom+';}</style>';
	 jQuery('#hc_color').html(color_css);
 });	 
}); 
</script>
<div id="hc_color"></div>