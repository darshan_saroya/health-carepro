<!--  News Start -->
<?php $health_data= health_care_get_options(); ?>
<span class="anchor" id="blog-section"></span>
<div class="container-fluid space">
	<div class="container news">
		<?php if($health_data['blog_heading']!=''){ ?>
				<h1 class="color"><?php echo $health_data['blog_heading']; ?></h1>
		<?php if($health_data['blog_icon']!=''){ ?>
			<div class="ln2 color"></div>
			<span class="<?php echo $health_data['blog_icon']; ?> color heart"></span>
			<div class="ln3 color"></div>
		<?php } ?>
<?php } ?>
<?php if($health_data['blog_desc']!=''){ ?>
		<p class="desc"><?php echo $health_data['blog_desc']; ?></p>
<?php } ?>
			<div class="span12">
			 <div class="hc_homeblog">
        <div id="home-blog" class="swiper-wrapper gallery">
		<?php
	$args = array( 'post_type' => 'post', 'post_status'=>'publish', 'posts_per_page' => $health_data['blog_slide_item'],'post__not_in' => get_option( 'sticky_posts' ));
		$blog_post = new WP_Query( $args );
		if($blog_post->have_posts()):
		while($blog_post->have_posts()):
		$blog_post->the_post();		?>
			<div class="swiper-slide">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 border no-pad">
					<div class="col-md-6  no-pad">
             <?php if(has_post_thumbnail()): ?>
           <div class="img-thumbnail">
					<?php $defalt_arg =array('class'=>"img-responsive home_post_thumb");
					the_post_thumbnail('home_post_thumb', $defalt_arg);
					?>
						<div class="overlay">
								<a class="home-blog" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>"><span class="fa fa-search"></span></a>
								<a href="<?php the_permalink(); ?>"><span class="fa fa-chain"></span></a>
						</div>
            </div>
            <?php endif; ?>
					</div>
					<div class="col-md-6">
						<div class="line"></div>
							<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
							<span class="fa fa-clock-o"> <?php the_date(); ?> </span> | <a href="<?php the_permalink(); ?>"><span class="fa fa-comment-o"> <?php echo comments_number(__('No Comments','weblizar'), __('1 Comment','weblizar'), '% Comments'); ?> </span></a>
							<?php if(get_the_category_list() != '') { ?>
							<div class="category">
								<span><?php _e('Categories : ','weblizar'); ?></span><?php the_category(); ?>
							</div>
							<?php } ?>
							<p><?php the_excerpt(); ?><p>
							<a class="btn" href="<?php the_permalink(); ?>"> <?php _e('Read More','weblizar'); ?></a>
					    </div>
					</div>
					<div class="col-md-1"></div>
				</div> 
			</div> 
				<?php endwhile; 
			 endif;?>
			</div>
			 <div class="swiper-button-next swiper-button-white"></div>
			<div class="swiper-button-prev swiper-button-white"></div>
			</div>
			</div>
			</div>
			</div>