<?php
if(is_page_template('blog-masonry-2c.php') || is_page_template('blog-masonry-left.php') || is_page_template('blog-masonry-right.php')) {
				$i='6';
				} elseif (is_page_template('blog-masonry-3c.php')) {
				$i='4';
				} elseif (is_page_template('blog-masonry-4c.php')) { 
				$i='3';
				}
 ?>						
								<div class="col-md-<?php echo $i; ?> col-sm-6 wl-gallery" >
									<div class="row b-link-fade b-animate-go masonry-content">
										<div class="col-xs-12 no-pad border">
											<?php if(has_post_thumbnail()): ?>
											<div class="masanary-pic">
												<div class="img-thumbnail">
													<?php $defalt_arg =array('class'=>"img-responsive mesonry_post_thumb");
					the_post_thumbnail('mesonry_post_thumb', $defalt_arg);
					?>
													<div class="overlay">
														<a class="home-blog" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"><span class="fa fa-search"></span></a>
														<a href="<?php the_permalink(); ?>"><span class="fa fa-chain"></span></a>
													</div>
												</div>
											</div>
										<?php endif; ?>
										</div>
											<div class="col-xs-12 no-pad">
												<div class="line"></div>
												<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
												<div class="date">
													<span class="fa fa-clock-o"> <?php echo get_the_date(); ?> </span> | <span class="fa fa-comment-o"> <?php echo comments_number(__('No Comments','weblizar'), __('1 Comment','weblizar'), '% Comments'); ?></span>
												</div>
												<p><?php the_excerpt(); ?> </p>
												<a class="btn bg" href="<?php the_permalink(); ?>"> <?php _e('READ MORE','weblizar'); ?> </a>
											</div>
									</div>
								</div>
							