<!-- Testimonail Start -->
<?php $health_data= health_care_get_options(); ?>
<span class="anchor" id="testimonial-section"></span>
<div class="testimonail">
<div class="container-fluid space testimonail-cover">
	<div class="container">
	<?php if($health_data['testi_heading']!=""){ ?>
		<h1 class="white"><?php echo $health_data['testi_heading']; ?></h1>
		<?php if($health_data['testi_icon']!=''){ ?>
			<div class="ln2 white"></div>
			<span class="<?php echo $health_data['testi_icon']; ?> white heart"></span>
			<div class="ln3 white"></div>
		<?php } ?>
	<?php } ?>
		<div class="container">
			<div class="home_testi">
				<div class="swiper-wrapper ">
					<?php $all_posts = $health_data['testi_num'];
					$args = array( 'post_type' => 'hc_testimonials','posts_per_page' =>$all_posts);        
					$hc_testimonials = new WP_Query( $args );
					if( $hc_testimonials->have_posts() ){ ?>
						<?php while ( $hc_testimonials->have_posts() ) : $hc_testimonials->the_post();if(has_post_thumbnail()){	?>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-xs-4 pics">
										<div class="img-thumbnail ">
										<?php $defalt_arg =array('class'=>"img-responsive img-circle home_testi_thumb");
										the_post_thumbnail('home_testi_thumb', $defalt_arg); ?>
										</div>
									</div>
									<div class="col-xs-8 testimony">
									<?php if(get_post_meta( get_the_ID(), 'testimonial_description', true )){ ?>
										<blockquote>
											"<?php echo get_post_meta( get_the_ID(), 'testimonial_description', true); ?>"
										</blockquote>
									<?php } ?>
									<h4 class="name"><?php the_title(); ?></h4>
									<?php if(get_post_meta( get_the_ID(), 'testimonial_designation', true )){ ?>
										<span class="pos">(<?php echo get_post_meta( get_the_ID(), 'testimonial_designation', true ); ?>)</span>
									<?php } ?>
									</div>
								</div>
							</div>
							<?php } ?>
						<?php endwhile;
					}else{ ?>
							<div class="swiper-slide">
								<div class="row">
									<div class="col-xs-4 pics">
									<div class="img-thumbnail ">
										<img class="img-responsive img-circle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/client2.png" alt=""/>
										</div>
									</div>
									<div class="col-xs-8 testimony">
										<blockquote>
											"I am realy satisfied with services of Healthcare Medical Services.
											All the Staffs are very friendly and helpful to assist throught
											medication procedure. The facilities are good."
										</blockquote>
										<h4 class="name">Marshall Law</h4>
										<span class="pos">(Web Developer)</span>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
							<div class="row">
											<div class="col-xs-4 pics">
											<div class="img-thumbnail ">
												<img class="img-responsive img-circle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/client1.png" alt=""/>
												</div>
											</div>
											<div class="col-xs-8 testimony">
												<blockquote>
													"I am realy satisfied with services of Healthcare Medical Services. 
													All the Staffs are very friendly and helpful to assist throught
													medication procedure. The facilities are good."
												</blockquote>
												<h4 class="name">Marshall Law</h4>
												<span class="pos">(Web Developer)</span>
											</div>
										</div>
							</div>
							<div class="swiper-slide">
							<div class="row">
								<div class="col-xs-4 pics">
								<div class="img-thumbnail">
									<img class="img-responsive img-circle" src="<?php echo get_stylesheet_directory_uri(); ?>/images/client2.png" alt=""/>
									</div>
								</div>
								<div class="col-xs-8 testimony">
									<blockquote>
										"I am realy satisfied with services of Healthcare Medical Services.
										All the Staffs are very friendly and helpful to assist throught
										medication procedure. The facilities are good."
									</blockquote>
										<h4 class="name">Marshall Law</h4>
										<span class="pos">(Web Developer)</span>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="swiper-pagination"></div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- Testimonail End -->