<?php get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space">
<?php the_post(); ?>
	<div class="container services">
<!-- Right Start -->
<div class="col-md-9 rightside gallery">
	<div class="col-md-12  blog-desc">
	<div class="col-md-12 ">
		<?php if(has_post_thumbnail()){ ?>
			<div class="img-thumbnail">
				<?php $class=array('class'=>'img-responsive service_detail_thumb');  
				the_post_thumbnail('service_detail_thumb', $class); ?>
			</div>
		<?php } ?>
		<?php if(get_post_meta( get_the_ID(), 'service_font_awesome_icons', true )){ ?>
			<div class="col-md-2 service-icon">
				<span class="<?php echo get_post_meta( get_the_ID(), 'service_font_awesome_icons', true ); ?>"></span>
			</div>
		<?php } ?>
		<div class="col-md-10">
			<div class="line"></div>
			<h3><?php the_title(); ?></h3>
		</div>
				<br/>
	</div>
	<?php the_content(); ?>
	<?php if(get_post_meta( get_the_ID(), 'service_button_link', true )){ ?>
	<a href="<?php echo get_post_meta( get_the_ID(), 'service_button_link', true ); ?>"><span class="fa fa-angle-right icon"></span><?php _e('Visit Link','weblizar'); ?></a>
	<?php } ?>
	</div>	
</div>
<!-- Right end -->
<!-- Side Bar start -->
<?php get_sidebar(); ?>
<!-- side bar end -->

	</div>
</div>


<?php get_footer(); ?>