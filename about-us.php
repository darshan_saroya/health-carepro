<?php //Template name: About Us
get_header(); 
get_template_part('blog','cover'); 
$health_data= health_care_get_options();
if ( have_posts()){ 
	while ( have_posts() ): the_post(); ?>
<!-- About Start -->
<div class="container-fluid space">
	<div class="container about-us">
	<?php if(has_post_thumbnail()): ?>
		<div class="col-md-12 no-pad">
			<?php $class=array('class'=>'img-responsive');  
				the_post_thumbnail('portfolio_detail_thumb', $class); ?>
		</div>
	<?php endif; ?>
		<div class="col-md-12">
		<?php the_content(); ?>
		</div>
	</div>
</div>
<?php endwhile; } ?>
<!-- Deppartment -->
<div class="container-fluid space">
	<div class="container about-department">
	<?php if($health_data['deptt_heading']!=''){ ?>
		<h1 class="color"><?php echo $health_data['deptt_heading']; ?></h1>
		<?php if($health_data['deptt_icon']!=''){ ?>
		<div class="ln2 color"></div>
		<span class="<?php echo $health_data['deptt_icon']; ?> color heart"></span>
		<div class="ln3 color"></div>
		<?php } ?>
	<?php } ?>
	<?php $args = array( 'post_type' => 'hc_deptts','posts_per_page' =>-1);
	$deptts= new WP_Query($args); 
	if( $deptts->have_posts() ){ ?>
		<div class="row depart">
		<?php while ( $deptts->have_posts() ) : $deptts->the_post(); ?>
			<button type="button" class="btn btn-lg collapsed" data-toggle="collapse" data-target="#<?php echo get_the_ID(); ?>"><span class="<?php echo get_post_meta( get_the_ID(), 'deptt_icon', true ); ?> icon"></span><h4><?php the_title(); ?></h4></button>
			  <div id="<?php echo get_the_ID(); ?>" class="collapse"><?php the_excerpt(); ?></div>
		<?php endwhile; ?>
		</div>
	<?php } ?>
	</div>
</div>
<!-- Member -->
<div class="container-fluid space">
	<div class="container about_doctors">
	<?php if($health_data['member_heading']!=''){ ?>
		<h1 class="color"><?php echo $health_data['member_heading']; ?></h1>
		<?php if($health_data['member_icon']!=''){ ?>
		<div class="ln2 color"></div>
		<span class="<?php echo $health_data['member_icon']; ?> color heart"></span>
		<div class="ln3 color"></div>
	<?php } } ?>
	<p class="desc"></p>
	<?php $args = array( 'post_type' => 'hc_member','posts_per_page' =>-1);
				$members= new WP_Query($args); 
				if( $members->have_posts() ){ ?>
		<div class="row">
		<?php while ( $members->have_posts() ) : $members->the_post(); ?>
			<div class="col-md-4 docs">
				<a href="<?php the_permalink(); ?>" class="btn name"><?php the_title(); ?></a>
				 <?php if(has_post_thumbnail()): ?>
				<div class="img-thumbnail">
					<?php $defalt_arg =array('class'=>"img-responsive");
					the_post_thumbnail('home_member_thumb', $defalt_arg);
					?>
				</div>
				<?php endif; ?>
				<div class="caption">
					<h4><?php if(get_post_meta( get_the_ID(), 'member_post', true )) echo get_post_meta( get_the_ID(), 'member_post', true ); ?> 
            <span>
						<?php $member_twitter = sanitize_text_field( get_post_meta( get_the_ID(), 'member_twitter', true ));
							if($member_twitter!=''){ ?>
							<a href="<?php echo $member_twitter; ?>"><i class="fa fa-twitter icon"></i></a>
									<?php } 
							$member_fb = sanitize_text_field( get_post_meta( get_the_ID(), 'member_fb', true ));
							if($member_fb!=''){ ?>
							<a href="<?php echo $member_fb; ?>"><i class="fa fa-facebook icon"></i></a>
							<?php } 
							$member_google = sanitize_text_field( get_post_meta( get_the_ID(), 'member_google', true ));
							if($member_google!=''){ ?>
							<a href="<?php echo $member_google; ?>"><i class="fa fa-google-plus icon"></i></a>
							<?php } ?>
            </span>
					</h4>
					<?php the_excerpt(); ?>
					<a href="<?php the_permalink(); ?>" class="btn"><?php _e('View Profile','weblizar'); ?></a>
				</div>
			</div>
			<?php endwhile; ?>
		</div>	
				<?php } ?>
	</div>
</div>

<?php get_template_part('home', 'appointment');
get_footer(); ?>