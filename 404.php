<?php get_header(); ?>
<!-- Start Blog Cover -->
<div class="container-fluid space cover">
	<div class="container">
		<h1 class="white"><?php echo get_bloginfo('name'); ?></h1>
		<h4 class="white"><?php _e('404 Error','weblizar'); ?></h4>
	</div>
</div>
<!-- End Blog Cover-->
<div class="container-fluid space">	
	<div class="container error">
			<h1><?php _e('40','weblizar'); ?><span class="grey"><?php _e('4','weblizar'); ?></span></h1>
			<h2><span class="fa fa-exclamation-circle"></span> <?php _e('ERROR','weblizar'); ?></h2>
			<h3><?php _e('Page cannot be found','weblizar'); ?></h3>
			<p><?php _e('The Page you requested is not be found. This could be spelling error in the url.','weblizar'); ?></p>
			<a href="<?php echo esc_url(home_url( '/' )); ?>" class="btn"><?php _e('Go back to homepage','weblizar'); ?></a>
	</div>
</div>
<?php get_footer(); ?>