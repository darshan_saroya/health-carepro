<?php
require( get_template_directory() . '/default-options.php' );
require(get_template_directory().'/core/theme-options/option-panel.php');
require( get_template_directory() . '/comment-function.php' );
require( get_template_directory() . '/core/custom/cpt.php' );
require( get_template_directory() . '/core/custom/image_crop.php' );
require( get_template_directory() . '/core/shortcodes/shortcodes.php' );
require( get_template_directory() . '/core/wl-footer-contact-widgets.php' );
require( get_template_directory() . '/core/wl-recent-posts.php' );
require( get_template_directory() . '/core/flickr-widget.php' );

/*After Theme Setup*/
add_action( 'after_setup_theme', 'health_care_head_setup' );
function health_care_head_setup(){
	$health_data= health_care_get_options();
	if ( ! isset( $content_width ) ) $content_width = 720;
	add_theme_support( 'post-thumbnails' ); //supports featured image
	register_nav_menu( 'primary', __( 'Primary Menu', 'weblizar' ) );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( "title-tag" );
	add_theme_support( "woocommerce" );
	load_theme_textdomain( 'weblizar', get_template_directory() . '/lang' );
	add_image_size('home_slider_thumb',1645,1000,true);
	add_image_size('home_testimonial_thumb',300,300,true);
	add_image_size('home_client_thumb',300,200,true);
	$custom_header = array(
	'width'         => 1920,
	'height'        => 500,
	'default-image' => get_template_directory_uri() . '/images/back4.jpg',
	);
	add_theme_support( 'custom-header', $custom_header );
	if($health_data['custom_bg']=='on'){
	$custom_bg = array(
	'default-color' => 'ffffff',
	);
	add_theme_support( 'custom-background', $custom_bg );
	}
	}

/*Excerpt Filter function*/
function hc_custom_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_more', 'hc_custom_excerpt_more' );

/*Enqueue Styles And JS*/
function health_care_theme_style(){
  $health_data= health_care_get_options();
	//style
	wp_enqueue_style('bootstrap',get_template_directory_uri() . '/css/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('font-awesome',get_template_directory_uri() . '/css/font-awesome.min.css');
	wp_enqueue_style('animate',get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style('simplelightbox',get_template_directory_uri() . '/css/simplelightbox.css');
	wp_enqueue_style('calendarCSS',get_template_directory_uri() . '/css/calendrical.css');
	wp_enqueue_style('mediaResponsive',get_template_directory_uri() . '/css/media-query.css');
	wp_enqueue_style('swiper',get_template_directory_uri() . '/css/swiper.min.css');
	wp_enqueue_style('imgGallery',get_template_directory_uri() . '/css/img-gallery.css');
	wp_enqueue_style('farbtasticStyle',get_template_directory_uri() . '/css/farbtastic.css');
	wp_enqueue_style('validationEngine-css',get_template_directory_uri() . '/css/validationEngine.jquery.css');
	wp_enqueue_style('color-chooser',get_template_directory_uri() . '/css/color-chooser.css');
  if($health_data['theme_color']!=''){
	require_once(get_template_directory() . '/custom-css.php');
		} else {
		wp_enqueue_style('color', get_template_directory_uri() . '/css/color/default.css');
		}
	//wp_enqueue_style('color',get_template_directory_uri() . '/css/color/default.css');
	wp_enqueue_style('open-sans','https://fonts.googleapis.com/css?family=Open+Sans:300');
	//JS
	
	wp_enqueue_script( 'jquery');
  wp_enqueue_script( 'masonry');
	wp_enqueue_script('bootstrap_js',get_template_directory_uri().'/css/bootstrap/js/bootstrap.min.js');
	wp_enqueue_script('simple-lightbox.min',get_template_directory_uri().'/js/simple-lightbox.min.js');
	wp_enqueue_script('calendarJS',get_template_directory_uri().'/js/jquery.calendrical.js');
	wp_enqueue_script('wow.min',get_template_directory_uri().'/js/wow.min.js');
	wp_enqueue_script('isotope.pkgd',get_template_directory_uri().'/js/isotope.pkgd.js');
	wp_enqueue_script('swiper.min',get_template_directory_uri().'/js/swiper.min.js');
	wp_enqueue_script('flexslider',get_template_directory_uri().'/js/jquery.flexslider.min.js');
	wp_enqueue_script('waypoints',get_template_directory_uri().'/js/waypoints.min.js');
	wp_enqueue_script('countTo',get_template_directory_uri().'/js/jquery.countTo.js');
	wp_enqueue_script('farbtastic',get_template_directory_uri().'/js/farbtastic.js');
	wp_enqueue_script('validate',get_template_directory_uri().'/js/validate.js');
	wp_enqueue_script('validate-en',get_template_directory_uri().'/js/jquery.validationEngine-en.js');
	if ( is_singular() ) wp_enqueue_script( "comment-reply" );
}
add_action( 'wp_enqueue_scripts', 'health_care_theme_style' );
add_action( 'wp_footer', 'footer_js' );
function footer_js(){
	wp_enqueue_script('scrollReveal',get_template_directory_uri().'/js/scrollReveal.min.js');
	wp_enqueue_script('theme_scripts',get_template_directory_uri().'/js/script.js');
	
}	

	
/* Health Care widget area */
	add_action( 'widgets_init', 'health_care_widgets_init');
	function health_care_widgets_init() {
	/*sidebar*/
	register_sidebar( array(
			'name' => __( 'Sidebar', 'weblizar' ),
			'id' => 'sidebar-primary',
			'description' => __( 'The primary widget area', 'weblizar' ),
			'before_widget' => '<div class="row sidebar-widget">',
			'after_widget' => '</div>',
			'before_title' => '<div class="line"></div><h4>',
			'after_title' => '</h4>'
		) );
	
	register_sidebar( array(
			'name' => __( 'Woocommerce Sidebar', 'weblizar' ),
			'id' => 'sidebar-woocommerce',
			'description' => __( 'The Woocommerce widget area', 'weblizar' ),
			'before_widget' => '<div class="row sidebar-widget">',
			'after_widget' => '</div>',
			'before_title' => '<div class="line"></div><h4>',
			'after_title' => '</h4>'
		) );

	register_sidebar( array(
			'name' => __( 'Footer Widget Area', 'weblizar' ),
			'id' => 'footer-widget-area',
			'description' => __( 'footer widget area', 'weblizar' ),
			'before_widget' => '<div class="col-md-3 col-sm-6 widget-footer">',
			'after_widget' => '</div>',
			'before_title' => '<div class="col-md-12 footer-heading"><h3>',
			'after_title' => '</h3><div class="ln white"></div><span class="fa fa-heart hrt"></span></div>',
		) );             
	}
	
	/** nav-menu-walker	*/
class health_care_nav_walker extends Walker_Nav_Menu {	
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"dropdown-menu\">\n";
	}
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		if ($args->has_children && $depth > 0) {
			$classes[] = 'dropdown dropdown-submenu';
		} else if($args->has_children && $depth === 0) {
			$classes[] = 'dropdown';
		}
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		//$attributes .= ($args->has_children) 	    ? ' data-toggle="dropdown" data-target="#" class="dropdown-toggle"' : '';
			
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= ($args->has_children) ? ' <b class="fa fa-plus"></b></a>' : '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

		if ( !$element )
			return;

		$id_field = $this->db_fields['id'];

		//display this element
		if ( is_array( $args[0] ) )
			$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
		else if ( is_object( $args[0] ) ) 
			$args[0]->has_children = ! empty( $children_elements[$element->$id_field] ); 
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array($this, 'start_el'), $cb_args);

		$id = $element->$id_field;

		// descend only when the depth is right and there are childrens for this element
		if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

			foreach( $children_elements[ $id ] as $child ){

				if ( !isset($newlevel) ) {
					$newlevel = true;
					//start the child delimiter
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array($this, 'start_lvl'), $cb_args);
				}
				$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
			}
			unset( $children_elements[ $id ] );
		}

		if ( isset($newlevel) && $newlevel ){
			//end the child delimiter
			$cb_args = array_merge( array(&$output, $depth), $args);
			call_user_func_array(array($this, 'end_lvl'), $cb_args);
		}

		//end this element
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array($this, 'end_el'), $cb_args);
	}
}
function health_care_nav_menu_css_class( $classes ) {
	if ( in_array('current-menu-item', $classes ) OR in_array( 'current-menu-ancestor', $classes ) OR in_array( 'menu-item-language-current', $classes ))
		$classes[]	=	'active';

	return $classes;
}
add_filter( 'nav_menu_css_class', 'health_care_nav_menu_css_class' );

function health_care_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'health_care_page_menu_args' );

 
function health_care_fallback_page_menu( $args = array() ) {

	$defaults = array('sort_column' => 'menu_order, post_title', 'menu_class' => 'menu', 'echo' => true, 'link_before' => '', 'link_after' => '');
	$args = wp_parse_args( $args, $defaults );
	$args = apply_filters( 'wp_page_menu_args', $args );

	$menu = '';

	$list_args = $args;

	// Show Home in the menu
	if ( ! empty($args['show_home']) ) {
		if ( true === $args['show_home'] || '1' === $args['show_home'] || 1 === $args['show_home'] )
			$text = 'Home';
		else
			$text = $args['show_home'];
		$class = '';
		if ( is_front_page() && !is_paged() )
			$class = 'class="current_page_item"';
		$menu .= '<li ' . $class . '><a href="' . home_url( '/' ) . '" title="' . esc_attr($text) . '">' . $args['link_before'] . $text . $args['link_after'] . '</a></li>';
		// If the front page is a page, add it to the exclude list
		if (get_option('show_on_front') == 'page') {
			if ( !empty( $list_args['exclude'] ) ) {
				$list_args['exclude'] .= ',';
			} else {
				$list_args['exclude'] = '';
			}
			$list_args['exclude'] .= get_option('page_on_front');
		}
	}

	$list_args['echo'] = false;
	$list_args['title_li'] = '';
	$list_args['walker'] = new weblizar_walker_page_menu;
	$menu .= str_replace( array( "\r", "\n", "\t" ), '', wp_list_pages($list_args) );

	if ( $menu )
		$menu = '<ul class="'. esc_attr($args['menu_class']) .'">' . $menu . '</ul>';

	$menu = '<div class="' . esc_attr($args['container_class']) . '">' . $menu . "</div>\n";
	$menu = apply_filters( 'wp_page_menu', $menu, $args );
	if ( $args['echo'] )
		echo $menu;
	else
		return $menu;
}

class health_care_walker_page_menu extends Walker_Page{
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class='dropdown-menu'>\n";
	}
	function start_el( &$output, $page, $depth=0, $args = array(), $current_page = 0 ) {
		if ( $depth )
			$indent = str_repeat("\t", $depth);
		else
			$indent = '';

		extract($args, EXTR_SKIP);
		$css_class = array('page_item', 'page-item-'.$page->ID);
		if ( !empty($current_page) ) {
			$_current_page = get_post( $current_page );
			if ( in_array( $page->ID, $_current_page->ancestors ) )
				$css_class[] = 'current_page_ancestor';
			if ( $page->ID == $current_page )
				$css_class[] = 'current_page_item';
			elseif ( $_current_page && $page->ID == $_current_page->post_parent )
				$css_class[] = 'current_page_parent';
		} elseif ( $page->ID == get_option('page_for_posts') ) {
			$css_class[] = 'current_page_parent';
		}

		$css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );

		$output .= $indent . '<li class="' . $css_class . '"><a href="' . get_permalink($page->ID) . '">' . $link_before . apply_filters( 'the_title', $page->post_title, $page->ID ) . $link_after . '</a>';

		if ( !empty($show_date) ) {
			if ( 'modified' == $show_date )
				$time = $page->post_modified;
			else
				$time = $page->post_date;

			$output .= " " . mysql2date($date_format, $time);
		}
	}
}
/****--- Navigation ---***/
//index page navigation
function health_care_navigation() {
	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;
	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}
	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}
	echo '<ul class="pagination">' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
	
	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}
	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );
	echo '</ul>' . "\n";
}

//Post Links
function health_care_post_link(){ 
	global $post; 
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
		<div class="row margin blog-pager">
		<ul class="pager">
			<li class="previous"><?php next_post_link( '%link', _x( '%title ', 'Next post link', 'weblizar' ) ); ?></li>
			<li class="next"><?php previous_post_link( '%link', _x( ' %title', 'Previous post link', 'weblizar' ) ); ?></li>
		</ul>
	</div>
	<?php }

	/* Breadcrumbs  */
	function health_care_breadcrumbs() {
    $delimiter = '';
    $home = __('Home', 'weblizar' ); // text for the 'Home' link
    $before = '<li>'; // tag before the current crumb
    $after = '</li>'; // tag after the current crumb
    echo '<ul class="breadcrumb">';
    global $post;
    $homeLink = home_url();
    echo '<li><a href="' . $homeLink . '">' . $home . '</a></li>' . $delimiter . ' ';
    if (is_category()) {
        global $wp_query;
        $cat_obj = $wp_query->get_queried_object();
        $thisCat = $cat_obj->term_id;
        $thisCat = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0)
            echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
        echo $before . _e("Archive by category","weblizar"). single_cat_title('', false). $after;
    } elseif (is_day()) {
        echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
        echo '<li><a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
        echo $before . get_the_time('d') . $after;
    } elseif (is_month()) {
        echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
        echo $before . get_the_time('F') . $after;
    } elseif (is_year()) {
        echo $before . get_the_time('Y') . $after;
    } elseif (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            echo '<li><a href="' . $homeLink . '/"></a></li>';
            echo get_the_title() . $after;
        } else {
            $cat = get_the_category();
            $cat = $cat[0];
            //echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            echo $before . get_the_title() . $after;
        }
    } elseif (is_search()) {
        echo $before . _e("Search results for","weblizar") . get_search_query() .$after;
    }elseif (is_attachment()) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID);
        $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo '<li><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
    } elseif (is_page() && !$post->post_parent) {
        echo $before . get_the_title() . $after;
    } elseif (is_page() && $post->post_parent) {
        $parent_id = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
            $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb)
            echo $crumb . ' ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
    } elseif (is_tag()) {        
		echo $before . _e('Tag','weblizar') . single_tag_title('', false) . $after;
    } elseif (is_author()) {
        global $author;
        $userdata = get_userdata($author);
        echo $before . _e("Articles posted by","weblizar") . $userdata->display_name . $after;
    } elseif (is_404()) {
        echo $before . _e("Error 404","weblizar") . $after;
    }elseif (!is_single() && !is_page() && get_post_type() != 'post') {
        $post_type = get_post_type_object(get_post_type());
        echo $before . $post_type->labels->singular_name . $after;
    }
    echo '</ul>';
	}

// template pagination
function template_pagination($pages = '', $range = 1)
{  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo '<ul class="pagination">';
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."'>&laquo; First</a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a></li>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class=\"active\"><a href='".get_pagenum_link($i)."'>".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a><li>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."'>Last &raquo;</a></li>";
         echo "</ul>\n";
     }
}
/*===================================================================================
	* Add Author Links
	* =================================================================================*/
	function hc_author_profile( $contactmethods ) {
	$contactmethods['social_icon_1'] = 'Social FontAwesome Icon 1';
	$contactmethods['social_link_1'] = 'Social Link 1';
	$contactmethods['social_icon_2'] = 'Social FontAwesome Icon 2';
	$contactmethods['social_link_2'] = 'Social Link 2';
	$contactmethods['social_icon_3'] = 'Social FontAwesome Icon 3';
	$contactmethods['social_link_3'] = 'Social Link 3';
	$contactmethods['social_icon_4'] = 'Social FontAwesome Icon 4';
	$contactmethods['social_link_4'] = 'Social Link 4';
	return $contactmethods;
	}
	add_filter( 'user_contactmethods', 'hc_author_profile', 10, 1);
	/*===================================================================================
	* Add Class Gravtar
	* =================================================================================*/
	add_filter('get_avatar','hc_gravatar_class');

	function hc_gravatar_class($class) {
    $class = str_replace("class='avatar", "class='img-responsive img-circle", $class);
    return $class;
	}

?>