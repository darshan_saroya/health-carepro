<div class="row <?php if(is_single()){ echo 'blog-desc'; }else{ echo 'right-widget'; }?>">		
	<?php if(has_post_thumbnail()): ?>
			<div class="col-md-12 no-pad">
			<div class="img-thumbnail">
			<?php $defalt_arg =array('class'=>"img-responsive single_post_thumb");
					the_post_thumbnail('single_post_thumb', $defalt_arg);
					?>
				<div class="overlay">
					<a class="lightbox" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"><span class="fa fa-search"></span></a>
					<?php if(!is_single() && !is_page()){ ?>
					<a href="<?php the_permalink();?>"><span class="fa fa-chain"></span></a>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="col-md-12 border">
				<div class="line"></div>
				<h3><?php if(is_page_template('blog-full.php') || is_page_template('blog-left.php') || is_page_template('blog-right.php')){ ?>
					<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
				<?php }elseif(is_single() || is_page()){ ?><?php the_title(); }else{ ?>
					<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
				<?php } ?></h3>
				<?php if(!is_page()): ?>
				<div class="hc_tags"><?php the_tags('<span class>Tags: </span>', NULL); ?></div>
				<div class="date">
					<a href="<?php the_permalink();?>"><span class="fa fa-clock-o"> <?php echo get_the_date(); ?></span> </a>  | <a href="<?php comments_link(); ?>"><span class="fa fa-comment-o"> <?php echo comments_number(__('No Comments','weblizar'), __('1 Comment','weblizar'), '% Comments'); ?></span> </a>
				</div>
				<?php if(get_the_category_list() != '') { ?>
				<div class="category">
					<span><?php _e('Categories :','weblizar'); ?> </span><?php the_category(' , '); ?> 
				</div>
				<?php } ?>
				<?php endif; ?>
				<div class="hc_content"><?php if(is_page_template('blog-full.php') || is_page_template('blog-left.php')|| is_page_template('blog-right.php')){ the_excerpt(); }elseif(is_single() || is_page()){ the_content(); }else{ the_excerpt(); } ?></div>
				<?php $defaults = array(
					'before'           => '<p>' . __( 'Pages:','weblizar' ),
					'after'            => '</p>',
					'link_before'      => '',
					'link_after'       => '',
					'next_or_number'   => 'number',
					'separator'        => ' ',
					'nextpagelink'     => __( 'Next page','weblizar'),
					'previouspagelink' => __( 'Previous page','weblizar' ),
					'pagelink'         => '%',
					'echo'             => 1
					);
				wp_link_pages( $defaults );	?>
			</div>
		</div>