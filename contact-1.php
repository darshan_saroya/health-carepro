<?php //Template name: Contact Us 1
get_header(); 
$health_data= health_care_get_options();
get_template_part('blog','cover'); ?>
<div class="container-fluid space">
	<div class="container contact">
		<div class="row">
      <?php if ( have_posts()){ 
			while ( have_posts() ): the_post(); 
			if(get_the_content()):?>
			<div class="col-md-6">
				<?php the_content(); ?>
        </div>
			<?php endif; endwhile;
			}
			?>
			
			<?php if($health_data['google_map_url']!=''){ ?>
			<div class="col-md-<?php if(get_the_content()){ echo '6';}else{ echo '12'; } ?> map1 no-pad">
				<iframe src="<?php echo $health_data['google_map_url']; ?>"
			width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<?php } ?>
		</div>
		<div class="row contact-detail">
			<?php if($health_data['contact_detail_heading']!=''){ ?>
				<div class="line"></div>
				<h3><?php echo $health_data['contact_detail_heading']; ?></h3>
			<?php } ?>
			<?php for($i=1; $i<=4; $i++){
				if($health_data['contact_heading_'.$i]!=''){ ?>
				<div class="col-md-3 contact-info">
					<div class="col-md-12">
					<?php if($health_data['contact_icon_'.$i]!=''){ ?>
						<span class="<?php echo $health_data['contact_icon_'.$i]; ?> icon"> <?php echo $health_data['contact_heading_'.$i]; ?> : </span>
					<?php } ?>
					<?php if($health_data['contact_detail_'.$i]!=''){ ?>
						<h4><?php echo $health_data['contact_detail_'.$i]; ?></h4>
					<?php } ?>
					</div>
				</div>
				<?php } } ?>
		</div>
		<div class="row message" id="contact-1">
		<?php if($health_data['contact_form_heading']!=''){ ?>
			<div class="line"></div>
			<h3><?php echo $health_data['contact_form_heading']; ?></h3>
		<?php } ?>
			<form role="form" method="POST" id="hc-form" action="#contact-1">
			<div class="result"></div>
				<div class="form-group col-md-4">
				<label id="name-label">
					<input type="text" name="user_name" id="user_name" class="validate[required] form-control" placeholder="Name"/>
				</label>
				</div>
				<div class="form-group col-md-4">
				<label id="email-label">
					<input type="text" name="user_email" id="user_email" class="validate[required,custom[email]] form-control" placeholder="Email Address"/>
				</label>
				</div>
				<div class="form-group col-md-4">
				<label id="phone-label">
					<input type="text" name="user_contact" id="user_contact" class="form-control" placeholder="Phone Number"/>
				</label>
				</div>
				<div class="form-group col-md-12">
				<label id="message-label">
					<textarea name="user_message" rows="5" id="user_message" class="validate[required] form-control" placeholder="Message"></textarea>
				</label>
				</div>
				<div class="col-md-12">
					<input type="submit" class="btn" name="query_submit" id="query_submit" value="<?php if($health_data['contact_form_button']!=""){ echo $health_data['contact_form_button']; }else{
						_e('Send Message','weblizar');
					} ?>" />
				</div>
			</form>
		</div>
		<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#hc-form").validationEngine();
		});
	</script>
	<?php 
				if(isset($_POST['query_submit']))
				{ 	if($_POST['user_name']==''){	
						echo "<script>jQuery('#contact_name_error').show();</script>";
					} else
					if($_POST['user_email']=='') {
						echo "<script>jQuery('#contact_email_error').show();</script>";
					} else
					if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $_POST['user_email'])) {								
						echo "<script>jQuery('#contact_email_error').show();</script>";
					} else	
					if($_POST['user_message'] ==''){							
						echo "<script>jQuery('#contact_user_massage_error').show();</script>";
					}
					else
					{	
						if($health_data['feedback_mail']!=''){ $email = $health_data['feedback_mail'];
						}else{
						$email = get_option('admin_email');
						}
						$subject = "You have new enquiry  form".get_option("blogname");
						$massage =  stripslashes(trim($_POST['user_message']))."Message sent from  Name:" . trim($_POST['user_name']). "<br>Email :". trim($_POST['user_email']). "<br>Contact :". trim($_POST['user_contact']);
						$headers = "From: ".trim($_POST['user_name'])." <".trim($_POST['user_email']).">\r\nReply-To:".trim($_POST['user_email']);							
						$enquerysend =wp_mail( $email, $subject, $massage, $headers);
						echo "<script>jQuery('.result').html('<p>Form Submitted Successfully!</p>');</script>";	
					}
				}
			?>
	</div>
</div>

<div class="container-fluid space blue">
	<div class="container">
		<div class="emergency white">
		<?php if($health_data['contact_footer_icon']!=''){ ?>
			<span class="<?php echo $health_data['contact_footer_icon']; ?> icon"></span>
		<?php } ?>
		<?php if($health_data['contact_footer_text']!=''){ ?>
			<h4><?php echo $health_data['contact_footer_text']; ?></h4>
		<?php } ?>
		<?php if($health_data['contact_footer_number']!=''){ ?>
			<h2><a href="tel:<?php echo $health_data['contact_footer_number']; ?>" ><?php echo $health_data['contact_footer_number']; ?></a></h2>
			<div class="ln"></div>
			<span class="fa fa-heart icon"></span>
		<?php } ?>
		</div>		
	</div>
</div>
<?php get_footer(); ?>