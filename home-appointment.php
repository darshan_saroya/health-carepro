<!-- Booking Appointment Start-->
<?php $health_data= health_care_get_options(); ?>
<span class="anchor" id="appoinment"></span>
	<div class="row abt-appoinment appoint">
	<?php if($health_data['feedback_heading']!=''){ ?>
		<div class="col-md-3 col-sm-12 appoinment">
			<h2><span class="<?php echo $health_data['feedback_icon']; ?> icon"></span><?php echo $health_data['feedback_heading']; ?></h2>
		</div>
	<?php } ?>
		<div class="col-md-9 col-sm-12 feed" >
			<form role="form" method="POST" id="hc-form" action="#appoinment">
			<div class="result"></div>
				<div class="form-group col-md-4 col-sm-4">
					<input type="text" name="user_name" id="user_name" class="validate[required] form-control" placeholder="Full Name">
				</div>
				<div class="form-group col-md-4 col-sm-4">
					<input type="email" id="user_email" name="user_email" class="validate[required,custom[email]] form-control"  placeholder="Your Email Address">
				</div>
				<div class="form-group col-md-4 col-sm-4">
					<input type="text" name="user_contact" id="user_contact" class="form-control" placeholder="Phone Number">
				</div>
				<div class="form-group col-md-12 col-sm-12">
					<textarea class="validate[required] form-control" name="user_message" id="user_message" placeholder="Message"></textarea>
				</div>
				<div class="col-md-12 col-sm-12">
					 <input type="submit" name="query_submit" id="query_submit" class="btn" value="<?php if($health_data['feedback_btn']!=''){ echo $health_data['feedback_btn']; }else { _e('Booking Appointment','weblizar'); }?>" />
				</div>
			</form>
		</div>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#hc-form").validationEngine();
		});
	</script>
	<?php 
				if(isset($_POST['query_submit']))
				{ 	if($_POST['user_name']==''){	
						echo "<script>jQuery('#contact_name_error').show();</script>";
					} else
					if($_POST['user_email']=='') {
						echo "<script>jQuery('#contact_email_error').show();</script>";
					} else
					if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $_POST['user_email'])) {								
						echo "<script>jQuery('#contact_email_error').show();</script>";
					} else	
					if($_POST['user_message'] ==''){							
						echo "<script>jQuery('#contact_user_massage_error').show();</script>";
					}
					else
					{	if($health_data['feedback_mail']!=''){ $email = $health_data['feedback_mail'];
						}else{
						$email = get_option('admin_email');
						}
						$subject = "You have new enquiry  form".get_option("blogname");
						$massage =  stripslashes(trim($_POST['user_message']))."Message sent from  Name:" . trim($_POST['user_name']). "<br>Email :". trim($_POST['user_email']). "<br>Contact Number :". trim($_POST['user_contact']);
						$headers = "From: ".trim($_POST['user_name'])." <".trim($_POST['user_email']).">\r\nReply-To:".trim($_POST['user_email']);							
						$enquerysend =wp_mail( $email, $subject, $massage, $headers);
						echo "<script>jQuery('.result').html('<p>Form Submitted Successfully!</p>');</script>";	
					}
				}
			?>
</div>
<!-- Booking Appointment End -->