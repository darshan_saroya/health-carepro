<?php $health_data = health_care_get_options();
if($health_data['front_page']=='on'){
get_header(); 
get_template_part('home','slider');
$data = $health_data['home_page_layout'];
		if($data)
		{	foreach($data as $key=>$value)
			{	switch($value) 
				{	case 'callout': 
					get_template_part('home', 'callout');
					break;
					case 'services': 
					get_template_part('home', 'services');
					break;
					case 'deptt': 
					get_template_part('home', 'deptt');
					break;
					case 'doctor': 
					get_template_part('home', 'doctor');
					break;
					case 'testimonial': 
					get_template_part('home', 'testimonial');
					break;
					case 'blog': 
					get_template_part('home', 'blog');
					break;
					case 'gallery': 
					get_template_part('home', 'gallery');
					break;
					case 'facts': 
					get_template_part('home', 'facts');
					break;
					case 'clients': 
					get_template_part('home', 'clients');
					break;
					case 'appointment': 
					get_template_part('home', 'appointment');
					break;
					}
			}
get_footer();
 }
}else {	
			get_template_part( 'page' );
	} ?>