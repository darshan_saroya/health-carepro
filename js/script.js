 /*-----------------------------------
 Home Section Text Slider
 -----------------------------------*/

 jQuery(window).load(function(){
  "use strict";
	jQuery('.header-text').flexslider({
	animation: "slide",
	selector: ".text-slider .text-slides",
	slideshowSpeed: 5000,  
	controlNav: false,
	directionNav: false ,
	direction: "vertical",
      start: function(slider){
       jQuery('body').removeClass('loader'); 
      }
  });
 });
 
/*============================================
	iPad Slider
	==============================================*/
	jQuery('.ipad-slider').flexslider({
		prevText: '<i class="fa fa-angle-left"></i>',
		nextText: '<i class="fa fa-angle-right"></i>',
		animation: 'slide',
		slideshowSpeed: 3000,
		useCSS: true,
		controlNav: false,
		pauseOnAction: false, 
		pauseOnHover: true,
		smoothHeight: false
	});

	
	/*============================================
	Skills Charts
	==============================================*/
	var firstLoad = true;
	jQuery('.skills').waypoint(function(){
		if(firstLoad){
			jQuery('.countTo').each(count);
			firstLoad = false;
		}
		function count(options) {
			var $this = jQuery(this);
			options = jQuery.extend({}, options || {}, $this.data('countToOptions') || {});
			$this.countTo(options);
		  }
		  
		jQuery('.chart').each(function(){
			({
					size:140,
					animate: 2000,
					lineCap:'round',
					scaleColor: false,
					barColor: '#fff',
					trackColor: 'transparent',
					lineWidth: 7
			});
		});					
		
	},{offset:'80%'});
	
	jQuery.easing['jswing']=jQuery.easing['swing'];jQuery.extend(jQuery.easing,{easeOutBounce:function(x,t,b,c,d){if((t/=d)<(1/2.75)){return c*(7.5625*t*t)+b}else if(t<(2/2.75)){return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b}else if(t<(2.5/2.75)){return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b}else{return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b}}});

	jQuery('.gHover').hover(function() {
		jQuery(this).stop().animate({
			top: -20
		}, 900, "easeOutBounce");
	}, function() {
		jQuery(this).stop().animate({
			top: 0
		}, 900, "easeOutBounce");
	});
	
	// Isotope blog
			var $service_style1 = jQuery('.gallery1');
            jQuery(window).load(function () {
                // Initialize Isotope
				$service_style1.isotope({
                    itemSelector: '.wl-gallery'
                });
			});
			
//wow animation
 new WOW().init();
 

 //calendar
 jQuery(function(){
        jQuery('#date_iso_format').calendricalDate({
            iso: 1,
        });
    });
	
//Menu JS
jQuery(document).ready(function() {
	if( jQuery(window).width() > 768) {
	   jQuery('.nav li.dropdown').hover(function() {
		   jQuery(this).addClass('open');
	   }, function() {
		   jQuery(this).removeClass('open');
	   }); 
	   jQuery('.nav li.dropdown-menu').hover(function() {
		   jQuery(this).addClass('open');
	   }, function() {
		   jQuery(this).removeClass('open');
	   }); 
	}
	
	jQuery('.nav li.dropdown').find('.fa-plus').each(function(){
		jQuery(this).on('click', function(){
			if( jQuery(window).width() < 768) {
				jQuery(this).parent().next().slideToggle();
			}
			return false;
		});
	});
});


//Custom Owl JS

   jQuery(document).ready(function() {
   	 jQuery('#grid').isotope({
    itemSelector: '.port-item',
    masonry: {
      columnWidth: 0
    }
  });
  });
// external js: isotope.pkgd.js

// filter functions
var filterFns = {
  // show if number is greater than 50
  numberGreaterThan50: function() {
    var number = $(this).find('.number').text();
    return parseInt( number, 10 ) > 50;
  },
  // show if name ends with -ium
  ium: function() {
    var name = $(this).find('.name').text();
    return name.match( /ium$/ );
  }
};

function getHashFilter() {
  // get filter=filterName
  var matches = location.hash.match( /filter=([^&]+)/i );
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent( hashFilter );
}

jQuery( function() {

  var $grid = jQuery('.grid');

  // bind filter button click
  var $filterButtonGroup = jQuery('.filter-button-group');
  $filterButtonGroup.on( 'click', 'button', function() {
    var filterAttr = jQuery( this ).attr('data-filter');
    // set filter in hash
    location.hash = 'filter=' + encodeURIComponent( filterAttr );
  });

  var isIsotopeInit = false;

  function onHashchange() {
    var hashFilter = getHashFilter();
    if ( !hashFilter && isIsotopeInit ) {
      return;
    }
    isIsotopeInit = true;
    // filter isotope
    $grid.isotope({
      itemSelector: '.element-item',
      layoutMode: 'fitRows',
      // use filterFns
      filter: filterFns[ hashFilter ] || hashFilter
    });
    // set selected class on button
    if ( hashFilter ) {
      $filterButtonGroup.find('.is-checked').removeClass('is-checked');
      $filterButtonGroup.find('[data-filter="' + hashFilter + '"]').addClass('is-checked');
    }
  }

  jQuery(window).on( 'hashchange', onHashchange );

  // trigger event handler to init Isotope
  onHashchange();

});
jQuery(document).ready(function() {
		jQuery(window).scroll(function () {
		if (jQuery(this).scrollTop() > 100) {
			jQuery('.menu-layout').addClass('sticky-head');
		} else {
			jQuery('.menu-layout').removeClass('sticky-head');
		}
	});
	});
//deptt
jQuery(document).ready(function() {
			jQuery('#home-deptts li').click(function(){
			jQuery('#home-deptts li').removeClass('active');
			jQuery(this).addClass('active');
			});
			jQuery('#home-deptts li').click(function(){
			var data1=jQuery(this).attr('id');
			jQuery('.tab-pane').removeClass('active');
			jQuery('.tab-pane.'+data1).addClass('active');
			});
		});
//scrolltop
jQuery(document).ready(function () {	
   jQuery('[data-toggle="tooltip"]').tooltip();
  jQuery('[data-toggle="popover"]').popover();
		jQuery(window).scroll(function () {
			if (jQuery(this).scrollTop() > 150) {
				jQuery('.hc_scrollup').fadeIn();
			} else {
				jQuery('.hc_scrollup').fadeOut();
			}
		});
	
		jQuery('.hc_scrollup').click(function () {
			jQuery("html, body").animate({
				scrollTop: 0
			}, 600);
			return false;
		});
  
	});	
//scroll

(function($) {
		'use strict';
		window.sr= new scrollReveal({
        reset: true,
        move: '50px',
        mobile: true
        });

      })();

jQuery(document).ready(function() {
	jQuery('#grid-masonry').masonry({
  // options
  itemSelector: '.port-item'
});
/* Swiper 1*/

var swiper = new Swiper('.home_testi', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });
	/* Swiper 1*/
	/* Swiper 2*/
	
	var swiper = new Swiper('.hc_homedept', {
        pagination: '.swiper-pagination',
		nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
		slidesPerView: 6,
        paginationClickable: true,
        spaceBetween: 30,
		breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 2,
                spaceBetween: 10
            }
        }
    });
	/* Blog Swiper */
	var swiper = new Swiper('.hc_homeblog', {
		nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
		slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,	
    });
	
	/* Client */
	var swiper = new Swiper('.hc_homeclient', {
        pagination: '.swiper-pagination',
		slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 0,
		breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
           },
            640: {
                slidesPerView: 2,
           },
            320: {
                slidesPerView: 2,
           }
        }
    });
	var swiper = new Swiper('.hc_relatedports', {
		nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
		slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 0,
		breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
           },
            640: {
                slidesPerView: 2,
           },
            320: {
                slidesPerView: 1,
           }
        }
    });
	
	var swiper = new Swiper('.hc_relatedposts', {
		nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
		slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 0,
		breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
           },
            640: {
                slidesPerView: 2,
           },
            320: {
                slidesPerView: 1,
           }
        }
    });
	
	
	/* about testimonial */
	var swiper = new Swiper('.about_testi', {
		slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,	
    });
	
  });