<div class="row posts">
					<?php $cats = array();
foreach(wp_get_post_categories(get_the_ID()) as $c){
	$cat = get_category($c);
	array_push($cats, $cat->cat_ID); } ?>
						<?php $args= array('post_type' => 'post','posts_per_page'=>-1, 'category__in' => $cats, 'post__not_in'=> array(get_the_ID())); 
						$the_query = new WP_Query( $args ); 
						if ( $the_query->have_posts() ) : ?>
						<div class="line"></div>
					<h3><?php _e('Related Post','weblizar'); ?></h3>
					<div class="hc_relatedposts">
						<div id="related-members" class="swiper-wrapper gallery">
							<?php while ( $the_query->have_posts() ): 
									$the_query->the_post();
										if(has_post_thumbnail()): ?>
									<div class="swiper-slide">
										<div class="col-md-12 pic">
											<div class="pics">
												<div class="img-thumbnail">
														<?php $defalt_arg =array('class'=>"img-responsive related_post_thumb");
														the_post_thumbnail('related_post_thumb',$defalt_arg); ?>
														<div class="overlay">
															<a class="related-post" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"><span class="fa fa-search"></span></a>
															<a href="<?php the_permalink(); ?>"><span class="fa fa-chain"></span></a>
														</div>
												</div>
											</div>
											<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											<h5><?php echo get_the_date(); ?></h5>
										</div>
									</div>
									<?php endif;
										endwhile; ?>
										</div>
										<div class="swiper-button-next swiper-button-white"></div>
			<div class="swiper-button-prev swiper-button-white"></div>
				</div>
									<?php endif; ?>
					
			</div>