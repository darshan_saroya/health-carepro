<?php get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space">
	<div class="container blogs">
	<!-- Right Start -->
		<div class="col-md-9 rightside gallery">
			<div <?php post_class();?>>	
				<?php if ( have_posts()): 
					while ( have_posts() ): the_post();
						get_template_part('post','content');
						health_care_post_link();
					endwhile; 
				endif; 
				get_template_part('author','intro');
				comments_template( '', true );
				get_template_part('post','related'); ?>
			</div>
		</div>
	<!-- Right end -->
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>