<!-- Clients Start-->
<?php $health_data= health_care_get_options(); ?>
<span class="anchor" id="client-section"></span>
<div class="container-fluid space clients">
	<div class="container">
		<?php if($health_data['client_heading']!=""){ ?>
			<h1 class="color"><?php echo $health_data['client_heading']; ?></h1>
			<?php if($health_data['client_icon']!=''){ ?>
			<div class="ln2 color"></div>
			<span class="<?php echo $health_data['client_icon']; ?> color heart"></span>
			<div class="ln3 color"></div>
		<?php } ?>
		<?php } ?>
		<?php if($health_data['client_desc']!=""){ ?>
			<p class="desc"><?php echo $health_data['client_desc']; ?></p>
		<?php } ?> 
		 <div class="hc_homeclient">
			<div class="swiper-wrapper">
<?php $all_posts = $health_data['client_count']; 
$args = array( 'post_type' => 'hc_clients','posts_per_page' =>$all_posts);
$client = new WP_Query( $args );
if( $client->have_posts() ){ ?>
<?php while ( $client->have_posts() ) : $client->the_post();  ?>
<?php if(has_post_thumbnail()): ?>
<div class="swiper-slide">
					<?php $defalt_arg =array('class'=>"img-responsive home_client_thumb"); ?>
					<?php if(get_post_meta( get_the_ID(), 'client_link', true )){ ?>
					<a href="<?php echo get_post_meta( get_the_ID(), 'client_link', true ); ?>" title="<?php the_title(); ?>" <?php if(get_post_meta( get_the_ID(),'client_link_target', true )) { echo "target='_blank'"; }?>><?php the_post_thumbnail('home_client_thumb', $defalt_arg); ?></a>
					<?php }else{ ?>
					<?php the_post_thumbnail('home_client_thumb', $defalt_arg); ?>
					<?php } ?>
</div>
<?php endif; ?>
<?php endwhile; ?>       
<?php }else{ 
for($i=1; $i<=4; $i++){ ?>
<div class="swiper-slide">
	<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/client/c<?php echo $i; ?>.png" alt="client" />
</div>
<?php }
} ?>
			</div>
			<div class="swiper-pagination"></div>
		</div>
	</div>
</div>
<!-- Clients End -->