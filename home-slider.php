<!-- Slider Start-->
<?php $health_data = health_care_get_options(); ?>
	<div id="carousel-example-generic" class="carousel slide slider" data-ride="carousel" data-interval="<?php if($health_data['slide_autoplay']=='off'){ echo 'false'; }else { echo $health_data['slide_interval']; } ?>" data-pause="<?php if($health_data['slide_pause']=='on') echo 'hover'; ?>" data-wrap="<?php if($health_data['slide_cycle']=='on') { echo 'true'; }else{ echo 'false';} ?>" > 
	<div class="carousel-inner" role="listbox">
	<?php $all_posts = wp_count_posts( 'hc_slider')->publish;
	$args = array( 'post_type' => 'hc_slider','posts_per_page' =>$all_posts);        
	$slider = new WP_Query( $args );
	if( $slider->have_posts() )
	{ $t=1; ?>
	<!-- Wrapper for slides -->
	<?php while ( $slider->have_posts() ) : $slider->the_post(); 
	if(has_post_thumbnail()): ?>
      <div class="item <?php if($t==1) { echo 'active'; } ?>">
        <?php 
					$defalt_arg =array('class'=>"img-responsive home_slider_thumb");
					the_post_thumbnail('home_slider_thumb', $defalt_arg); 	
				 ?>
			<div class="container">
				<div class="carousel-caption">
				<div class="row slider-caption">
				<?php if(get_post_meta( get_the_ID(),'slider_icon', true )) { ?>	
				<div class="ln white"></div>
				<span class="<?php echo get_post_meta( get_the_ID(),'slider_icon', true ); ?> icon"></span>
					<div class="ln1 white"></div>
					<?php } ?>
					<div class="col-md-12">
					<?php if(the_title('','','')) { ?>
					<h1 class="wow zoomInUp" data-animation="animated bounceInRight"><?php the_title(); ?></h1>
					<?php } ?>
					<?php if(get_post_meta( get_the_ID(),'slider_desciption', true )) { ?>
					<p class="wow fadeIn" data-animation="animated bounceInLeft"><?php echo get_post_meta( get_the_ID(),'slider_desciption', true ); ?></p>
					<?php } ?>
					</div>
				</div>
					<?php if(get_post_meta( get_the_ID(),'slider_button_text', true )) { ?>
					<div class="row slider-link">
					<a href="<?php echo get_post_meta( get_the_ID(),'slider_button_link', true );  ?>" class="btn btn-primary" data-animation="animated zoomInUp" <?php if(get_post_meta( get_the_ID(),'slider_button_target', true )) { echo "target='_blank'"; }?> ><?php echo get_post_meta( get_the_ID(),'slider_button_text', true ); ?></a>
					</div>
					<?php } ?>
				</div>
			</div>
      </div>
	<?php $t++; endif; endwhile; ?>
	<?php }else{ $t=3; 
for($j=1; $j<=2; $j++){ ?>
	<div class="item <?php if($j==1) echo 'active'; ?>">
        <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/slider<?php echo $j; ?>.jpg" alt="Chania" style="max-height:<?php echo $health_data['slide_height']; ?>px;" >
			<div class="container">
				<div class="carousel-caption">
					<div class="row slider-caption">
						<div class="ln white"></div>
						<span class="fa fa-heart icon"></span>
						<div class="ln1 white"></div>
						<div class="col-md-12">
							<h1 class="wow zoomInUp" data-animation="animated bounceInRight">Our Healthcare</h1>
							<p class="wow fadeIn" data-animation="animated bounceInLeft">The Redwood Platform: The Ultimate Healthcare web marketing and management plateform one can sue of our responsibilities. we never compro mise with the diseas of the patient because that can be harmful for him and we can't take risks. </p>
						</div>
					</div>
					<div class="row slider-link">
						<a class="btn btn-primary" data-animation="animated zoomInUp" >LEARN MORE +</a>
					</div>
				</div>
			</div>
      </div>
	<?php } } ?>
	</div>
	<!-- Indicators -->
    <ol class="carousel-indicators">
	<?php for($i=0; $i<$t-1; $i++) { ?>
      <li  data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" <?php if($i==0) { echo 'class="active"'; } ?> ></li>
	  <?php } ?>
    </ol>
	<?php if($t>2){ ?>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only"><?php _e('Previous','weblizar'); ?></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only"><?php _e('Next','weblizar'); ?></span>
    </a>
	<?php } ?>
  </div>
   <!-- Slider End-->