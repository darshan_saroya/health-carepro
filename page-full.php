<?php //Template name: Full Width Page
get_header(); 
get_template_part('blog','cover'); ?>
<div class="container-fluid space">
	<div class="container blogs">
	<!-- Right Start -->
<div class="col-md-12 rightside gallery">
<div <?php post_class();?>>
				<?php if ( have_posts()): 
					while ( have_posts() ): the_post(); ?>
					<?php get_template_part('post','content'); ?>
					<?php endwhile; 
				endif; ?>
	</div>	
</div>
<!-- Right end -->
	</div>
</div>
<?php get_footer(); ?>