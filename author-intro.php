<?php if(get_the_author_meta('description')) : ?>
<div class="row author border ">
					<div class="line"></div>
					<h3><?php the_author(); ?></h3>
					<div class="row mg">
						<div class="col-xs-2">
							<?php echo get_avatar( get_the_author_meta('email') , 130 ); ?>
						</div>
						<div class="col-xs-10">
							<p><?php echo get_the_author_meta('description'); ?></p>
						</div>
					</div>
					<div class="col-md-12">
						<div class="alignright">
						<?php for($i=1; $i<=4; $i++){ 
						if(get_the_author_meta( 'social_icon_'.$i )!='' && get_the_author_meta( 'social_link_'.$i )!=''){?>
							<a href="<?php echo get_the_author_meta( 'social_link_'.$i ); ?>"><span class="<?php echo get_the_author_meta( 'social_icon_'.$i ); ?>"></span></a>
						<?php } } ?>
						</div>
					</div>
				</div>
<?php endif; ?>